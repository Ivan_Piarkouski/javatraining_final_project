import by.epam.hospital.dao.DaoHolder;
import by.epam.hospital.dao.IPatientDao;
import by.epam.hospital.dao.IPrescriptionDao;
import by.epam.hospital.dao.IWorkerDao;
import by.epam.hospital.dao.exception.DaoException;
import by.epam.hospital.dao.entity.Patient;
import by.epam.hospital.dao.entity.PatientStatus;
import by.epam.hospital.dao.entity.Prescription;
import by.epam.hospital.dao.entity.PrescriptionStatus;
import by.epam.hospital.dao.entity.PrescriptionType;
import by.epam.hospital.dao.entity.Worker;
import by.epam.hospital.dao.entity.WorkersPosition;
import by.epam.hospital.dao.entity.WorkersStatus;
import by.epam.hospital.dao.util.ConnectionPull;
import org.junit.Test;
import static org.junit.Assert.assertTrue;

import java.sql.Date;

public class CrudeDaoTest {

   private final IWorkerDao workerDao = DaoHolder.getWorkerDao();
    private final IPatientDao patientDao = DaoHolder.getPatientDao();
    private final  IPrescriptionDao prescriptionDao = DaoHolder.getPrescriptionDao();
    private final Worker worker;
    private final Patient patient;
    private final Prescription prescription;

    {
	ConnectionPull.getInstance().initPool();

	worker = new Worker();
	worker.setFullName("testWorker");
	worker.setAddress("testAddress");
	worker.setStatus(WorkersStatus.IS_WORKING);
	worker.setPosition(WorkersPosition.DOCTOR);
	worker.setEmail("test@test.com");
	worker.setHashPass("abracadabra".hashCode());



	patient = new Patient();
	patient.setFullName("TestFullName");
	patient.setAddress("testAddress");
	patient.setStatus(PatientStatus.ON_TREATMENT);
	patient.setInitDiagnosis("smthWrong");
	patient.setInDate(Date.valueOf("2015-05-06"));

	prescription = new Prescription();
	prescription.setDate(Date.valueOf("2015-05-06"));
	prescription.setStatus(PrescriptionStatus.ASSIGNED);
	prescription.setType(PrescriptionType.OPERATION);
	prescription.setContent("operate something");
    }

    @Test
    public void crudeWorkerTest()throws DaoException{
	int workerId = workerDao.addEntity(worker);
	worker.setId(workerId);
	Worker databaseWorker = workerDao.getEntity(workerId);
	assertTrue(worker.equals(databaseWorker));

	worker.setFullName("new Test Full name");
	worker.setPosition(WorkersPosition.NURSE);
	worker.setStatus(WorkersStatus.ON_VACATION);
	worker.setEmail("new@new.new");
	worker.setHashPass("pass".hashCode());
	worker.setAddress("new Address");
	workerDao.updateEntity(worker);
	databaseWorker = workerDao.getEntity(workerId);
	assertTrue(worker.equals(databaseWorker));

	workerDao.deleteEntity(workerId);
	databaseWorker = workerDao.getEntity(workerId);
	assertTrue(databaseWorker == null);
    }

    @Test
    public void crudePatientTest()throws DaoException{
	int workerId = workerDao.addEntity(worker);
	patient.setDoctorId(workerId);

	int patientId = patientDao.addEntity(patient);
	patient.setPatientId(patientId);
	Patient databasePatient = patientDao.getEntity(patientId);
	assertTrue(patient.equals(databasePatient));

	patient.setOutDate(Date.valueOf("2015-05-10"));
	patient.setFinalDiagnosis("definitely something wrong");
	patient.setFullName("new full name");
	patient.setAddress("new testAddress");
	patient.setStatus(PatientStatus.RECOVERED);
	patient.setInitDiagnosis("smthWrong !!!");
	patient.setInDate(Date.valueOf("2015-04-01"));
	patientDao.updateEntity(patient);
	databasePatient = patientDao.getEntity(patientId);
	assertTrue(patient.equals(databasePatient));

	patientDao.deleteEntity(patientId);
	databasePatient = patientDao.getEntity(patientId);
	assertTrue(databasePatient == null);

	workerDao.deleteEntity(workerId);
    }

    @Test
    public void crudePrescriptionTest()throws DaoException {
	int workerId = workerDao.addEntity(worker);
	patient.setDoctorId(workerId);
    	int patientId = patientDao.addEntity(patient);
	prescription.setWorkerId(workerId);
	prescription.setPatientId(patientId);

	int prescriptionId = prescriptionDao.addEntity(prescription);
	prescription.setPrescriptionId(prescriptionId);
	Prescription databasePrescription = prescriptionDao.getEntity(prescriptionId);
	assertTrue(prescription.equals(databasePrescription));

	prescription.setContent("new Content");
	prescription.setDate(Date.valueOf("2016-05-10"));
	prescription.setStatus(PrescriptionStatus.DONE);
	prescription.setType(PrescriptionType.DRUG_THERAPY);
	prescriptionDao.updateEntity(prescription);
	databasePrescription = prescriptionDao.getEntity(prescriptionId);
	assertTrue(prescription.equals(databasePrescription));

	prescriptionDao.deleteEntity(prescriptionId);
	databasePrescription = prescriptionDao.getEntity(prescriptionId);
	assertTrue(databasePrescription == null);

	patientDao.deleteEntity(patientId);
	workerDao.deleteEntity(workerId);
    }

}

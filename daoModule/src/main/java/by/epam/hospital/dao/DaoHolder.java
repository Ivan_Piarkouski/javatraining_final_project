package by.epam.hospital.dao;

import by.epam.hospital.dao.impl.PatientDaoImpl;
import by.epam.hospital.dao.impl.PrescriptionDaoImpl;
import by.epam.hospital.dao.impl.WorkerDaoImpl;

public abstract class DaoHolder {

    public static IWorkerDao getWorkerDao(){
	return WorkerDaoImpl.getInstance();
    }
    public static IPatientDao getPatientDao(){
	return PatientDaoImpl.getInstance();
    }
    public static IPrescriptionDao getPrescriptionDao(){return PrescriptionDaoImpl.getInstance();}
}

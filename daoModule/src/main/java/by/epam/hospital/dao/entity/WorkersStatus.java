package by.epam.hospital.dao.entity;

public enum WorkersStatus {

    IS_WORKING, LEFT, ON_VACATION
}

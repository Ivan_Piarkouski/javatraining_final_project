package by.epam.hospital.dao.entity;

public enum PatientStatus {
    ON_TREATMENT, RECOVERED, DECEASED
}

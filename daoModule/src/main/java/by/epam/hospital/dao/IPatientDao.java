package by.epam.hospital.dao;

import by.epam.hospital.dao.exception.DaoException;
import by.epam.hospital.dao.entity.Patient;

import java.util.List;


public interface IPatientDao extends IDao<Patient>{

    /**
     * method returning list of patients, who currently are on treatment
     *
     * @param filter filtrates results by full name
     * @param limitFirst index of row started from which rows will be included in result
     * @param itemsNumber number of rows included in result
     * @return List<Patient> object, if there are no patients satisfying the params returns empty list
     * @throws by.epam.hospital.dao.exception.DaoException
     */
    List<Patient> getCurrentPatientList(String filter, int limitFirst, int itemsNumber) throws DaoException;

    /**
     * method returning count of patients, who currently are on treatment and satisfy given filter
     *
     * @param filter filtrates results by full name
     * @return int count of patients, which full name satisfy filter restriction
     * @throws by.epam.hospital.dao.exception.DaoException
     */
    int getCurrentPatientCount(String filter) throws DaoException;

    /**
     * method returning list of patients, who currently are on treatment and are treated by the doctor with the given id
     *
     * @param doctorId treating doctor's id
     * @param filter filtrates results by full name
     * @param limitFirst index of row started from which rows will be included in result
     * @param itemsPerPage number of rows included in result
     * @return List<Patient> object, if there are no patients satisfying the params returns empty list
     * @throws by.epam.hospital.dao.exception.DaoException
     */
    List<Patient> getDoctorsCurrentPatients(int doctorId, String filter, int limitFirst, int itemsPerPage) throws DaoException;

    /**
     * method returning count of patients, who currently are on treatment and satisfy given filter and doctorId
     *
     * @param doctorId treating doctor's id
     * @param filter filtrates results by full name
     * @return int count of patients, who satisfy given params
     * @throws by.epam.hospital.dao.exception.DaoException
     */
    int getDoctorsCurrentPatientsCount(int doctorId, String filter) throws DaoException;

    /**
     * method returning full name of patient by id
     *
     * @param id patient id
     * @return String patient full name, if there are no patients with such id returns null
     * @throws by.epam.hospital.dao.exception.DaoException
     */
    String getPatientFullNameById(int id) throws DaoException;
}

package by.epam.hospital.dao.entity;

public enum WorkersPosition {
    NURSE, DOCTOR, CHIEF_DOCTOR
}

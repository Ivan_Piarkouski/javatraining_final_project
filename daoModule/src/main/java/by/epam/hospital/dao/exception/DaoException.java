package by.epam.hospital.dao.exception;


public class DaoException extends Exception {

    public DaoException(DaoExceptionMessage exceptionMessage) {
	super(exceptionMessage.getErrorMessage());
    }

    public DaoException(DaoExceptionMessage exceptionMessage, Throwable cause) {
	super(exceptionMessage.getErrorMessage(), cause);
    }
}

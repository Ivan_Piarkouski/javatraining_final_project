package by.epam.hospital.dao.impl;

import by.epam.hospital.dao.IPrescriptionDao;
import by.epam.hospital.dao.exception.DaoException;
import by.epam.hospital.dao.exception.DaoExceptionMessage;
import by.epam.hospital.dao.entity.Prescription;
import by.epam.hospital.dao.entity.PrescriptionStatus;
import by.epam.hospital.dao.entity.PrescriptionType;
import by.epam.hospital.dao.util.ConnectionPull;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class PrescriptionDaoImpl implements IPrescriptionDao {

    private final static PrescriptionDaoImpl INSTANCE = new PrescriptionDaoImpl();

    private final static String SQL_STATUSES_BY_PATIENT_ID = "SELECT F_PRESCRIPTION_STATUS FROM t_prescription JOIN " +
	"t_prescription_status ps ON F_PRESCRIPTION_STATUS_ID = ps.F_ID WHERE F_PATIENT_ID = ?";
    private final static String SQL_COUNT_ASSIGNED_PRESCRIPTIONS_FOR_NURSE = "SELECT count(*) FROM t_prescription p " +
	"JOIN t_prescription_status ps ON F_PRESCRIPTION_STATUS_ID = ps.F_ID JOIN t_prescription_type pt ON " +
	"F_PRESCRIPTION_TYPE_ID = pt.F_ID JOIN t_patient pat ON F_PATIENT_ID = pat.F_ID WHERE F_PRESCRIPTION_STATUS =" +
	" 'ASSIGNED' AND F_TYPE <> 'OPERATION' AND F_FULLNAME LIKE ?";
    private final static String SQL_ASSIGNED_PRESCRIPTIONS_FOR_NURSE = "SELECT p.F_ID, F_CONTENT, F_WORKER_ID, F_PATIENT_ID," +
	" F_DATE, F_TYPE, F_PRESCRIPTION_STATUS FROM t_prescription p JOIN t_prescription_status ps ON F_PRESCRIPTION_STATUS_ID" +
	" = ps.F_ID JOIN t_prescription_type pt ON F_PRESCRIPTION_TYPE_ID = pt.F_ID JOIN t_patient pat ON F_PATIENT_ID =" +
	" pat.F_ID WHERE F_PRESCRIPTION_STATUS = 'ASSIGNED' AND F_TYPE <> 'OPERATION' AND F_FULLNAME LIKE ? ORDER BY " +
	"F_PATIENT_ID LIMIT ?, ?";
    private final static String SQL_UNDONE_OPERATIONS_BY_DOCTOR = "SELECT p.F_ID, F_CONTENT, F_WORKER_ID, F_PATIENT_ID," +
	" F_DATE, F_TYPE, F_PRESCRIPTION_STATUS FROM t_prescription p JOIN t_prescription_status ps ON F_PRESCRIPTION_STATUS_ID" +
	" = ps.F_ID JOIN t_prescription_type pt ON F_PRESCRIPTION_TYPE_ID = pt.F_ID JOIN t_patient pat ON F_PATIENT_ID = pat.F_ID WHERE F_PRESCRIPTION_STATUS =" +
	" 'ASSIGNED' AND F_TYPE = 'OPERATION' AND F_WORKER_ID = ? AND F_FULLNAME LIKE ? ORDER BY F_PATIENT_ID LIMIT ?, ?";
    private final static String SQL_COUNT_OPERATIONS_BY_DOCTOR = "SELECT count(*) FROM t_prescription p JOIN t_prescription_status ps " +
	"ON F_PRESCRIPTION_STATUS_ID = ps.F_ID JOIN t_prescription_type pt ON F_PRESCRIPTION_TYPE_ID = pt.F_ID JOIN " +
	"t_patient pat ON F_PATIENT_ID = pat.F_ID WHERE F_PRESCRIPTION_STATUS = 'ASSIGNED' AND F_TYPE = 'OPERATION' " +
	"AND F_WORKER_ID = ? AND F_FULLNAME LIKE ?";
    private final static String SQL_ADD_PRESCRIPTION = "INSERT INTO t_prescription (F_CONTENT, F_DATE, F_WORKER_ID, " +
	"F_PATIENT_ID, F_PRESCRIPTION_TYPE_ID, F_PRESCRIPTION_STATUS_ID) VALUES (?, ?, ?, ?, ?, ?)";
    private final static String SQL_DELETE_PRESCRIPTION_BY_PATIENT = "DELETE FROM t_prescription WHERE F_PATIENT_ID = ?";
    private final static String SQL_STATUS_ID_BY_NAME = "SELECT F_ID FROM t_prescription_status WHERE F_PRESCRIPTION_STATUS = ?";
    private final static String SQL_TYPE_ID_BY_NAME = "SELECT F_ID FROM t_prescription_type WHERE F_TYPE = ?";
    private final static String SQL_PRESCRIPTIONS_BY_PATIENT = "SELECT p.F_ID, F_CONTENT, F_WORKER_ID, F_PATIENT_ID," +
	" F_DATE, F_TYPE, F_PRESCRIPTION_STATUS FROM t_prescription p JOIN t_prescription_status ps ON F_PRESCRIPTION_STATUS_ID" +
	" = ps.F_ID JOIN t_prescription_type pt ON F_PRESCRIPTION_TYPE_ID = pt.F_ID WHERE F_PATIENT_ID = ?";
    private final static String SQL_GET_PRESCRIPTION = "SELECT p.F_ID, F_CONTENT, F_WORKER_ID, F_PATIENT_ID, F_DATE, " +
	"F_TYPE, F_PRESCRIPTION_STATUS FROM t_prescription p JOIN t_prescription_status ps ON F_PRESCRIPTION_STATUS_ID =" +
	" ps.F_ID JOIN t_prescription_type pt ON F_PRESCRIPTION_TYPE_ID = pt.F_ID WHERE p.F_ID = ?";
    private final static String SQL_DELETE_PRESCRIPTION = "DELETE FROM t_prescription WHERE F_ID = ?";
    private final static String SQL_GET_PATIENT_ID_BY_PRESCRIPTION = "SELECT F_PATIENT_ID FROM t_prescription WHERE F_ID = ?";
    private final static String SQL_UPDATE_PRESCRIPTION = "UPDATE t_prescription SET F_CONTENT = ?, F_WORKER_ID = ?, F_DATE = ?," +
	"F_PRESCRIPTION_TYPE_ID = ?, F_PRESCRIPTION_STATUS_ID = ?, F_PATIENT_ID = ? WHERE F_ID = ?";
    private final static String COLUMN_NAME_PRESCRIPTION_STATUS = "F_PRESCRIPTION_STATUS";
    private final static String COLUMN_NAME_ID = "F_ID";
    private final static String COLUMN_NAME_CONTENT = "F_CONTENT";
    private final static String COLUMN_NAME_WORKER_ID = "F_WORKER_ID";
    private final static String COLUMN_NAME_PATIENT_ID = "F_PATIENT_ID";
    private final static String COLUMN_NAME_DATE = "F_DATE";
    private final static String COLUMN_NAME_TYPE = "F_TYPE";

    private final ConnectionPull pull = ConnectionPull.getInstance();

    private PrescriptionDaoImpl() {
    }

    public static PrescriptionDaoImpl getInstance() {
	return INSTANCE;
    }

    @Override
    public int addEntity(Prescription prescription) throws DaoException {
	int id;
	String sql = SQL_ADD_PRESCRIPTION;
	try (Connection connection = pull.getConnection();
	     PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
	    preparedStatement.setString(1, prescription.getContent());
	    preparedStatement.setDate(2, prescription.getDate());
	    preparedStatement.setInt(3, prescription.getWorkerId());
	    preparedStatement.setInt(4, prescription.getPatientId());
	    preparedStatement.setInt(5, getPrescriptionTypeId(prescription.getType(), connection));
	    preparedStatement.setInt(6, getPrescriptionStatusId(prescription.getStatus(), connection));
	    preparedStatement.executeUpdate();
	    ResultSet resultSet = preparedStatement.getGeneratedKeys();
	    resultSet.next();
	    id = resultSet.getInt(1);
	} catch (SQLException e) {
	    throw new DaoException(DaoExceptionMessage.ERROR_PRESCRIPTION_ADDING, e);
	}
	return id;
    }

    @Override
    public void updateEntity(Prescription prescription) throws DaoException {
	String sql = SQL_UPDATE_PRESCRIPTION;
	try (Connection connection = pull.getConnection();
	     PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
	    preparedStatement.setString(1, prescription.getContent());
	    preparedStatement.setInt(2, prescription.getWorkerId());
	    preparedStatement.setDate(3, prescription.getDate());
	    preparedStatement.setInt(4, getPrescriptionTypeId(prescription.getType(), connection));
	    preparedStatement.setInt(5, getPrescriptionStatusId(prescription.getStatus(), connection));
	    preparedStatement.setInt(6, prescription.getPatientId());
	    preparedStatement.setInt(7, prescription.getPrescriptionId());
	    preparedStatement.executeUpdate();
	} catch (SQLException e) {
	    throw new DaoException(DaoExceptionMessage.ERROR_PRESCRIPTION_UPDATING, e);
	}
    }

    @Override
    public Prescription getEntity(int prescriptionId) throws DaoException {
	Prescription prescription = null;
	String sql = SQL_GET_PRESCRIPTION;
	try (Connection connection = pull.getConnection();
	     PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
	    preparedStatement.setInt(1, prescriptionId);
	    ResultSet resultSet = preparedStatement.executeQuery();
	    if (resultSet.next()) {
		prescription = getFilledPrescription(resultSet);
	    }
	} catch (SQLException e) {
	    throw new DaoException(DaoExceptionMessage.ERROR_PRESCRIPTIONS_BY_PATIENT_GETTING, e);
	}
	return prescription;
    }

    @Override
    public void deleteEntity(int prescriptionId) throws DaoException {
	String sql = SQL_DELETE_PRESCRIPTION;
	try (Connection connection = pull.getConnection();
	     PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
	    preparedStatement.setInt(1, prescriptionId);
	    preparedStatement.executeUpdate();
	} catch (SQLException e) {
	    throw new DaoException(DaoExceptionMessage.ERROR_PRESCRIPTION_DELETING, e);
	}
    }

    private Prescription getFilledPrescription(ResultSet resultSet) throws SQLException {
	Prescription prescription = new Prescription();
	prescription.setPrescriptionId(resultSet.getInt(COLUMN_NAME_ID));
	prescription.setContent(resultSet.getString(COLUMN_NAME_CONTENT));
	prescription.setPatientId(resultSet.getInt(COLUMN_NAME_PATIENT_ID));
	prescription.setWorkerId(resultSet.getInt(COLUMN_NAME_WORKER_ID));
	prescription.setDate(resultSet.getDate(COLUMN_NAME_DATE));
	prescription.setStatus(PrescriptionStatus.valueOf(resultSet.getString(COLUMN_NAME_PRESCRIPTION_STATUS)));
	prescription.setType(PrescriptionType.valueOf(resultSet.getString(COLUMN_NAME_TYPE)));
	return prescription;
    }

    private int getPrescriptionStatusId(PrescriptionStatus status, Connection connection) throws SQLException {
	String sql = SQL_STATUS_ID_BY_NAME;
	PreparedStatement preparedStatement = connection.prepareStatement(sql);
	preparedStatement.setString(1, status.name());
	ResultSet resultSet = preparedStatement.executeQuery();
	resultSet.next();
	return resultSet.getInt(COLUMN_NAME_ID);
    }

    private int getPrescriptionTypeId(PrescriptionType type, Connection connection) throws SQLException {
	String sql = SQL_TYPE_ID_BY_NAME;
	PreparedStatement preparedStatement = connection.prepareStatement(sql);
	preparedStatement.setString(1, type.name());
	ResultSet resultSet = preparedStatement.executeQuery();
	resultSet.next();
	return resultSet.getInt(COLUMN_NAME_ID);
    }


    @Override
    public List<PrescriptionStatus> getPrescriptionStatusesByPatientId(int patientId) throws DaoException {
	List<PrescriptionStatus> statuses = new ArrayList<>();
	String sql = SQL_STATUSES_BY_PATIENT_ID;
	try (Connection connection = pull.getConnection();
	     PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
	    preparedStatement.setInt(1, patientId);
	    ResultSet resultSet = preparedStatement.executeQuery();
	    while (resultSet.next()) {
		String prescriptionStatus = resultSet.getString(COLUMN_NAME_PRESCRIPTION_STATUS);
		PrescriptionStatus status = PrescriptionStatus.valueOf(prescriptionStatus);
		statuses.add(status);
	    }
	} catch (SQLException e) {
	    throw new DaoException(DaoExceptionMessage.ERROR_PATIENT_STATUSES_BY_ID_GETTING, e);
	}
	return statuses;
    }

    @Override
    public List<Prescription> getAssignedNursePrescriptionList(String filter, int limitFirst, int itemsPerPage) throws DaoException {
	List<Prescription> prescriptions = new ArrayList<>();
	String sql = SQL_ASSIGNED_PRESCRIPTIONS_FOR_NURSE;
	try (Connection connection = pull.getConnection();
	     PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
	    preparedStatement.setString(1, filter);
	    preparedStatement.setInt(2, limitFirst);
	    preparedStatement.setInt(3, itemsPerPage);
	    ResultSet resultSet = preparedStatement.executeQuery();
	    while (resultSet.next()) {
		Prescription prescription = getFilledPrescription(resultSet);
		prescriptions.add(prescription);
	    }
	} catch (SQLException e) {
	    throw new DaoException(DaoExceptionMessage.ERROR_PRESCRIPTIONS_FOR_NURSE_GETTING, e);
	}
	return prescriptions;
    }

    @Override
    public int getNursePrescriptionCount(String filter) throws DaoException {
	int operationCount;
	String sql = SQL_COUNT_ASSIGNED_PRESCRIPTIONS_FOR_NURSE;
	try (Connection connection = pull.getConnection();
	     PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
	    preparedStatement.setString(1, filter);
	    ResultSet resultSet = preparedStatement.executeQuery();
	    resultSet.next();
	    operationCount = resultSet.getInt(1);
	} catch (SQLException e) {
	    throw new DaoException(DaoExceptionMessage.ERROR_PRESCRIPTION_COUNT_FOR_NURSE_GETTING, e);
	}
	return operationCount;
    }

    @Override
    public List<Prescription> getDoctorsOperationList(int doctorId, String filter, int limitFirst, int itemsPerPage)
	throws DaoException {
	List<Prescription> operations = new ArrayList<>();
	String sql = SQL_UNDONE_OPERATIONS_BY_DOCTOR;
	try (Connection connection = pull.getConnection();
	     PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
	    preparedStatement.setInt(1, doctorId);
	    preparedStatement.setString(2, filter);
	    preparedStatement.setInt(3, limitFirst);
	    preparedStatement.setInt(4, itemsPerPage);
	    ResultSet resultSet = preparedStatement.executeQuery();
	    while (resultSet.next()) {
		Prescription operation = getFilledPrescription(resultSet);
		operations.add(operation);
	    }
	} catch (SQLException e) {
	    throw new DaoException(DaoExceptionMessage.ERROR_PRESCRIPTION_OPERATIONS_BY_DOCTOR_GETTING, e);
	}
	return operations;
    }

    @Override
    public int getDoctorsOperationsCount(int doctorId, String filter) throws DaoException {
	int operationCount;
	String sql = SQL_COUNT_OPERATIONS_BY_DOCTOR;
	try (Connection connection = pull.getConnection();
	     PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
	    preparedStatement.setInt(1, doctorId);
	    preparedStatement.setString(2, filter);
	    ResultSet resultSet = preparedStatement.executeQuery();
	    resultSet.next();
	    operationCount = resultSet.getInt(1);
	} catch (SQLException e) {
	    throw new DaoException(DaoExceptionMessage.ERROR_PRESCRIPTION_OPERATION_COUNT_BY_DOCTOR_GETTING, e);
	}
	return operationCount;
    }

    @Override
    public void deletePrescriptionsByPatient(int patientId) throws DaoException {
	String sql = SQL_DELETE_PRESCRIPTION_BY_PATIENT;
	try (Connection connection = pull.getConnection();
	     PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
	    preparedStatement.setInt(1, patientId);
	    preparedStatement.executeUpdate();
	} catch (SQLException e) {
	    throw new DaoException(DaoExceptionMessage.ERROR_PRESCRIPTIONS_DELETING_BY_PATIENT, e);
	}
    }

    @Override
    public List<Prescription> getPatientsPrescriptions(int patientId) throws DaoException {
	List<Prescription> patientsPrescriptions = new ArrayList<>();
	String sql = SQL_PRESCRIPTIONS_BY_PATIENT;
	try (Connection connection = pull.getConnection();
	     PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
	    preparedStatement.setInt(1, patientId);
	    ResultSet resultSet = preparedStatement.executeQuery();
	    while (resultSet.next()) {
		Prescription prescription = getFilledPrescription(resultSet);
		patientsPrescriptions.add(prescription);
	    }
	} catch (SQLException e) {
	    throw new DaoException(DaoExceptionMessage.ERROR_PRESCRIPTIONS_BY_PATIENT_GETTING, e);
	}
	return patientsPrescriptions;
    }

    @Override
    public int getPatientIdByPrescription(int prescriptionId) throws DaoException {
	int patientId;
	String sql = SQL_GET_PATIENT_ID_BY_PRESCRIPTION;
	try (Connection connection = pull.getConnection();
	     PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
	    preparedStatement.setInt(1, prescriptionId);
	    ResultSet resultSet = preparedStatement.executeQuery();
	    resultSet.next();
	    patientId = resultSet.getInt(COLUMN_NAME_PATIENT_ID);
	} catch (SQLException e) {
	    throw new DaoException(DaoExceptionMessage.ERROR_PRESCRIPTION_DELETING, e);
	}
	return patientId;
    }

}

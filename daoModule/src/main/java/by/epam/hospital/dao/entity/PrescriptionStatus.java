package by.epam.hospital.dao.entity;

public enum PrescriptionStatus {
    ASSIGNED, DONE
}

package by.epam.hospital.dao.entity;

import java.io.Serializable;
import java.sql.Date;

public class Patient implements Serializable {

    private final static long serialVersionUID = 1L;

    private int patientId;
    private String fullName;
    private String address;
    private PatientStatus status;
    private String initDiagnosis;
    private String finalDiagnosis;
    private int doctorId;
    private Date inDate;
    private Date outDate;

    public int getPatientId() {
	return patientId;
    }

    public void setPatientId(int patientId) {
	this.patientId = patientId;
    }

    public String getFullName() {
	return fullName;
    }

    public void setFullName(String fullName) {
	this.fullName = fullName;
    }

    public String getAddress() {
	return address;
    }

    public void setAddress(String address) {
	this.address = address;
    }

    public PatientStatus getStatus() {
	return status;
    }

    public void setStatus(PatientStatus status) {
	this.status = status;
    }

    public String getInitDiagnosis() {
	return initDiagnosis;
    }

    public void setInitDiagnosis(String initDiagnosis) {
	this.initDiagnosis = initDiagnosis;
    }

    public String getFinalDiagnosis() {
	return finalDiagnosis;
    }

    public void setFinalDiagnosis(String finalDiagnosis) {
	this.finalDiagnosis = finalDiagnosis;
    }

    public int getDoctorId() {
	return doctorId;
    }

    public void setDoctorId(int doctorId) {
	this.doctorId = doctorId;
    }

    public Date getInDate() {
	return inDate;
    }

    public void setInDate(Date inDate) {
	this.inDate = inDate;
    }

    public Date getOutDate() {
	return outDate;
    }

    public void setOutDate(Date outDate) {
	this.outDate = outDate;
    }

    @Override
    public boolean equals(Object o) {
	if (this == o) {return true;}
	if (o == null || getClass() != o.getClass()) {return false;}

	Patient patient = (Patient) o;

	if (doctorId != patient.doctorId) {return false;}
	if (patientId != patient.patientId) {return false;}
	if (address != null ? !address.equals(patient.address) : patient.address != null) {return false;}
	if (finalDiagnosis != null ? !finalDiagnosis.equals(patient.finalDiagnosis) : patient.finalDiagnosis != null)
		{return false;}
	if (fullName != null ? !fullName.equals(patient.fullName) : patient.fullName != null) {return false;}
	if (inDate != null ? !inDate.equals(patient.inDate) : patient.inDate != null) {return false;}
	if (initDiagnosis != null ? !initDiagnosis.equals(patient.initDiagnosis) : patient.initDiagnosis != null)
		{return false;}
	if (outDate != null ? !outDate.equals(patient.outDate) : patient.outDate != null) {return false;}
	if (status != patient.status) {return false;}

	return true;
    }

    @Override
    public int hashCode() {
	int result = patientId;
	result = 31 * result + (fullName != null ? fullName.hashCode() : 0);
	result = 31 * result + (address != null ? address.hashCode() : 0);
	result = 31 * result + (status != null ? status.hashCode() : 0);
	result = 31 * result + (initDiagnosis != null ? initDiagnosis.hashCode() : 0);
	result = 31 * result + (finalDiagnosis != null ? finalDiagnosis.hashCode() : 0);
	result = 31 * result + doctorId;
	result = 31 * result + (inDate != null ? inDate.hashCode() : 0);
	result = 31 * result + (outDate != null ? outDate.hashCode() : 0);
	return result;
    }

    @Override
    public String toString() {
	return "Patient{" +
	    "patientId=" + patientId +
	    ", fullName='" + fullName + '\'' +
	    ", address='" + address + '\'' +
	    ", status=" + status +
	    ", initDiagnosis='" + initDiagnosis + '\'' +
	    ", finalDiagnosis='" + finalDiagnosis + '\'' +
	    ", doctorId=" + doctorId +
	    ", inDate=" + inDate +
	    ", outDate=" + outDate +
	    '}';
    }
}

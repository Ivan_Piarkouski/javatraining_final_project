package by.epam.hospital.dao;

import by.epam.hospital.dao.exception.DaoException;
import by.epam.hospital.dao.entity.Prescription;
import by.epam.hospital.dao.entity.PrescriptionStatus;

import java.util.List;

public interface IPrescriptionDao extends IDao<Prescription>{

    /**
     * returns collection of prescription statuses of a patient with given id
     *
     * @param patientId - patient id
     * @return List<PrescriptionStatus> if the are no prescriptions for the given patient id, returns empty list
     * @throws by.epam.hospital.dao.exception.DaoException
     */
    List<PrescriptionStatus> getPrescriptionStatusesByPatientId(int patientId) throws DaoException;

    /**
     * returns all undone prescriptions except operations sorted by patient id
     *
     * @param filter filtrates results by patient full name
     * @param limitFirst index of row started from which rows will be included in result, not included
     * @param itemsPerPage number of rows included in result
     * @return List<Prescription>, if the are no prescriptions satisfying parameters, returns empty list
     * @throws by.epam.hospital.dao.exception.DaoException
     */
    List<Prescription> getAssignedNursePrescriptionList(String filter, int limitFirst, int itemsPerPage) throws DaoException;

    /**
     * returns all undone operations of the given doctor (id) sorted by patient id
     *
     * @param doctorId - doctor id
     * @param filter filtrates results by patient full name
     * @param limitFirst index of row started from which rows will be included in result, not included
     * @param itemsPerPage number of rows included in result
     * @return List<Prescription>, if the are no prescriptions satisfying given parameters, returns empty list
     * @throws by.epam.hospital.dao.exception.DaoException
     */
    List<Prescription> getDoctorsOperationList(int doctorId, String filter, int limitFirst, int itemsPerPage) throws DaoException;

    /**
     * deletes all prescription of the given patient (id), if there are no such patient or patient don't have prescriptions
     * - does nothing
     *
     * @param patientId - patient id
     * @throws by.epam.hospital.dao.exception.DaoException
     */
    void deletePrescriptionsByPatient(int patientId) throws DaoException;

    /**
     * returns all prescription of the given patient (id)
     *
     * @param patientId - patient id
     * @return List<Prescription>, if patient don't have prescriptions or there are no such patient, returns empty list
     * @throws by.epam.hospital.dao.exception.DaoException
     */
    List<Prescription> getPatientsPrescriptions(int patientId) throws DaoException;

    /**
     * returns patient id using prescription id
     *
     * @param prescriptionId - prescription id
     * @return int patient id, or null if there is no such prescription
     * @throws by.epam.hospital.dao.exception.DaoException
     */
    int getPatientIdByPrescription(int prescriptionId) throws DaoException;

    /**
     * returns count of undone operations of the given doctor (id)
     *
     * @param doctorId - doctor id
     * @param filter filtrates counted operations by patient full name
     * @return int operation count or 0 if the are no prescriptions satisfying given parameters
     * @throws by.epam.hospital.dao.exception.DaoException
     */
    int getDoctorsOperationsCount(int doctorId, String filter) throws DaoException;

    /**
     * returns count of undone prescriptions  except operations
     *
     * @param filter filtrates counted operations by patient full name
     * @return int operation count or 0 if the are no prescriptions satisfying given parameters
     * @throws by.epam.hospital.dao.exception.DaoException
     */
    int getNursePrescriptionCount(String filter) throws DaoException;

}

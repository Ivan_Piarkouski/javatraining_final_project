package by.epam.hospital.dao.util;


import by.epam.hospital.dao.exception.DaoException;
import by.epam.hospital.dao.exception.DaoExceptionMessage;
import org.apache.log4j.Logger;

import java.sql.Array;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.NClob;
import java.sql.PreparedStatement;
import java.sql.SQLClientInfoException;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Savepoint;
import java.sql.Statement;
import java.sql.Struct;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.locks.ReentrantLock;

/**
 * class with a synchronized que of ready to use connections, has inner class PooledConnection, responsible for opening
 * and closing connections
 */
public class ConnectionPull {	//TODO think about refactoring

    private final static String DB_DRIVER = "db.driver";
    private final static String DB_USER = "db.user";
    private final static String DB_PASS = "db.pass";
    private final static String DB_URL = "db.url";
    private final static String DB_POOL_SIZE = "db.poolsize";
    private final static Logger log = Logger.getLogger(ConnectionPull.class);
    private final static ReentrantLock LOCK = new ReentrantLock();
    private static volatile ConnectionPull instance;
    private final String driverName;
    private final String dbUrl;
    private final String dbUser;
    private final String dbPass;
    private final int poolSize;

    private BlockingQueue<Connection> connectionQueue;
    private BlockingQueue<Connection> givenAwayConQueue;

    public static ConnectionPull getInstance(){
	if (instance== null){
	    try {
		LOCK.lock();
		if (instance == null) {
		    instance = new ConnectionPull();
		}
	    }finally {
		LOCK.unlock();
	    }
	}
	return instance;
    }

    /**
     * constructor initializing all parameters need to connect to database
     */
    private ConnectionPull(){
	DBSettingsManager settingsManager = DBSettingsManager.getInstance();
	driverName = settingsManager.getValue(DB_DRIVER);
	dbUrl = settingsManager.getValue(DB_URL);
	dbUser = settingsManager.getValue(DB_USER);
	dbPass = settingsManager.getValue(DB_PASS);
	poolSize = Integer.parseInt(settingsManager.getValue(DB_POOL_SIZE));
    }

    /**
     * method filling pull with number of connections written in db settings
     */
    public void initPool (){
	try{
	    Class.forName(driverName);
	    connectionQueue = new ArrayBlockingQueue<>(poolSize);
	    givenAwayConQueue = new ArrayBlockingQueue<>(poolSize);
	    for(int i = 0; i<poolSize; i++){
		Connection connection = DriverManager.getConnection(dbUrl, dbUser, dbPass);
		PooledConnection pooledConnection = new PooledConnection(connection);
		connectionQueue.add(pooledConnection);
	    }
	}catch (ClassNotFoundException | SQLException e){
	    log.error(e);
	}
    }

    /**
     * method trying to retrieve connection from connectionQueue and if a success relocates it to given givenAwayConQueue,
     * if there are no connection available  - waits till free connection appears or till InterruptedException occurs
     *
     * @return Connection object
     * @throws by.epam.hospital.dao.exception.DaoException
     */
    public Connection getConnection() throws DaoException {
	Connection connection;
	try {
	    connection = connectionQueue.take();
	    givenAwayConQueue.add(connection);
	}catch (InterruptedException e){
	    throw new DaoException(DaoExceptionMessage.ERROR_WAITING_CONNECTION, e);
	}
	return connection;
    }

    /**
     * private method for closing connection queues
     *
     * @param queue -queue of connections to be closed
     */
    private void closeConnectionQueue(BlockingQueue<Connection> queue){
	while (!queue.isEmpty()){
	    try {
		Connection connection = queue.poll();
		if(!connection.getAutoCommit()){
		    connection.commit();
		}
		((PooledConnection)connection).closeFinally();
	    }catch (SQLException e){
		log.error(e);
	    }
	}
    }

    /**
     * method closing pull, when application shut down
     */
    public void closePull(){
	closeConnectionQueue(connectionQueue);
	closeConnectionQueue(givenAwayConQueue);
    }

    /**
     * wrapper class for connection overriding  close() method, to prevent unintended connection close,
     * and to use auto closable function of try-catch block for returning connection to the pull
     */
    private class PooledConnection implements Connection{
	private final Connection connection;

	public PooledConnection(Connection connection) throws SQLException{
	    this.connection = connection;
	    connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
	}

	/**
	 * method closing wrapped connection
	 * @throws java.sql.SQLException
	 */
	public void closeFinally() throws SQLException{
	    connection.close();
	}

	@Override
	public void close() throws SQLException {
		if(connection.isClosed()){
		    throw new SQLException("Trying to close closed connection");
		}
	    if(connection.isReadOnly()){
		connection.setReadOnly(false);
	    }
	    if(!connection.getAutoCommit()){
		connection.setAutoCommit(true);
	    }
	    if(!givenAwayConQueue.remove(this)){
		throw new SQLException("Error deleting connection from the givenAwayConQueue");
	    }
	    if (!connectionQueue.offer(this)){
		throw new SQLException("Error allocating connection in the pool");

	    }
	}

	@Override
	public Statement createStatement() throws SQLException {
	    return connection.createStatement();
	}

	@Override
	public PreparedStatement prepareStatement(String sql) throws SQLException {
	    return connection.prepareStatement(sql);
	}

	@Override
	public CallableStatement prepareCall(String sql) throws SQLException {
	    return connection.prepareCall(sql);
	}

	@Override
	public String nativeSQL(String sql) throws SQLException {
	    return connection.nativeSQL(sql);
	}

	@Override
	public void setAutoCommit(boolean autoCommit) throws SQLException {
	    connection.setAutoCommit(autoCommit);
	}

	@Override
	public boolean getAutoCommit() throws SQLException {
	    return connection.getAutoCommit();
	}

	@Override
	public void commit() throws SQLException {
	    connection.commit();
	}

	@Override
	public void rollback() throws SQLException {
	    connection.rollback();
	}

	@Override
	public boolean isClosed() throws SQLException {
	    return connection.isClosed();
	}

	@Override
	public DatabaseMetaData getMetaData() throws SQLException {
	    return connection.getMetaData();
	}

	@Override
	public void setReadOnly(boolean readOnly) throws SQLException {
		connection.setReadOnly(readOnly);
	}

	@Override
	public boolean isReadOnly() throws SQLException {
	    return connection.isReadOnly();
	}

	@Override
	public void setCatalog(String catalog) throws SQLException {
		connection.setCatalog(catalog);
	}

	@Override
	public String getCatalog() throws SQLException {
	    return connection.getCatalog();
	}

	@Override
	public void setTransactionIsolation(int level) throws SQLException {
		connection.setTransactionIsolation(level);
	}

	@Override
	public int getTransactionIsolation() throws SQLException {
	    return connection.getTransactionIsolation();
	}

	@Override
	public SQLWarning getWarnings() throws SQLException {
	    return connection.getWarnings();
	}

	@Override
	public void clearWarnings() throws SQLException {
		connection.clearWarnings();
	}

	@Override
	public Statement createStatement(int resultSetType, int resultSetConcurrency) throws SQLException {
	    return connection.createStatement(resultSetType, resultSetConcurrency);
	}

	@Override
	public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency) throws SQLException {
	    return connection.prepareStatement(sql, resultSetType, resultSetConcurrency);
	}

	@Override
	public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency) throws SQLException {
	    return connection.prepareCall(sql, resultSetType, resultSetConcurrency);
	}

	@Override
	public Map<String, Class<?>> getTypeMap() throws SQLException {
	    return connection.getTypeMap();
	}

	@Override
	public void setTypeMap(Map<String, Class<?>> map) throws SQLException {
		connection.setTypeMap(map);
	}

	@Override
	public void setHoldability(int holdability) throws SQLException {
		connection.setHoldability(holdability);
	}

	@Override
	public int getHoldability() throws SQLException {
	    return connection.getHoldability();
	}

	@Override
	public Savepoint setSavepoint() throws SQLException {
	    return connection.setSavepoint();
	}

	@Override
	public Savepoint setSavepoint(String name) throws SQLException {
	    return connection.setSavepoint(name);
	}

	@Override
	public void rollback(Savepoint savepoint) throws SQLException {
		connection.rollback(savepoint);
	}

	@Override
	public void releaseSavepoint(Savepoint savepoint) throws SQLException {
		connection.releaseSavepoint(savepoint);
	}

	@Override
	public Statement createStatement(int resultSetType, int resultSetConcurrency, int resultSetHoldability)
									    throws SQLException {
	    return connection.createStatement(resultSetType, resultSetConcurrency, resultSetHoldability);
	}

	@Override
	public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency,
						  int resultSetHoldability) throws SQLException {
	    return connection.prepareStatement(sql, resultSetType, resultSetConcurrency,resultSetHoldability);
	}

	@Override
	public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency,
					     int resultSetHoldability) throws SQLException {
	    return connection.prepareCall(sql, resultSetType, resultSetConcurrency, resultSetHoldability);
	}

	@Override
	public PreparedStatement prepareStatement(String sql, int autoGeneratedKeys) throws SQLException {
	    return connection.prepareStatement(sql, autoGeneratedKeys);
	}

	@Override
	public PreparedStatement prepareStatement(String sql, int[] columnIndexes) throws SQLException {
	    return connection.prepareStatement(sql, columnIndexes);
	}

	@Override
	public PreparedStatement prepareStatement(String sql, String[] columnNames) throws SQLException {
	    return connection.prepareStatement(sql, columnNames);
	}

	@Override
	public Clob createClob() throws SQLException {
	    return connection.createClob();
	}

	@Override
	public Blob createBlob() throws SQLException {
	    return connection.createBlob();
	}

	@Override
	public NClob createNClob() throws SQLException {
	    return connection.createNClob();
	}

	@Override
	public SQLXML createSQLXML() throws SQLException {
	    return connection.createSQLXML();
	}

	@Override
	public boolean isValid(int timeout) throws SQLException {
	    return connection.isValid(timeout);
	}

	@Override
	public void setClientInfo(String name, String value) throws SQLClientInfoException {
		connection.setClientInfo(name, value);
	}

	@Override
	public void setClientInfo(Properties properties) throws SQLClientInfoException {
		connection.setClientInfo(properties);
	}

	@Override
	public String getClientInfo(String name) throws SQLException {
	    return connection.getClientInfo(name);
	}

	@Override
	public Properties getClientInfo() throws SQLException {
	    return connection.getClientInfo();
	}

	@Override
	public Array createArrayOf(String typeName, Object[] elements) throws SQLException {
	    return connection.createArrayOf(typeName, elements);
	}

	@Override
	public Struct createStruct(String typeName, Object[] attributes) throws SQLException {
	    return connection.createStruct(typeName, attributes);
	}

	@Override
	public void setSchema(String schema) throws SQLException {
		connection.setSchema(schema);
	}

	@Override
	public String getSchema() throws SQLException {
	    return connection.getSchema();
	}

	@Override
	public void abort(Executor executor) throws SQLException {
		connection.abort(executor);
	}

	@Override
	public void setNetworkTimeout(Executor executor, int milliseconds) throws SQLException {
		connection.setNetworkTimeout(executor, milliseconds);
	}

	@Override
	public int getNetworkTimeout() throws SQLException {
	    return connection.getNetworkTimeout();
	}

	@Override
	public <T> T unwrap(Class<T> iface) throws SQLException {
	    return connection.unwrap(iface);
	}

	@Override
	public boolean isWrapperFor(Class<?> iface) throws SQLException {
	    return connection.isWrapperFor(iface);
	}
    }
}

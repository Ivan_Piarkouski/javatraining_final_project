package by.epam.hospital.dao.impl;


import by.epam.hospital.dao.IWorkerDao;
import by.epam.hospital.dao.exception.DaoException;
import by.epam.hospital.dao.exception.DaoExceptionMessage;
import by.epam.hospital.dao.entity.Worker;
import by.epam.hospital.dao.entity.WorkersPosition;
import by.epam.hospital.dao.entity.WorkersStatus;
import by.epam.hospital.dao.util.ConnectionPull;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class WorkerDaoImpl implements IWorkerDao {

    private final static WorkerDaoImpl INSTANCE = new WorkerDaoImpl();
    private final static String COLUMN_NAME_ID = "F_ID";
    private final static String COLUMN_NAME_FULLNAME = "F_FULLNAME";
    private final static String COLUMN_NAME_EMAIL = "F_EMAIL";
    private final static String COLUMN_NAME_PASS = "F_PASS";
    private final static String COLUMN_NAME_POSITION = "F_POSITION";
    private final static String COLUMN_NAME_STATUS = "F_STATUS";
    private final static String COLUMN_NAME_ADDRESS = "F_ADDRESS";

    private final static String SQL_WORKER_BY_EMAIL = "SELECT w.F_ID, F_FULLNAME, F_EMAIL, F_PASS, F_POSITION, F_STATUS " +
	"FROM T_WORKER w JOIN T_POSITION p ON F_POSITION_ID = p.F_ID JOIN T_WORKER_STATUS s ON F_WORKER_STATUS_ID = s.F_ID " +
	"WHERE F_STATUS <> 'LEFT' and F_EMAIL = ?";
    private final static String SQL_WORKER_FULLNAME_BY_ID = "SELECT F_FULLNAME FROM T_WORKER WHERE F_ID = ?";
    private final static String SQL_CURRENT_DOCTOR_LIST = "SELECT w.F_ID, F_FULLNAME FROM T_WORKER w JOIN T_WORKER_STATUS " +
	"ws ON F_WORKER_STATUS_ID = ws.F_ID JOIN T_POSITION p ON F_POSITION_ID = p.F_ID WHERE (F_POSITION = 'DOCTOR' OR" +
	" F_POSITION = 'CHIEF_DOCTOR') AND F_STATUS = 'IS_WORKING'";
    private final static  String SQL_ADD_WORKER = "INSERT INTO T_WORKER (F_FULLNAME, F_EMAIL, F_ADDRESS, " +
	"F_POSITION_ID, F_WORKER_STATUS_ID, F_PASS) values (?, ?, ?, ?, ?, ?)";
    private final static  String SQL_GET_WORKER = "select w.F_ID, F_FULLNAME, F_EMAIL, F_ADDRESS, F_POSITION, F_STATUS, F_PASS " +
	"from t_worker w join t_position p on F_POSITION_ID = p.F_ID join t_worker_status s on F_WORKER_STATUS_ID = s.F_ID" +
	" where w.F_ID = ?";
    private final static String SQL_WORKER_UPDATE = "update t_worker set F_FULLNAME = ?, F_EMAIL = ?, F_ADDRESS = ?, " +
	"F_POSITION_ID = ?, F_WORKER_STATUS_ID = ?, F_PASS = ? where F_ID = ?";
    private final static String SQL_DELETE_WORKER = "delete from t_worker where F_ID = ?";
    private final static String SQL_GET_STATUS_ID = "SELECT F_ID FROM t_worker_status WHERE F_STATUS = ?";
    private final static String SQL_GET_POSITION_ID = "SELECT F_ID FROM t_position WHERE F_POSITION = ?";

    private final ConnectionPull pull = ConnectionPull.getInstance();

    private WorkerDaoImpl(){}

    public static WorkerDaoImpl getInstance(){
	return INSTANCE;
    }

    @Override
    public Worker getWorkerByEmail(String email) throws DaoException {
	Worker worker = null;
	String sql = SQL_WORKER_BY_EMAIL;
	try(Connection connection = pull.getConnection();
	    PreparedStatement preparedStatement = connection.prepareStatement(sql))
	{
	    preparedStatement.setString(1, email);
	    ResultSet resultSet = preparedStatement.executeQuery();
	    if (resultSet.next()){
		worker = new Worker();
		worker.setId(resultSet.getInt(COLUMN_NAME_ID));
		worker.setFullName(resultSet.getString(COLUMN_NAME_FULLNAME));
		worker.setEmail(resultSet.getString(COLUMN_NAME_EMAIL));
		worker.setHashPass(resultSet.getInt(COLUMN_NAME_PASS));
		worker.setPosition(WorkersPosition.valueOf(resultSet.getString(COLUMN_NAME_POSITION)));
		worker.setStatus(WorkersStatus.valueOf(resultSet.getString(COLUMN_NAME_STATUS)));
	    }
	}catch (SQLException e){
	    throw new DaoException(DaoExceptionMessage.ERROR_WORKER_GETTING_BY_EMAIL, e);
	}
	return worker;
    }

    @Override
    public String getWorkerFullName(int id) throws DaoException {
	String fullName = null;
	String sql = SQL_WORKER_FULLNAME_BY_ID;
	try(Connection connection = pull.getConnection();
	    PreparedStatement preparedStatement = connection.prepareStatement(sql))
	{
	    preparedStatement.setInt(1, id);
	    ResultSet resultSet = preparedStatement.executeQuery();
	    if(resultSet.next()){
		fullName=resultSet.getString(COLUMN_NAME_FULLNAME);
	    }
	}catch (SQLException e){
	    throw new DaoException(DaoExceptionMessage.ERROR_WORKER_FULL_NAME_BY_ID_GETTING, e);
	}
	return fullName;
    }

    @Override
    public List<Worker> getCurrentDoctorsList() throws DaoException {
	List<Worker> doctors = new ArrayList<>();
	String sql = SQL_CURRENT_DOCTOR_LIST;
	try(Connection connection = pull.getConnection();
	    PreparedStatement preparedStatement = connection.prepareStatement(sql))
	{
	    ResultSet resultSet = preparedStatement.executeQuery();
	    while(resultSet.next()){
		Worker doctor = new Worker();
		doctor.setId(resultSet.getInt(COLUMN_NAME_ID));
		doctor.setFullName(resultSet.getString(COLUMN_NAME_FULLNAME));
		doctors.add(doctor);
	    }
	}catch (SQLException e){
	    throw new DaoException(DaoExceptionMessage.ERROR_WORKER_DOCTORS_ACTUAL_LIST, e);
	}
	return doctors;
    }

    private Worker getFilledWorker(ResultSet resultSet) throws SQLException{
	Worker worker = new Worker();
	worker.setId(resultSet.getInt(COLUMN_NAME_ID));
	worker.setFullName(resultSet.getString(COLUMN_NAME_FULLNAME));
	worker.setEmail(resultSet.getString(COLUMN_NAME_EMAIL));
	worker.setAddress(resultSet.getString(COLUMN_NAME_ADDRESS));
	worker.setPosition(WorkersPosition.valueOf(resultSet.getString(COLUMN_NAME_POSITION)));
	worker.setStatus(WorkersStatus.valueOf(resultSet.getString(COLUMN_NAME_STATUS)));
	worker.setHashPass(resultSet.getInt(COLUMN_NAME_PASS));
	return worker;
    }

    @Override
    public Worker getEntity(int id) throws DaoException {
	String sql = SQL_GET_WORKER;
	Worker worker = null;
	try (Connection connection = pull.getConnection();
	     PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
	    preparedStatement.setInt(1, id);
	    ResultSet resultSet =preparedStatement.executeQuery();
	    if(resultSet.next()){
		worker = getFilledWorker(resultSet);
	    }
	} catch (SQLException e) {
	    throw new DaoException(DaoExceptionMessage.ERROR_WORKER_ADDING, e);
	}
	return worker;
    }

    @Override
    public int addEntity(Worker worker) throws DaoException {
	int id;
	String sql = SQL_ADD_WORKER;
	try (Connection connection = pull.getConnection();
	     PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
	    preparedStatement.setString(1, worker.getFullName());
	    preparedStatement.setString(2, worker.getEmail());
	    preparedStatement.setString(3, worker.getAddress());
	    preparedStatement.setInt(4, getWorkerPositionId(worker.getPosition(), connection));
	    preparedStatement.setInt(5, getWorkerStatusId(worker.getStatus(), connection));
	    preparedStatement.setInt(6, worker.getHashPass());
	    preparedStatement.executeUpdate();
	    ResultSet resultSet = preparedStatement.getGeneratedKeys();
	    resultSet.next();
	    id = resultSet.getInt(1);
	} catch (SQLException e) {
	    throw new DaoException(DaoExceptionMessage.ERROR_WORKER_ADDING, e);
	}
	return id;
    }

    /**
     * deletes Worker entity, may throw DaoException if entity's fields are used as Foreign Keys
     */
    @Override
    public void deleteEntity(int workerId) throws DaoException {
	String sql = SQL_DELETE_WORKER;
	try (Connection connection = pull.getConnection();
	     PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
	    preparedStatement.setInt(1, workerId);
	    preparedStatement.executeUpdate();
	} catch (SQLException e) {
	    throw new DaoException(DaoExceptionMessage.ERROR_PATIENT_DELETING, e);
	}
    }

    @Override
    public void updateEntity(Worker worker) throws DaoException {
	String sql = SQL_WORKER_UPDATE;
	try (Connection connection = pull.getConnection();
	     PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
	    preparedStatement.setString(1, worker.getFullName());
	    preparedStatement.setString(2, worker.getEmail());
	    preparedStatement.setString(3, worker.getAddress());
	    preparedStatement.setInt(4, getWorkerPositionId(worker.getPosition(), connection));
	    preparedStatement.setInt(5, getWorkerStatusId(worker.getStatus(), connection));
	    preparedStatement.setInt(6, worker.getHashPass());
	    preparedStatement.setInt(7, worker.getId());
	    preparedStatement.executeUpdate();
	} catch (SQLException e) {
	    throw new DaoException(DaoExceptionMessage.ERROR_PATIENT_DELETING, e);
	}
    }

    private int getWorkerStatusId(WorkersStatus workersStatus, Connection connection) throws SQLException{
	String sql = SQL_GET_STATUS_ID;
	PreparedStatement preparedStatement = connection.prepareStatement(sql);
	preparedStatement.setString(1, workersStatus.name());
	ResultSet resultSet = preparedStatement.executeQuery();
	resultSet.next();
	return resultSet.getInt(COLUMN_NAME_ID);
    }

    private int getWorkerPositionId(WorkersPosition workersPosition, Connection connection) throws SQLException{
	String sql = SQL_GET_POSITION_ID;
	PreparedStatement preparedStatement = connection.prepareStatement(sql);
	preparedStatement.setString(1, workersPosition.name());
	ResultSet resultSet = preparedStatement.executeQuery();
	resultSet.next();
	return resultSet.getInt(COLUMN_NAME_ID);
    }
}

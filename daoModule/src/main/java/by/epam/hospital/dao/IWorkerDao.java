package by.epam.hospital.dao;


import by.epam.hospital.dao.exception.DaoException;
import by.epam.hospital.dao.entity.Worker;

import java.util.List;

public interface IWorkerDao extends IDao<Worker>{

    /**
     * method getting worker by email
     *
     * @param email worker email
     * @return Worker object, if there is no worker with such email returns null
     * @throws by.epam.hospital.dao.exception.DaoException
     */
    Worker getWorkerByEmail(String email) throws DaoException;

    /**
     * method returning full name of worker by id
     *
     * @param id - worker id
     * @return String full name of worker, if there is no worker with such id returns null
     * @throws by.epam.hospital.dao.exception.DaoException
     */
    String getWorkerFullName(int id) throws DaoException;

    /**
     * method returning list doctors and chief doctors currently working in the hospital
     *
     * @return List<Worker> if there are no doctors returns empty list
     * @throws by.epam.hospital.dao.exception.DaoException
     */
    List<Worker> getCurrentDoctorsList() throws DaoException;
}

package by.epam.hospital.dao;


import by.epam.hospital.dao.exception.DaoException;

public interface IDao <T>{

    /**
     * method for getting an object representing current state of entity in database
     * @param id positive int id parameter
     * @return object of entity class depending on parametrization, if there are no entity with such id returns null
     * @throws by.epam.hospital.dao.exception.DaoException
     */
    T getEntity(int id) throws DaoException;

    /**
     * method adding new entities to database
     * @param obj not null, filled entity of parametrized class
     * @return int, id of entity added
     * @throws by.epam.hospital.dao.exception.DaoException
     */
    int addEntity(T obj) throws DaoException;

    /**
     * method deleting entity from database by it's id
     * @param id positive int id parameter
     * @throws by.epam.hospital.dao.exception.DaoException
     */
    void deleteEntity(int id) throws DaoException;

    /**
     * method updating entity's state in database
     * @param obj not null, filled entity of parametrized class
     * @throws by.epam.hospital.dao.exception.DaoException
     */
    void updateEntity(T obj) throws DaoException;
}

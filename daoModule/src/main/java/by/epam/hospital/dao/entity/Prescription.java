package by.epam.hospital.dao.entity;

import java.io.Serializable;
import java.sql.Date;

public class Prescription implements Serializable{

    private final static long serialVersionUID = 2L;

    private int prescriptionId;
    private String content;
    private Date date;
    private int workerId;
    private int patientId;
    private PrescriptionType type;
    private PrescriptionStatus status;

    public int getPrescriptionId() {
	return prescriptionId;
    }

    public void setPrescriptionId(int prescriptionId) {
	this.prescriptionId = prescriptionId;
    }

    public String getContent() {
	return content;
    }

    public void setContent(String content) {
	this.content = content;
    }

    public Date getDate() {
	return date;
    }

    public void setDate(Date date) {
	this.date = date;
    }

    public int getWorkerId() {
	return workerId;
    }

    public void setWorkerId(int workerId) {
	this.workerId = workerId;
    }

    public int getPatientId() {
	return patientId;
    }

    public void setPatientId(int patientId) {
	this.patientId = patientId;
    }

    public PrescriptionType getType() {
	return type;
    }

    public void setType(PrescriptionType type) {
	this.type = type;
    }

    public PrescriptionStatus getStatus() {
	return status;
    }

    public void setStatus(PrescriptionStatus status) {
	this.status = status;
    }

    @Override
    public boolean equals(Object o) {
	if (this == o) {return true;}
	if (o == null || getClass() != o.getClass()) {return false;}

	Prescription that = (Prescription) o;

	if (patientId != that.patientId) {return false;}
	if (prescriptionId != that.prescriptionId) {return false;}
	if (workerId != that.workerId) {return false;}
	if (content != null ? !content.equals(that.content) : that.content != null) {return false;}
	if (date != null ? !date.equals(that.date) : that.date != null) {return false;}
	if (status != that.status) {return false;}
	if (type != that.type) {return false;}

	return true;
    }

    @Override
    public int hashCode() {
	int result = prescriptionId;
	result = 31 * result + (content != null ? content.hashCode() : 0);
	result = 31 * result + (date != null ? date.hashCode() : 0);
	result = 31 * result + workerId;
	result = 31 * result + patientId;
	result = 31 * result + (type != null ? type.hashCode() : 0);
	result = 31 * result + (status != null ? status.hashCode() : 0);
	return result;
    }

    @Override
    public String toString() {
	return "Prescription{" +
	    "prescriptionId=" + prescriptionId +
	    ", content='" + content + '\'' +
	    ", date=" + date +
	    ", workerId=" + workerId +
	    ", patientId=" + patientId +
	    ", type=" + type +
	    ", status=" + status +
	    '}';
    }
}

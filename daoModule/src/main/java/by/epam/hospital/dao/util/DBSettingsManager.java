package by.epam.hospital.dao.util;


import java.util.ResourceBundle;

/**
 * Class working with connection pull settings
 */
public class DBSettingsManager {

    private final static String CONFIG_FILE = "dbSettings";
    private final static DBSettingsManager INSTANCE = new DBSettingsManager();
    private final ResourceBundle bundle;

    public static DBSettingsManager getInstance(){
	return INSTANCE;
    }

    private DBSettingsManager(){
	bundle = ResourceBundle.getBundle(CONFIG_FILE);
    }

    public String getValue (String key){
	return bundle.getString(key);
    }
}

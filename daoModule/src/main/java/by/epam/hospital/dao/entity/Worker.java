package by.epam.hospital.dao.entity;

import java.io.Serializable;

public class Worker implements Serializable {

    private final static long serialVersionUID = 3L;

    private int id;
    private String fullName;
    private String email;
    private String address;
    private WorkersPosition position;
    private WorkersStatus status;
    private int hashPass;

    public int getId() {
	return id;
    }

    public void setId(int id) {
	this.id = id;
    }

    public String getFullName() {
	return fullName;
    }

    public void setFullName(String fullName) {
	this.fullName = fullName;
    }

    public String getEmail() {
	return email;
    }

    public void setEmail(String email) {
	this.email = email;
    }

    public String getAddress() {
	return address;
    }

    public void setAddress(String address) {
	this.address = address;
    }

    public WorkersPosition getPosition() {
	return position;
    }

    public void setPosition(WorkersPosition position) {
	this.position = position;
    }

    public WorkersStatus getStatus() {
	return status;
    }

    public void setStatus(WorkersStatus status) {
	this.status = status;
    }

    public int getHashPass() {
	return hashPass;
    }

    public void setHashPass(int hashPass) {
	this.hashPass = hashPass;
    }

    @Override
    public boolean equals(Object o) {
	if (this == o) return true;
	if (o == null || getClass() != o.getClass()) return false;

	Worker worker = (Worker) o;

	if (hashPass != worker.hashPass) {return false;}
	if (id != worker.id) {return false;}
	if (address != null ? !address.equals(worker.address) : worker.address != null) {return false;}
	if (email != null ? !email.equals(worker.email) : worker.email != null) {return false;}
	if (fullName != null ? !fullName.equals(worker.fullName) : worker.fullName != null) {return false;}
	if (position != worker.position) {return false;}
	if (status != worker.status) {return false;}

	return true;
    }

    @Override
    public int hashCode() {
	int result = id;
	result = 31 * result + (fullName != null ? fullName.hashCode() : 0);
	result = 31 * result + (email != null ? email.hashCode() : 0);
	result = 31 * result + (address != null ? address.hashCode() : 0);
	result = 31 * result + (position != null ? position.hashCode() : 0);
	result = 31 * result + (status != null ? status.hashCode() : 0);
	result = 31 * result + hashPass;
	return result;
    }

    @Override
    public String toString() {
	return "Worker{" +
	    "id=" + id +
	    ", fullName='" + fullName + '\'' +
	    ", email='" + email + '\'' +
	    ", address='" + address + '\'' +
	    ", position=" + position +
	    ", status=" + status +
	    ", hashPass=" + hashPass +
	    '}';
    }
}

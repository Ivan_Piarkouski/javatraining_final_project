package by.epam.hospital.dao.entity;


public enum PrescriptionType {
    OPERATION, DRUG_THERAPY, PHYSIOTHERAPY
}

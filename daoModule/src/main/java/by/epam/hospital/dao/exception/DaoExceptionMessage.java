package by.epam.hospital.dao.exception;


public enum DaoExceptionMessage {

    // connection pull errors
    ERROR_LOADING_DRIVER ("Error while loading jdbc driver"),
    ERROR_GETTING_CONNECTION ("Error while getting connection object"),
    ERROR_WAITING_CONNECTION ("Error while waiting connection object"),
    ERROR_CLOSING_CONNECTION ("Error while closing connection"),

    //workerDao errors
    ERROR_WORKER_GETTING_BY_EMAIL("Error while getting user by email"),
    ERROR_WORKER_FULL_NAME_BY_ID_GETTING("Error while getting worker full name by id"),
    ERROR_WORKER_DOCTORS_ACTUAL_LIST("Error while getting list of doctors currently working"),
    ERROR_WORKER_ADDING("Error while adding worker to database"),
    ERROR_WORKER_GETTING("Error while getting worker from database"),
    ERROR_WORKER_DELETING("Error while deleting worker from database"),

    //patientDao errors
    ERROR_PATIENTS_BY_STATUS_GETTING("Error while getting list of patients by status"),
    ERROR_PATIENTS_ARCHIVE_GETTING("Error while getting list of archive patients"),
    ERROR_PATIENTS_BY_DOCTORS_ID_GETTING("Error while getting list of patients by doctor's id"),
    ERROR_PATIENT_COUNT_BY_DOCTORS_ID_GETTING("Error while getting doctor's patient count"),
    ERROR_PATIENT_STATUSES_BY_ID_GETTING("Error while getting patient statuses by patient id"),
    ERROR_PATIENT_ADDING("Error while adding patient to database"),
    ERROR_PATIENT_GETTING("Error while getting patient by id"),
    ERROR_PATIENT_FULL_NAME_BY_ID_GETTING("Error while getting patient fullName by id"),
    ERROR_PATIENT_DELETING("Error while deleting patient"),
    ERROR_PATIENTS_CURRENT_COUNT_GETTING("Error while getting count of patients currently on treatment"),
    ERROR_UPDATING_PATIENT("Error while performing full update of patient"),

    //prescriptionDao errors
    ERROR_PRESCRIPTIONS_FOR_NURSE_GETTING("Error while getting assigned prescription for nurse"),
    ERROR_PRESCRIPTION_COUNT_FOR_NURSE_GETTING("Error while getting count of assigned prescription for nurse"),
    ERROR_PRESCRIPTION_OPERATIONS_BY_DOCTOR_GETTING("Error while getting list of doctors operations"),
    ERROR_PRESCRIPTION_OPERATION_COUNT_BY_DOCTOR_GETTING("Error while getting count of doctors operations"),
    ERROR_PRESCRIPTION_ADDING("Error while adding new prescription"),
    ERROR_PRESCRIPTIONS_DELETING_BY_PATIENT("Error while deleting prescription by patientId"),
    ERROR_PRESCRIPTIONS_BY_PATIENT_GETTING("Error while getting patient prescriptions"),
    ERROR_PRESCRIPTION_DELETING("Error while deleting prescription"),
    ERROR_PRESCRIPTION_UPDATING("Error while updating prescription");


    private final String errorMessage;
    private DaoExceptionMessage (String errorMessage){
	this.errorMessage=errorMessage;
    }
    public String getErrorMessage(){
	return errorMessage;
    }
}

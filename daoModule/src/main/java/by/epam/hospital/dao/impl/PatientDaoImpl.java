package by.epam.hospital.dao.impl;

import by.epam.hospital.dao.IPatientDao;
import by.epam.hospital.dao.exception.DaoException;
import by.epam.hospital.dao.exception.DaoExceptionMessage;
import by.epam.hospital.dao.entity.Patient;
import by.epam.hospital.dao.entity.PatientStatus;
import by.epam.hospital.dao.util.ConnectionPull;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public class PatientDaoImpl implements IPatientDao {

    private final static PatientDaoImpl INSTANCE = new PatientDaoImpl();
    private final static String SQL_STATUS_PATIENT_LIST = "SELECT p.F_ID, F_FULLNAME, F_ADDRESS, F_STATUS, " +
	"F_INIT_DIAGNOSIS, F_FINAL_DIAGNOSIS, F_DOCTOR_ID, F_OUT_DATE, F_IN_DATE FROM T_PATIENT p JOIN T_PATIENT_STATUS ps ON " +
	"F_PATIENT_STATUS_ID = ps.F_ID WHERE F_STATUS = ? AND F_FULLNAME like ? order by F_IN_DATE DESC, F_FULLNAME limit ?, ?";
    private final static String SQL_STATUS_PATIENT_COUNT = "SELECT count(*) FROM T_PATIENT JOIN T_PATIENT_STATUS ps ON " +
	"F_PATIENT_STATUS_ID = ps.F_ID WHERE F_STATUS = ? AND F_FULLNAME like ?";
    private final static String SQL_DOCTORS_STATUS_PATIENTS = "SELECT  p.F_ID, F_FULLNAME, F_ADDRESS, F_STATUS, " +
	"F_INIT_DIAGNOSIS, F_FINAL_DIAGNOSIS, F_DOCTOR_ID, F_OUT_DATE, F_IN_DATE FROM T_PATIENT p JOIN T_PATIENT_STATUS ps ON " +
	"F_PATIENT_STATUS_ID = ps.F_ID WHERE F_STATUS = ? AND F_DOCTOR_ID = ? AND F_FULLNAME like ? order by F_IN_DATE," +
	" F_FULLNAME limit ?, ?";
    private final static String SQL_DOCTORS_PATIENT_COUNT = "SELECT count(*) FROM T_PATIENT JOIN T_PATIENT_STATUS ps ON " +
	"F_PATIENT_STATUS_ID = ps.F_ID WHERE F_STATUS = ? AND F_DOCTOR_ID = ? AND F_FULLNAME like ?";
    private final static String SQL_PATIENT_BY_ID = "SELECT p.F_ID, F_FULLNAME, F_ADDRESS, F_STATUS, F_INIT_DIAGNOSIS, " +
	"F_FINAL_DIAGNOSIS, F_DOCTOR_ID, F_OUT_DATE, F_IN_DATE FROM T_PATIENT p JOIN T_PATIENT_STATUS ps ON " +
	"F_PATIENT_STATUS_ID = ps.F_ID WHERE p.F_ID = ?";
    private final static String SQL_PATIENT_FULL_NAME_BY_ID = "SELECT F_FULLNAME FROM T_PATIENT WHERE F_ID = ?";
    private final static String SQL_ADD_PATIENT = "INSERT INTO T_PATIENT (F_FULLNAME, F_ADDRESS, F_PATIENT_STATUS_ID, " +
	"F_INIT_DIAGNOSIS, F_FINAL_DIAGNOSIS, F_DOCTOR_ID, F_OUT_DATE, F_IN_DATE) values (?, ?, ?, ?, ?, ?, ?, ?)";
    private final static String SQL_GET_STATUS_ID = "SELECT F_ID FROM T_PATIENT_STATUS WHERE F_STATUS = ?";
    private final static String SQL_DELETE_PATIENT_BY_ID = "delete from t_patient where F_ID = ?";
    private final static String SQL_PATIENT_FULL_UPDATE = "update t_patient set F_FULLNAME = ?, F_ADDRESS = ?, " +
	"F_PATIENT_STATUS_ID = ?, F_INIT_DIAGNOSIS = ?, F_FINAL_DIAGNOSIS = ?, F_DOCTOR_ID = ?, F_OUT_DATE = ?, " +
	"F_IN_DATE = ? where F_ID = ?";
    private final static String COLUMN_NAME_STATUS = "F_STATUS";
    private final static String COLUMN_NAME_ID = "F_ID";
    private final static String COLUMN_NAME_FULLNAME = "F_FULLNAME";
    private final static String COLUMN_NAME_ADDRESS = "F_ADDRESS";
    private final static String COLUMN_NAME_INIT_DIAGNOSIS = "F_INIT_DIAGNOSIS";
    private final static String COLUMN_NAME_FINAL_DIAGNOSIS = "F_FINAL_DIAGNOSIS";
    private final static String COLUMN_NAME_DOCTOR_ID = "F_DOCTOR_ID";
    private final static String COLUMN_NAME_OUT_DATE = "F_OUT_DATE";
    private final static String COLUMN_NAME_IN_DATE = "F_IN_DATE";

    private final ConnectionPull pull = ConnectionPull.getInstance();

    private PatientDaoImpl() {
    }

    public static PatientDaoImpl getInstance() {
	return INSTANCE;
    }

    @Override
    public int addEntity(Patient patient) throws DaoException {
	int id;
	String sql = SQL_ADD_PATIENT;
	try (Connection connection = pull.getConnection();
	     PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
	    preparedStatement.setString(1, patient.getFullName());
	    preparedStatement.setString(2, patient.getAddress());
	    preparedStatement.setInt(3, getPatientStatusId(patient.getStatus(), connection));
	    preparedStatement.setString(4, patient.getInitDiagnosis());
	    preparedStatement.setString(5, patient.getFinalDiagnosis());
	    preparedStatement.setInt(6, patient.getDoctorId());
	    preparedStatement.setDate(7, patient.getOutDate());
	    preparedStatement.setDate(8, patient.getInDate());
	    preparedStatement.executeUpdate();
	    ResultSet resultSet = preparedStatement.getGeneratedKeys();
	    resultSet.next();
	    id = resultSet.getInt(1);
	} catch (SQLException e) {
	    throw new DaoException(DaoExceptionMessage.ERROR_PATIENT_ADDING, e);
	}
	return id;
    }

    @Override
    public void updateEntity(Patient patient) throws DaoException {
	String sql = SQL_PATIENT_FULL_UPDATE;
	try (Connection connection = pull.getConnection();
	     PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
	    preparedStatement.setString(1, patient.getFullName());
	    preparedStatement.setString(2, patient.getAddress());
	    preparedStatement.setInt(3, getPatientStatusId(patient.getStatus(), connection));
	    preparedStatement.setString(4, patient.getInitDiagnosis());
	    preparedStatement.setString(5, patient.getFinalDiagnosis());
	    preparedStatement.setInt(6, patient.getDoctorId());
	    preparedStatement.setDate(7, patient.getOutDate());
	    preparedStatement.setDate(8, patient.getInDate());
	    preparedStatement.setInt(9, patient.getPatientId());
	    preparedStatement.executeUpdate();
	} catch (SQLException e) {
	    throw new DaoException(DaoExceptionMessage.ERROR_UPDATING_PATIENT, e);
	}
    }

    @Override
    public Patient getEntity(int id) throws DaoException {
	Patient patient = null;
	String sql = SQL_PATIENT_BY_ID;
	try (Connection connection = pull.getConnection();
	     PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
	    preparedStatement.setInt(1, id);
	    ResultSet resultSet = preparedStatement.executeQuery();
	    if (resultSet.next()) {
		patient = getFilledPatient(resultSet);
	    }
	} catch (SQLException e) {
	    throw new DaoException(DaoExceptionMessage.ERROR_PATIENT_GETTING, e);
	}
	return patient;
    }

    @Override
    public void deleteEntity(int patientId) throws DaoException {
	String sql = SQL_DELETE_PATIENT_BY_ID;
	try (Connection connection = pull.getConnection();
	     PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
	    preparedStatement.setInt(1, patientId);
	    preparedStatement.executeUpdate();
	} catch (SQLException e) {
	    throw new DaoException(DaoExceptionMessage.ERROR_PATIENT_DELETING, e);
	}
    }

    @Override
    public int getDoctorsCurrentPatientsCount(int doctorId, String filter) throws DaoException {
	String sql = SQL_DOCTORS_PATIENT_COUNT;
	PatientStatus status = PatientStatus.ON_TREATMENT;
	int patientCount;
	try (Connection connection = pull.getConnection();
	     PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
	    preparedStatement.setString(1, status.name());
	    preparedStatement.setInt(2, doctorId);
	    preparedStatement.setString(3, filter);
	    ResultSet resultSet = preparedStatement.executeQuery();
	    resultSet.next();
	    patientCount=resultSet.getInt(1);
	} catch (SQLException e) {
	    throw new DaoException(DaoExceptionMessage.ERROR_PATIENT_COUNT_BY_DOCTORS_ID_GETTING, e);
	}
	return patientCount;
    }

    @Override
    public List<Patient> getCurrentPatientList(String filter, int limitFirst, int limitLast) throws DaoException {
	List<Patient> patientList = new ArrayList<>();
	PatientStatus status = PatientStatus.ON_TREATMENT;
	String sql = SQL_STATUS_PATIENT_LIST;
	try (Connection connection = pull.getConnection();
	     PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
	    preparedStatement.setString(1, status.name());
	    preparedStatement.setString(2, filter);
	    preparedStatement.setInt(3, limitFirst);
	    preparedStatement.setInt(4, limitLast);
	    ResultSet resultSet = preparedStatement.executeQuery();
	    while (resultSet.next()) {
		Patient patient = getFilledPatient(resultSet);
		patientList.add(patient);
	    }
	} catch (SQLException e) {
	    throw new DaoException(DaoExceptionMessage.ERROR_PATIENTS_BY_STATUS_GETTING, e);
	}
	return patientList;
    }

    private Patient getFilledPatient(ResultSet resultSet) throws SQLException{
	Patient patient = new Patient();
	patient.setPatientId(resultSet.getInt(COLUMN_NAME_ID));
	patient.setFullName(resultSet.getString(COLUMN_NAME_FULLNAME));
	patient.setDoctorId(resultSet.getInt(COLUMN_NAME_DOCTOR_ID));
	patient.setInDate(resultSet.getDate(COLUMN_NAME_IN_DATE));
	patient.setOutDate(resultSet.getDate(COLUMN_NAME_OUT_DATE));
	patient.setStatus(PatientStatus.valueOf(resultSet.getString(COLUMN_NAME_STATUS)));
	patient.setInitDiagnosis(resultSet.getString(COLUMN_NAME_INIT_DIAGNOSIS));
	patient.setFinalDiagnosis(resultSet.getString(COLUMN_NAME_FINAL_DIAGNOSIS));
	patient.setAddress(resultSet.getString(COLUMN_NAME_ADDRESS));
	return patient;
    }

    @Override
    public int getCurrentPatientCount(String filter) throws DaoException {
	String sql = SQL_STATUS_PATIENT_COUNT;
	PatientStatus status = PatientStatus.ON_TREATMENT;
	int patientCount;
	try (Connection connection = pull.getConnection();
	     PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
	    preparedStatement.setString(1, status.name());
	    preparedStatement.setString(2, filter);
	    ResultSet resultSet = preparedStatement.executeQuery();
	    resultSet.next();
	    patientCount=resultSet.getInt(1);
	} catch (SQLException e) {
	    throw new DaoException(DaoExceptionMessage.ERROR_PATIENTS_CURRENT_COUNT_GETTING, e);
	}
	return patientCount;
    }

    @Override
    public List<Patient> getDoctorsCurrentPatients(int doctorId, String filter, int limitFirst, int itemsPerPage) throws DaoException {
	List<Patient> patientList = new ArrayList<>();
	PatientStatus status = PatientStatus.ON_TREATMENT;
	String sql = SQL_DOCTORS_STATUS_PATIENTS;
	try (Connection connection = pull.getConnection();
	     PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
	    preparedStatement.setString(1, status.name());
	    preparedStatement.setInt(2, doctorId);
	    preparedStatement.setString(3, filter);
	    preparedStatement.setInt(4, limitFirst);
	    preparedStatement.setInt(5, itemsPerPage);
	    ResultSet resultSet = preparedStatement.executeQuery();
	    while (resultSet.next()) {
		Patient patient = getFilledPatient(resultSet);
		patientList.add(patient);
	    }
	} catch (SQLException e) {
	    throw new DaoException(DaoExceptionMessage.ERROR_PATIENTS_BY_DOCTORS_ID_GETTING, e);
	}
	return patientList;
    }


    private int getPatientStatusId(PatientStatus status, Connection connection) throws SQLException {
	String sql = SQL_GET_STATUS_ID;
	PreparedStatement preparedStatement = connection.prepareStatement(sql);
	preparedStatement.setString(1, status.name());
	ResultSet resultSet = preparedStatement.executeQuery();
	resultSet.next();
	return resultSet.getInt(COLUMN_NAME_ID);
    }

    @Override
    public String getPatientFullNameById(int id) throws DaoException {
	String fullName = null;
	String sql = SQL_PATIENT_FULL_NAME_BY_ID;
	try (Connection connection = pull.getConnection();
	     PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
	    preparedStatement.setInt(1, id);
	    ResultSet resultSet = preparedStatement.executeQuery();
	    if (resultSet.next()) {
		fullName = resultSet.getString(COLUMN_NAME_FULLNAME);
	    }
	} catch (SQLException e) {
	    throw new DaoException(DaoExceptionMessage.ERROR_PATIENT_FULL_NAME_BY_ID_GETTING, e);
	}
	return fullName;
    }
}

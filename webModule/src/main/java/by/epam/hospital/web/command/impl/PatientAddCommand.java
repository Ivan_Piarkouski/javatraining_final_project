package by.epam.hospital.web.command.impl;

import by.epam.hospital.service.IPatientService;
import by.epam.hospital.service.IValidationService;
import by.epam.hospital.service.ServiceHolder;
import by.epam.hospital.service.exception.ServiceException;
import by.epam.hospital.web.WebPage;
import by.epam.hospital.web.WebParameter;
import by.epam.hospital.web.command.CommandHolder;
import by.epam.hospital.web.command.ICommand;

import javax.servlet.http.HttpServletRequest;


public class PatientAddCommand implements ICommand {

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {

	String fullName = request.getParameter(WebParameter.INPUT_FULL_NAME);
	String address = request.getParameter(WebParameter.INPUT_ADDRESS);
	String doctorId = request.getParameter(WebParameter.DOCTOR_INPUT_ID);
	IValidationService validator = ServiceHolder.getValidationService();

	if (validator.validateFullName(fullName) && validator.validateAddress(address)) {
	    IPatientService patientService = ServiceHolder.getPatientService();
	    patientService.addPatient(fullName, address, Integer.parseInt(doctorId));
	    request.setAttribute(WebParameter.COMMAND, CommandHolder.PATIENT_LIST_COMMAND.name());
	} else {
	    request.setAttribute(WebParameter.INPUT_FULL_NAME, fullName);
	    request.setAttribute(WebParameter.INPUT_ADDRESS, address);
	    request.setAttribute(WebParameter.DOCTOR_INPUT_ID, Integer.valueOf(doctorId));
	    request.setAttribute(WebParameter.SHOW_WARNING, true);
	    request.setAttribute(WebParameter.COMMAND, CommandHolder.PATIENT_FORM_COMMAND.name());
	}
	return WebPage.MAIN_PAGE;
    }
}

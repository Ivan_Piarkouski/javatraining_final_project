package by.epam.hospital.web.command.impl;

import by.epam.hospital.service.ServiceHolder;
import by.epam.hospital.service.exception.ServiceException;
import by.epam.hospital.service.model.LogInModel;
import by.epam.hospital.web.WebPage;
import by.epam.hospital.web.WebParameter;
import by.epam.hospital.web.command.ICommand;

import javax.servlet.http.HttpServletRequest;

public class InitDiagnosisFormCommand implements ICommand {

    private final static String MAIN_PAGE_DIAGNOSIS_FORM = "diagnosisForm";

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
	LogInModel loggedUser = (LogInModel) request.getSession().getAttribute(WebParameter.LOGGED_USER);
	String patientId = request.getParameter(WebParameter.PATIENT_ID);
	int parsedPatientId = Integer.parseInt(patientId);

	if(ServiceHolder.getAuthorizationService().authorizeInitDiagnosisSet(loggedUser, parsedPatientId)) {
	    String patientFullName = ServiceHolder.getPatientService().getPatientFullNameById(parsedPatientId);
	    request.setAttribute(WebParameter.PATIENT_ID, patientId);
	    request.setAttribute(WebParameter.PATIENT_FULL_NAME, patientFullName);
	    request.setAttribute(WebParameter.MAIN_PAGE, MAIN_PAGE_DIAGNOSIS_FORM);
	}else {
	    request.setAttribute(WebParameter.MAIN_PAGE, WebParameter.MAIN_PAGE_NOT_AUTHORIZED);
	}
	return WebPage.CORE_PAGE;
    }
}

package by.epam.hospital.web.command.impl;

import by.epam.hospital.service.ServiceHolder;
import by.epam.hospital.service.exception.ServiceException;
import by.epam.hospital.service.model.PatientViewModel;
import by.epam.hospital.service.model.PrescriptionModel;
import by.epam.hospital.web.WebPage;
import by.epam.hospital.web.WebParameter;
import by.epam.hospital.web.command.ICommand;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class PatientViewCommand implements ICommand {

    private final static String MAIN_PAGE_PATIENT_VIEW = "viewPatient";

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
	String patientId = request.getParameter(WebParameter.PATIENT_ID);
	int parsedPatientId = Integer.parseInt(patientId);
	PatientViewModel patient = ServiceHolder.getPatientService().getPatientById(parsedPatientId);
	List<PrescriptionModel> prescriptionModels = ServiceHolder.getPrescriptionService()
	    .getPatientsPrescriptions(parsedPatientId);
	request.setAttribute(WebParameter.MAIN_PAGE, MAIN_PAGE_PATIENT_VIEW);
	request.setAttribute(WebParameter.PATIENT, patient);
	request.setAttribute(WebParameter.PRESCRIPTIONS, prescriptionModels);
	return WebPage.CORE_PAGE;
    }
}

package by.epam.hospital.web.listener;

import by.epam.hospital.service.ServiceHolder;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * inits and closes dao
 */
public class ContextListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
	ServiceHolder.getCommonService().initDao();
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
	ServiceHolder.getCommonService().shutDownDao();
    }
}

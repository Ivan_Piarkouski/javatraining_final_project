package by.epam.hospital.web.command.impl;

import by.epam.hospital.service.IValidationService;
import by.epam.hospital.service.ServiceHolder;
import by.epam.hospital.service.exception.ServiceException;
import by.epam.hospital.service.model.LogInModel;
import by.epam.hospital.web.WebPage;
import by.epam.hospital.web.WebParameter;
import by.epam.hospital.web.command.CommandHolder;
import by.epam.hospital.web.command.ICommand;

import javax.servlet.http.HttpServletRequest;

public class PrescriptionEditCommand implements ICommand {

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
	LogInModel loggedUser = (LogInModel) request.getSession(true).getAttribute(WebParameter.LOGGED_USER);
	String prescriptionId = request.getParameter(WebParameter.PRESCRIPTION_ID);
	int parsedPrescriptionId = Integer.parseInt(prescriptionId);

	if(ServiceHolder.getAuthorizationService().authorizePrescriptionEdit(loggedUser, parsedPrescriptionId)) {

	    String prescriptionContent = request.getParameter(WebParameter.PRESCRIPTION_CONTENT);
	    String prescriptionDate = request.getParameter(WebParameter.DATE);
	    String prescriptionStatus = request.getParameter(WebParameter.PRESCRIPTION_STATUS);
	    String prescriptionType = request.getParameter(WebParameter.PRESCRIPTION_TYPE);
	    String workerId = request.getParameter(WebParameter.DOCTOR_INPUT_ID);
	    IValidationService validator = ServiceHolder.getValidationService();

	    if (validator.validatePrescriptionContent(prescriptionContent) && validator.validateDate(prescriptionDate)) {
		ServiceHolder.getPrescriptionService().editPrescription(parsedPrescriptionId,
		    Integer.parseInt(workerId), prescriptionContent, prescriptionDate, prescriptionStatus, prescriptionType);
		request.setAttribute(WebParameter.COMMAND, CommandHolder.PRESCRIPTION_VIEW_COMMAND.name());
	    } else {
		request.setAttribute(WebParameter.COMMAND, CommandHolder.PRESCRIPTION_EDIT_FORM_COMMAND.name());
		request.setAttribute(WebParameter.SHOW_WARNING, true);
	    }
	}else {
	    request.setAttribute(WebParameter.COMMAND, CommandHolder.NOT_AUTHORIZED_COMMAND.name());
	}
	return WebPage.MAIN_PAGE;
    }
}

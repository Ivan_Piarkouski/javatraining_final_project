package by.epam.hospital.web.command.impl;

import by.epam.hospital.service.ServiceHolder;
import by.epam.hospital.service.exception.ServiceException;
import by.epam.hospital.service.model.LogInModel;
import by.epam.hospital.web.WebPage;
import by.epam.hospital.web.WebParameter;
import by.epam.hospital.web.command.ICommand;

import javax.servlet.http.HttpServletRequest;

public class PatientDelConfirmCommand implements ICommand {

    private final static String MAIN_PAGE_CONFIRM_DELETE = "patientDeleteConfirm";

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
	String patientId = request.getParameter(WebParameter.PATIENT_ID);
	LogInModel loggedUser = (LogInModel) request.getSession(true).getAttribute(WebParameter.LOGGED_USER);
	int intPatientId = Integer.parseInt(patientId);

	if (ServiceHolder.getAuthorizationService().authorizePatientDelete(loggedUser, intPatientId)) {
	    String patientFullName = ServiceHolder.getPatientService().getPatientFullNameById(intPatientId);
	    request.setAttribute(WebParameter.PATIENT_FULL_NAME, patientFullName);
	    request.setAttribute(WebParameter.PATIENT_ID, patientId);
	    request.setAttribute(WebParameter.MAIN_PAGE, MAIN_PAGE_CONFIRM_DELETE);
	} else {
	    request.setAttribute(WebParameter.MAIN_PAGE, WebParameter.MAIN_PAGE_NOT_AUTHORIZED);
	}
	return WebPage.CORE_PAGE;
    }
}

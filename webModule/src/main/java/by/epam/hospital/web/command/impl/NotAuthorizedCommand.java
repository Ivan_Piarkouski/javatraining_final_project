package by.epam.hospital.web.command.impl;

import by.epam.hospital.service.exception.ServiceException;
import by.epam.hospital.web.WebPage;
import by.epam.hospital.web.WebParameter;
import by.epam.hospital.web.command.ICommand;

import javax.servlet.http.HttpServletRequest;

public class NotAuthorizedCommand implements ICommand{

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
	request.setAttribute(WebParameter.MAIN_PAGE, WebParameter.MAIN_PAGE_NOT_AUTHORIZED);
	return WebPage.CORE_PAGE;
    }
}

package by.epam.hospital.web;

public final class WebParameter {

    public final static String LOGGED_USER = "loggedUser";
    public final static String EMAIL = "email";
    public final static String PASS = "pass";
    public final static String LOCAL = "local";
    public final static String COMMAND = "command";
    public final static String PRESCRIPTIONS = "prescriptions";
    public final static String MAIN_PAGE = "mainPage";
    public final static String DOCTORS = "doctors";
    public final static String INPUT_FULL_NAME = "inputFullName";
    public final static String PATIENT_ADDRESS = "patientAddress";
    public final static String INPUT_ADDRESS = "inputAddress";
    public final static String PATIENT_ID = "patientId";
    public final static String DOCTOR_INPUT_ID = "inputDoctorId";
    public final static String PATIENT = "patient";
    public final static String PRESCRIPTION_ID = "prescriptionId";
    public final static String NEXT_COMMAND = "nextCommand";
    public final static String PATIENT_FULL_NAME = "patientFullName";
    public final static String PRESCRIPTION_TYPES = "prescriptionTypes";
    public final static String PRESCRIPTION_STATUSES = "prescriptionStatuses";
    public final static String INIT_DIAGNOSIS = "initDiagnosis";
    public final static String FINAL_DIAGNOSIS = "finalDiagnosis";
    public final static String PRESCRIPTION_CONTENT = "prescriptionContent";
    public final static String PRESCRIPTION_TYPE = "prescriptionType";
    public final static String PRESCRIPTION = "prescription";
    public final static String DATE = "date";
    public final static String PRESCRIPTION_STATUS = "prescriptionStatus";
    public final static String PATIENT_STATUSES = "patientStatuses";
    public final static String PATIENT_STATUS = "patientStatus";
    public final static String PAGINATED_MODEL_LIST = "paginatedModelList";
    public final static String IN_DATE = "inDate";
    public final static String OUT_DATE = "outDate";
    public final static String PAGE = "page";
    public final static String ITEMS_PER_PAGE = "itemsPerPage";
    public final static String FILTER = "filter";
    public final static String SHOW_WARNING = "showWarning";
    public final static String MAIN_PAGE_NOT_AUTHORIZED = "notAuthorized";

}

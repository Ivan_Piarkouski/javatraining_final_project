package by.epam.hospital.web.command;


import by.epam.hospital.service.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;

public interface ICommand {

    /**proceeds request
     *
     * gets params from request and session, sets attributes, uses service module classes and methods
     * @param request HttpServletRequest
     * @return String address to  go to
     * @throws ServiceException
     */
    public String execute(HttpServletRequest request) throws ServiceException;

}

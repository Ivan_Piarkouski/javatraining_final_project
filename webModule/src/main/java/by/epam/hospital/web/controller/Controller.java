package by.epam.hospital.web.controller;


import by.epam.hospital.service.exception.ServiceException;
import by.epam.hospital.web.WebPage;
import by.epam.hospital.web.WebParameter;
import by.epam.hospital.web.command.CommandHolder;
import by.epam.hospital.web.command.ICommand;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class Controller extends HttpServlet {

    private final static Logger log = Logger.getLogger(Controller.class);
    private final static String DEFAULT_COMMAND_NAME = "WELCOME_COMMAND";

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	RequestDispatcher dispatcher = request.getRequestDispatcher(WebPage.LOGIN);
	dispatcher.forward(request, response);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	String commandName = (String) request.getAttribute(WebParameter.COMMAND); //for redirecting from one controller to another
	String page;
	if(commandName == null){
	    commandName = request.getParameter(WebParameter.COMMAND);
	    if (commandName==null) {
		commandName = DEFAULT_COMMAND_NAME;
	    }
	}
	commandName = commandName.toUpperCase();
	try {
	    ICommand command = CommandHolder.valueOf(commandName).getCommand();
	    page = command.execute(request);
	    request.getSession(true).setAttribute(WebParameter.COMMAND, commandName); //for localization
	}catch (IllegalArgumentException e){
	    log.error(e);
	    page = WebPage.NO_SUCH_PAGE;
	} catch (ServiceException e) {
	    log.error(e);
	    page = WebPage.ERROR;
	}
	RequestDispatcher dispatcher = request.getRequestDispatcher(page);
	dispatcher.forward(request, response);
    }
}

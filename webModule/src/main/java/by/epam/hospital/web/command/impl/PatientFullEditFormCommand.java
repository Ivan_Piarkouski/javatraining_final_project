package by.epam.hospital.web.command.impl;

import by.epam.hospital.service.ServiceHolder;
import by.epam.hospital.service.exception.ServiceException;
import by.epam.hospital.service.model.LogInModel;
import by.epam.hospital.service.model.PatientViewModel;
import by.epam.hospital.service.model.WorkerViewModel;
import by.epam.hospital.web.WebPage;
import by.epam.hospital.web.WebParameter;
import by.epam.hospital.web.command.ICommand;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


public class PatientFullEditFormCommand implements ICommand {

    private final static String MAIN_PAGE_PATIENT_EDIT = "patientFullEditForm";

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
	LogInModel loggedUser = (LogInModel) request.getSession(true).getAttribute(WebParameter.LOGGED_USER);

	if(ServiceHolder.getAuthorizationService().authorizeChief(loggedUser)) {
	    String patientId = request.getParameter(WebParameter.PATIENT_ID);
	    PatientViewModel patient = ServiceHolder.getPatientService().getPatientById(Integer.parseInt(patientId));
	    List<WorkerViewModel> doctors = ServiceHolder.getWorkerService().getActualDoctorsList();
	    List<String> patientStatuses = ServiceHolder.getPatientService().getPatientStatuses();

	    request.setAttribute(WebParameter.PATIENT_STATUSES, patientStatuses);
	    request.setAttribute(WebParameter.DOCTORS, doctors);
	    request.setAttribute(WebParameter.MAIN_PAGE, MAIN_PAGE_PATIENT_EDIT);
	    request.setAttribute(WebParameter.PATIENT, patient);
	}else {
	    request.setAttribute(WebParameter.MAIN_PAGE, WebParameter.MAIN_PAGE_NOT_AUTHORIZED);
	}
	return WebPage.CORE_PAGE;
    }
}

package by.epam.hospital.web.filter;

import by.epam.hospital.service.ServiceHolder;
import by.epam.hospital.web.WebParameter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class LocalizationFilter implements Filter {

    private final static String DEFAULT_LOCALE_EN = "en";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
	HttpSession session = ((HttpServletRequest) servletRequest).getSession(true);
	String requestLocale = servletRequest.getParameter(WebParameter.LOCAL);
	String sessionLocale = (String) session.getAttribute(WebParameter.LOCAL);
	if(ServiceHolder.getValidationService().validateLocale(requestLocale) && !requestLocale.equals(sessionLocale)){
	    session.setAttribute(WebParameter.LOCAL, requestLocale);
	}else if(sessionLocale == null){
	    session.setAttribute(WebParameter.LOCAL, DEFAULT_LOCALE_EN);
	}
	filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}

package by.epam.hospital.web.tag;

import by.epam.hospital.service.model.PaginatedListModel;
import by.epam.hospital.web.WebParameter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

public class PaginationTag extends TagSupport {

    private final static int maxPageShown = 10;
    private final static String BUTTON_DEFAULT = "btn-default";
    private final static String BUTTON_INFO = "btn-info";

    @Override
    public int doStartTag() throws JspException {
	HttpServletRequest request = (HttpServletRequest)pageContext.getRequest();
	String contextPath = request.getContextPath();
	PaginatedListModel paginatedModelList = (PaginatedListModel)request.getAttribute(WebParameter.PAGINATED_MODEL_LIST);
	String filter = request.getParameter(WebParameter.FILTER);
	String command = (String) pageContext.getSession().getAttribute(WebParameter.COMMAND);

	int itemsPerPage = paginatedModelList.getItemsPerPage();
	int page = paginatedModelList.getPage();
	int totalPages = paginatedModelList.getPageCount();
	int firstPageShown = page - maxPageShown/2;
	int lastPageShown = page + maxPageShown/2;
	while (firstPageShown <= 0) {
	    firstPageShown++;
	    lastPageShown++;
	}

	while (lastPageShown > totalPages) {
	    lastPageShown--;
	}

	try {
	    pageContext.getOut().write("<table align=\"center\" cellspacing=\"2px\"><tr>");
	    for (int i = firstPageShown; i <= lastPageShown; i++) {
		String button;
		if(i == page) {
		    button = BUTTON_INFO;
		}else {
		    button = BUTTON_DEFAULT;
		}
		pageContext.getOut().write("<td><form role=\"form\" action=\""+contextPath+"/mainpage\" method=\"post\">");
		pageContext.getOut().write("<input type=\"hidden\" name=\"" + WebParameter.PAGE + "\" value=\"" + i + "\"/>");
		pageContext.getOut().write("<input type=\"hidden\" name=\"" + WebParameter.COMMAND + "\" value=\"" + command + "\"/>");
		pageContext.getOut().write("<input type=\"hidden\" name=\"" + WebParameter.ITEMS_PER_PAGE + "\" value=\"" + itemsPerPage + "\"/>");
		if (filter != null) {
		    pageContext.getOut().write("<input type=\"hidden\" name=\"" + WebParameter.FILTER + "\" value=\"" + filter + "\"/>");
		}
		pageContext.getOut().write("<button type=\"submit\" class=\"btn "+button+" btn-xs\">" + i + "</button>");
		pageContext.getOut().write("</form></td>");
	    }
	    pageContext.getOut().write("</tr></table>");

	} catch (IOException e) {
	    throw new JspTagException(e.getMessage());
	}
	return SKIP_BODY;
    }
}

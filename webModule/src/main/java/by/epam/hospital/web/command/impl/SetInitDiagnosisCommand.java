package by.epam.hospital.web.command.impl;

import by.epam.hospital.service.ServiceHolder;
import by.epam.hospital.service.exception.ServiceException;
import by.epam.hospital.service.model.LogInModel;
import by.epam.hospital.web.WebPage;
import by.epam.hospital.web.WebParameter;
import by.epam.hospital.web.command.CommandHolder;
import by.epam.hospital.web.command.ICommand;

import javax.servlet.http.HttpServletRequest;

public class SetInitDiagnosisCommand implements ICommand {

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
	LogInModel loggedUser = (LogInModel) request.getSession().getAttribute(WebParameter.LOGGED_USER);
	String patientId = request.getParameter(WebParameter.PATIENT_ID);
	int intPatientId = Integer.parseInt(patientId);

	if (ServiceHolder.getAuthorizationService().authorizeInitDiagnosisSet(loggedUser, intPatientId)) {
	    String initDiagnosis = request.getParameter(WebParameter.INIT_DIAGNOSIS);
	    if (ServiceHolder.getValidationService().validateDiagnosis(initDiagnosis)) {
		ServiceHolder.getPatientService().setPatientsInitDiagnosis(initDiagnosis, Integer.parseInt(patientId));
		request.setAttribute(WebParameter.COMMAND, CommandHolder.PATIENT_VIEW_COMMAND.name());
	    } else {
		request.setAttribute(WebParameter.INIT_DIAGNOSIS, initDiagnosis);
		request.setAttribute(WebParameter.SHOW_WARNING, true);
		request.setAttribute(WebParameter.COMMAND, CommandHolder.INIT_DIAGNOSIS_FORM_COMMAND.name());
	    }
	} else {
	    request.setAttribute(WebParameter.COMMAND, CommandHolder.NOT_AUTHORIZED_COMMAND.name());
	}
	return WebPage.MAIN_PAGE;
    }
}

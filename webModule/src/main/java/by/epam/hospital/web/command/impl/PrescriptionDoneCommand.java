package by.epam.hospital.web.command.impl;

import by.epam.hospital.service.ServiceHolder;
import by.epam.hospital.service.exception.ServiceException;
import by.epam.hospital.service.model.LogInModel;
import by.epam.hospital.web.WebPage;
import by.epam.hospital.web.WebParameter;
import by.epam.hospital.web.command.CommandHolder;
import by.epam.hospital.web.command.ICommand;

import javax.servlet.http.HttpServletRequest;


public class PrescriptionDoneCommand implements ICommand {

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
	LogInModel loggedUser = (LogInModel) request.getSession(true).getAttribute(WebParameter.LOGGED_USER);
	String prescriptionId = request.getParameter(WebParameter.PRESCRIPTION_ID);
	int parsedPrescriptionId = Integer.parseInt(prescriptionId);

	if (ServiceHolder.getAuthorizationService().authorizePrescriptionDo(loggedUser, parsedPrescriptionId)) {
	    String nextCommand = request.getParameter(WebParameter.NEXT_COMMAND);
	    ServiceHolder.getPrescriptionService().doPrescription(Integer.parseInt(prescriptionId));
	    request.setAttribute(WebParameter.COMMAND, nextCommand);
	} else {
	    request.setAttribute(WebParameter.COMMAND, CommandHolder.NOT_AUTHORIZED_COMMAND.name());
	}
	return WebPage.MAIN_PAGE;
    }
}

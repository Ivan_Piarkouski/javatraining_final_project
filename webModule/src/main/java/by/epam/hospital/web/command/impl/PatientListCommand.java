package by.epam.hospital.web.command.impl;

import by.epam.hospital.service.IPatientService;
import by.epam.hospital.service.ServiceHolder;
import by.epam.hospital.service.exception.ServiceException;
import by.epam.hospital.service.model.PaginatedListModel;
import by.epam.hospital.service.model.PatientListModel;
import by.epam.hospital.web.WebPage;
import by.epam.hospital.web.WebParameter;
import by.epam.hospital.web.command.ICommand;

import javax.servlet.http.HttpServletRequest;

public class PatientListCommand implements ICommand {

    private final static String MAIN_PAGE_PATIENTS_LIST = "viewListPatients";

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
	String page = request.getParameter(WebParameter.PAGE);
	String itemsPerPage = request.getParameter(WebParameter.ITEMS_PER_PAGE);
	String filter = request.getParameter(WebParameter.FILTER);

	IPatientService patientService = ServiceHolder.getPatientService();
	PaginatedListModel<PatientListModel> paginatedPatientModels = patientService
	    .getCurrentPaginatedPatientList(filter, page, itemsPerPage);

	request.setAttribute(WebParameter.FILTER, filter);
	request.setAttribute(WebParameter.PAGINATED_MODEL_LIST, paginatedPatientModels);
	request.setAttribute(WebParameter.MAIN_PAGE, MAIN_PAGE_PATIENTS_LIST);
	return WebPage.CORE_PAGE;
    }
}

package by.epam.hospital.web.command.impl;

import by.epam.hospital.service.ServiceHolder;
import by.epam.hospital.service.exception.ServiceException;
import by.epam.hospital.service.model.LogInModel;
import by.epam.hospital.web.WebPage;
import by.epam.hospital.web.WebParameter;
import by.epam.hospital.web.command.ICommand;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class PrescriptionFormCommand implements ICommand {

    private final static String MAIN_PAGE_PRESCRIPTION_FORM = "prescriptionForm";

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
	String patientId = request.getParameter(WebParameter.PATIENT_ID);
	LogInModel loggedUser = (LogInModel) request.getSession(true).getAttribute(WebParameter.LOGGED_USER);
	int parsedPatientId = Integer.parseInt(patientId);

	if (ServiceHolder.getAuthorizationService().authorizePrescriptionAdd(loggedUser, parsedPatientId)) {

	    String patientFullName = ServiceHolder.getPatientService().getPatientFullNameById(parsedPatientId);
	    List<String> prescriptionTypes = ServiceHolder.getPrescriptionService().getPrescriptionTypes();
	    request.setAttribute(WebParameter.PATIENT_ID, patientId);
	    request.setAttribute(WebParameter.PRESCRIPTION_TYPES, prescriptionTypes);
	    request.setAttribute(WebParameter.PATIENT_FULL_NAME, patientFullName);
	    request.setAttribute(WebParameter.MAIN_PAGE, MAIN_PAGE_PRESCRIPTION_FORM);
	} else {
	    request.setAttribute(WebParameter.MAIN_PAGE, WebParameter.MAIN_PAGE_NOT_AUTHORIZED);
	}
	return WebPage.CORE_PAGE;
    }
}

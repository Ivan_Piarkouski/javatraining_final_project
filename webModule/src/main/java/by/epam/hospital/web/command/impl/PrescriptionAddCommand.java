package by.epam.hospital.web.command.impl;

import by.epam.hospital.service.IValidationService;
import by.epam.hospital.service.ServiceHolder;
import by.epam.hospital.service.exception.ServiceException;
import by.epam.hospital.service.model.LogInModel;
import by.epam.hospital.web.WebPage;
import by.epam.hospital.web.WebParameter;
import by.epam.hospital.web.command.CommandHolder;
import by.epam.hospital.web.command.ICommand;

import javax.servlet.http.HttpServletRequest;

public class PrescriptionAddCommand implements ICommand {

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
	String patientId = request.getParameter(WebParameter.PATIENT_ID);
	LogInModel loggedUser = (LogInModel) request.getSession(true).getAttribute(WebParameter.LOGGED_USER);
	int parsedPatientId = Integer.parseInt(patientId);

	if (ServiceHolder.getAuthorizationService().authorizePrescriptionAdd(loggedUser, parsedPatientId)) {
	    String prescriptionContent = request.getParameter(WebParameter.PRESCRIPTION_CONTENT);
	    String prescriptionType = request.getParameter(WebParameter.PRESCRIPTION_TYPE);
	    LogInModel doctor = (LogInModel) request.getSession().getAttribute(WebParameter.LOGGED_USER);
	    int doctorId = doctor.getId();
	    IValidationService validator = ServiceHolder.getValidationService();
	    if (validator.validatePrescriptionContent(prescriptionContent)) {
		ServiceHolder.getPrescriptionService().addPrescription(parsedPatientId, doctorId,
		    prescriptionContent, prescriptionType);
		request.setAttribute(WebParameter.COMMAND, CommandHolder.PATIENT_VIEW_COMMAND.name());
	    } else {
		request.setAttribute(WebParameter.PRESCRIPTION_CONTENT, prescriptionContent);
		request.setAttribute(WebParameter.PRESCRIPTION_TYPE, prescriptionType);
		request.setAttribute(WebParameter.SHOW_WARNING, true);
		request.setAttribute(WebParameter.COMMAND, CommandHolder.PRESCRIPTION_FORM_COMMAND.name());
	    }
	} else {
	    request.setAttribute(WebParameter.COMMAND, CommandHolder.NOT_AUTHORIZED_COMMAND.name());
	}
	return WebPage.MAIN_PAGE;
    }
}

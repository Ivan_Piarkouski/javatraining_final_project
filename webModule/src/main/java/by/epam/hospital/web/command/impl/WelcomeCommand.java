package by.epam.hospital.web.command.impl;

import by.epam.hospital.service.exception.ServiceException;
import by.epam.hospital.web.WebPage;
import by.epam.hospital.web.WebParameter;
import by.epam.hospital.web.command.ICommand;

import javax.servlet.http.HttpServletRequest;

public class WelcomeCommand implements ICommand {

    private final static String MAIN_PAGE_PARAMETER_VALUE = "welcome";

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
	request.setAttribute(WebParameter.MAIN_PAGE, MAIN_PAGE_PARAMETER_VALUE);
	return WebPage.CORE_PAGE;
    }
}

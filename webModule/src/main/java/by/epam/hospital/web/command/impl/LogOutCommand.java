package by.epam.hospital.web.command.impl;

import by.epam.hospital.service.exception.ServiceException;
import by.epam.hospital.web.WebPage;
import by.epam.hospital.web.WebParameter;
import by.epam.hospital.web.command.ICommand;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


public class LogOutCommand implements ICommand {

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
	HttpSession session = request.getSession(true);
	session.removeAttribute(WebParameter.LOGGED_USER);
	return WebPage.LOGIN;
    }
}

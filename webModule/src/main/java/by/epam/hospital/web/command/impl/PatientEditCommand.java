package by.epam.hospital.web.command.impl;


import by.epam.hospital.service.IPatientService;
import by.epam.hospital.service.IValidationService;
import by.epam.hospital.service.ServiceHolder;
import by.epam.hospital.service.exception.ServiceException;
import by.epam.hospital.web.WebPage;
import by.epam.hospital.web.WebParameter;
import by.epam.hospital.web.command.CommandHolder;
import by.epam.hospital.web.command.ICommand;

import javax.servlet.http.HttpServletRequest;

public class PatientEditCommand implements ICommand {

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
	String patientId = request.getParameter(WebParameter.PATIENT_ID);
	String fullName = request.getParameter(WebParameter.INPUT_FULL_NAME);
	String address = request.getParameter(WebParameter.INPUT_ADDRESS);
	IValidationService validator = ServiceHolder.getValidationService();

	if (validator.validateFullName(fullName) && validator.validateAddress(address)) {
	    IPatientService patientService = ServiceHolder.getPatientService();
	    patientService.editPatientData(fullName, address, Integer.parseInt(patientId));
	    request.setAttribute(WebParameter.COMMAND, CommandHolder.PATIENT_VIEW_COMMAND.name());
	} else {
	    request.setAttribute(WebParameter.SHOW_WARNING, true);
	    request.setAttribute(WebParameter.COMMAND, CommandHolder.PATIENT_VIEW_EDIT_COMMAND.name());
	}
	return WebPage.MAIN_PAGE;
    }
}

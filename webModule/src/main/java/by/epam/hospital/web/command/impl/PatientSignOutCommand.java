package by.epam.hospital.web.command.impl;

import by.epam.hospital.service.IValidationService;
import by.epam.hospital.service.ServiceHolder;
import by.epam.hospital.service.exception.ServiceException;
import by.epam.hospital.service.model.LogInModel;
import by.epam.hospital.web.WebPage;
import by.epam.hospital.web.WebParameter;
import by.epam.hospital.web.command.CommandHolder;
import by.epam.hospital.web.command.ICommand;

import javax.servlet.http.HttpServletRequest;

public class PatientSignOutCommand implements ICommand {

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
	LogInModel loggedUser = (LogInModel) request.getSession(true).getAttribute(WebParameter.LOGGED_USER);
	String patientId = request.getParameter(WebParameter.PATIENT_ID);
	int parsedPatientId = Integer.parseInt(patientId);

	if (ServiceHolder.getAuthorizationService().authorizePatientSignOut(loggedUser, parsedPatientId)) {
	    String finalDiagnosis = request.getParameter(WebParameter.FINAL_DIAGNOSIS);
	    IValidationService validator = ServiceHolder.getValidationService();
	    if (validator.validateDiagnosis(finalDiagnosis)) {
		ServiceHolder.getPatientService().signOutPatient(parsedPatientId, finalDiagnosis);
		request.setAttribute(WebParameter.COMMAND, CommandHolder.PATIENT_VIEW_COMMAND.name());
	    } else {
		request.setAttribute(WebParameter.FINAL_DIAGNOSIS, finalDiagnosis);
		request.setAttribute(WebParameter.SHOW_WARNING, true);
		request.setAttribute(WebParameter.COMMAND, CommandHolder.PATIENT_SIGN_OUT_FORM_COMMAND.name());
	    }
	}else {
	    request.setAttribute(WebParameter.COMMAND, CommandHolder.NOT_AUTHORIZED_COMMAND.name());
	}
	return WebPage.MAIN_PAGE;
    }
}

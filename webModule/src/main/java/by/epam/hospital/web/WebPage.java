package by.epam.hospital.web;

public final class WebPage {

    public final static String LOGIN = "/login.jsp";
    public final static String ERROR = "/error.jsp";
    public final static String NO_SUCH_PAGE = "/noSuchPage.jsp";
    public final static String CORE_PAGE = "/WEB-INF/jsp/corepage.jsp";
    public final static String MAIN_PAGE = "/mainpage";

}

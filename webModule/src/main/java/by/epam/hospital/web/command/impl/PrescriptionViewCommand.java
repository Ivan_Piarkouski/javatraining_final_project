package by.epam.hospital.web.command.impl;

import by.epam.hospital.service.ServiceHolder;
import by.epam.hospital.service.exception.ServiceException;
import by.epam.hospital.service.model.PrescriptionModel;
import by.epam.hospital.web.WebPage;
import by.epam.hospital.web.WebParameter;
import by.epam.hospital.web.command.ICommand;

import javax.servlet.http.HttpServletRequest;

public class PrescriptionViewCommand implements ICommand {

    private final static String MAIN_PAGE_PRESCRIPTION_VIEW = "prescriptionView";

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
	String prescriptionId = request.getParameter(WebParameter.PRESCRIPTION_ID);
	PrescriptionModel prescriptionModel = ServiceHolder.getPrescriptionService()
	    .getPrescription(Integer.parseInt(prescriptionId));
	request.setAttribute(WebParameter.PRESCRIPTION, prescriptionModel);
	request.setAttribute(WebParameter.MAIN_PAGE, MAIN_PAGE_PRESCRIPTION_VIEW);
	return WebPage.CORE_PAGE;
    }
}

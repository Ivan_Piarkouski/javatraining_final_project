package by.epam.hospital.web.command.impl;

import by.epam.hospital.service.ServiceHolder;
import by.epam.hospital.service.exception.ServiceException;
import by.epam.hospital.service.model.LogInModel;
import by.epam.hospital.service.model.PrescriptionModel;
import by.epam.hospital.web.WebPage;
import by.epam.hospital.web.WebParameter;
import by.epam.hospital.web.command.ICommand;

import javax.servlet.http.HttpServletRequest;

public class PrescriptionDelConfirmCommand implements ICommand{

    private final static String MAIN_PAGE_CONFIRM_DELETE = "prescriptionDeleteConfirm";

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
	LogInModel loggedUser = (LogInModel) request.getSession(true).getAttribute(WebParameter.LOGGED_USER);
	String prescriptionId = request.getParameter(WebParameter.PRESCRIPTION_ID);
	int parsedPrescriptionId = Integer.parseInt(prescriptionId);
	if(ServiceHolder.getAuthorizationService().authorizePrescriptionDelete(loggedUser, parsedPrescriptionId)) {
	    PrescriptionModel prescriptionModel = ServiceHolder.getPrescriptionService().getPrescription(parsedPrescriptionId);
	    int patientId = ServiceHolder.getPrescriptionService().getPatientIdByPrescription(parsedPrescriptionId);
	    request.setAttribute(WebParameter.PRESCRIPTION, prescriptionModel);
	    request.setAttribute(WebParameter.PATIENT_ID, patientId);
	    request.setAttribute(WebParameter.MAIN_PAGE, MAIN_PAGE_CONFIRM_DELETE);
	}else {
	    request.setAttribute(WebParameter.MAIN_PAGE, WebParameter.MAIN_PAGE_NOT_AUTHORIZED);
	}
	return WebPage.CORE_PAGE;
    }
}

package by.epam.hospital.web.command;


import by.epam.hospital.web.command.impl.InitDiagnosisFormCommand;
import by.epam.hospital.web.command.impl.LogOutCommand;
import by.epam.hospital.web.command.impl.MyPatientListCommand;
import by.epam.hospital.web.command.impl.NotAuthorizedCommand;
import by.epam.hospital.web.command.impl.OperationListCommand;
import by.epam.hospital.web.command.impl.PatientAddCommand;
import by.epam.hospital.web.command.impl.PatientDelConfirmCommand;
import by.epam.hospital.web.command.impl.PatientDeleteCommand;
import by.epam.hospital.web.command.impl.PatientEditCommand;
import by.epam.hospital.web.command.impl.PatientFormCommand;
import by.epam.hospital.web.command.impl.PatientFullEditCommand;
import by.epam.hospital.web.command.impl.PatientFullEditFormCommand;
import by.epam.hospital.web.command.impl.PatientListCommand;
import by.epam.hospital.web.command.impl.PatientSignOutCommand;
import by.epam.hospital.web.command.impl.PatientSignOutFormCommand;
import by.epam.hospital.web.command.impl.PatientViewCommand;
import by.epam.hospital.web.command.impl.PatientViewEditCommand;
import by.epam.hospital.web.command.impl.PrescriptionAddCommand;
import by.epam.hospital.web.command.impl.PrescriptionDelConfirmCommand;
import by.epam.hospital.web.command.impl.PrescriptionDeleteCommand;
import by.epam.hospital.web.command.impl.PrescriptionDoneCommand;
import by.epam.hospital.web.command.impl.PrescriptionEditCommand;
import by.epam.hospital.web.command.impl.PrescriptionEditFormCommand;
import by.epam.hospital.web.command.impl.PrescriptionFormCommand;
import by.epam.hospital.web.command.impl.PrescriptionListCommand;
import by.epam.hospital.web.command.impl.PrescriptionViewCommand;
import by.epam.hospital.web.command.impl.SetInitDiagnosisCommand;
import by.epam.hospital.web.command.impl.WelcomeCommand;

public enum CommandHolder {

    LOGOUT_COMMAND(new LogOutCommand()),
    WELCOME_COMMAND(new WelcomeCommand()),
    PATIENT_LIST_COMMAND(new PatientListCommand()),
    PATIENT_FORM_COMMAND(new PatientFormCommand()),
    PATIENT_ADD_COMMAND(new PatientAddCommand()),
    PATIENT_VIEW_COMMAND(new PatientViewCommand()),
    PRESCRIPTION_LIST_COMMAND(new PrescriptionListCommand()),
    PRESCRIPTION_DONE_COMMAND(new PrescriptionDoneCommand()),
    PATIENT_VIEW_EDIT_COMMAND(new PatientViewEditCommand()),
    PATIENT_EDIT_COMMAND(new PatientEditCommand()),
    OPERATION_LIST_COMMAND(new OperationListCommand()),
    PATIENT_DEL_CONFIRM_COMMAND(new PatientDelConfirmCommand()),
    PATIENT_DELETE_COMMAND(new PatientDeleteCommand()),
    MY_PATIENT_LIST_COMMAND(new MyPatientListCommand()),
    INIT_DIAGNOSIS_FORM_COMMAND(new InitDiagnosisFormCommand()),
    SET_INIT_DIAGNOSIS_COMMAND(new SetInitDiagnosisCommand()),
    PRESCRIPTION_FORM_COMMAND(new PrescriptionFormCommand()),
    PRESCRIPTION_ADD_COMMAND(new PrescriptionAddCommand()),
    PRESCRIPTION_VIEW_COMMAND(new PrescriptionViewCommand()),
    PRESCRIPTION_DEL_CONFIRM_COMMAND(new PrescriptionDelConfirmCommand()),
    PRESCRIPTION_DELETE_COMMAND(new PrescriptionDeleteCommand()),
    PRESCRIPTION_EDIT_FORM_COMMAND(new PrescriptionEditFormCommand()),
    PRESCRIPTION_EDIT_COMMAND(new PrescriptionEditCommand()),
    PATIENT_FULL_EDIT_FORM_COMMAND(new PatientFullEditFormCommand()),
    PATIENT_FULL_EDIT_COMMAND(new PatientFullEditCommand()),
    PATIENT_SIGN_OUT_FORM_COMMAND(new PatientSignOutFormCommand()),
    PATIENT_SIGN_OUT_COMMAND(new PatientSignOutCommand()),
    NOT_AUTHORIZED_COMMAND(new NotAuthorizedCommand());

    private final ICommand command;

    private CommandHolder(ICommand command) {
	this.command = command;
    }

    public ICommand getCommand() {
	return command;
    }
}

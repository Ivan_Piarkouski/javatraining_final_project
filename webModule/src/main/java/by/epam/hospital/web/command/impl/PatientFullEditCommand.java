package by.epam.hospital.web.command.impl;


import by.epam.hospital.service.IValidationService;
import by.epam.hospital.service.ServiceHolder;
import by.epam.hospital.service.exception.ServiceException;
import by.epam.hospital.service.model.LogInModel;
import by.epam.hospital.service.model.PatientViewModel;
import by.epam.hospital.web.WebPage;
import by.epam.hospital.web.WebParameter;
import by.epam.hospital.web.command.CommandHolder;
import by.epam.hospital.web.command.ICommand;

import javax.servlet.http.HttpServletRequest;

public class PatientFullEditCommand implements ICommand {

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
	LogInModel loggedUser = (LogInModel) request.getSession(true).getAttribute(WebParameter.LOGGED_USER);

	if (ServiceHolder.getAuthorizationService().authorizeChief(loggedUser)) {
	    String patientId = request.getParameter(WebParameter.PATIENT_ID);
	    String fullName = request.getParameter(WebParameter.PATIENT_FULL_NAME);
	    String address = request.getParameter(WebParameter.PATIENT_ADDRESS);
	    String inDate = request.getParameter(WebParameter.IN_DATE);
	    String outDate = request.getParameter(WebParameter.OUT_DATE);
	    String doctorId = request.getParameter(WebParameter.DOCTOR_INPUT_ID);
	    String initDiagnosis = request.getParameter(WebParameter.INIT_DIAGNOSIS);
	    String finalDiagnosis = request.getParameter(WebParameter.FINAL_DIAGNOSIS);
	    String patientStatus = request.getParameter(WebParameter.PATIENT_STATUS);

	    IValidationService validator = ServiceHolder.getValidationService();
	    if (validator.validateDate(inDate) && validator.validateDateAndEmpty(outDate) && validator.validateAddress(address)
		&& validator.validateDiagnosisAndEmpty(initDiagnosis) && validator.validateDiagnosisAndEmpty(finalDiagnosis) &&
		validator.validateFullName(fullName)) {

		PatientViewModel patientViewModel = new PatientViewModel();
		patientViewModel.setDoctorId(Integer.parseInt(doctorId));
		patientViewModel.setPatientId(Integer.parseInt(patientId));
		patientViewModel.setPatientStatus(patientStatus);
		patientViewModel.setInitDiagnosis(initDiagnosis);
		patientViewModel.setFinalDiagnosis(finalDiagnosis);
		patientViewModel.setInDate(inDate);
		patientViewModel.setOutDate(outDate);
		patientViewModel.setFullName(fullName);
		patientViewModel.setAddress(address);
		ServiceHolder.getPatientService().fullUpdatePatient(patientViewModel);
		request.setAttribute(WebParameter.COMMAND, CommandHolder.PATIENT_VIEW_COMMAND.name());

	    } else {
		request.setAttribute(WebParameter.COMMAND, CommandHolder.PATIENT_FULL_EDIT_FORM_COMMAND.name());
		request.setAttribute(WebParameter.SHOW_WARNING, true);
	    }
	} else {
	    request.setAttribute(WebParameter.COMMAND, CommandHolder.NOT_AUTHORIZED_COMMAND.name());

	}
	return WebPage.MAIN_PAGE;
    }
}

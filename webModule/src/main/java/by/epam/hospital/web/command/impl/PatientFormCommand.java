package by.epam.hospital.web.command.impl;

import by.epam.hospital.service.IWorkerService;
import by.epam.hospital.service.ServiceHolder;
import by.epam.hospital.service.exception.ServiceException;
import by.epam.hospital.service.model.WorkerViewModel;
import by.epam.hospital.web.WebPage;
import by.epam.hospital.web.WebParameter;
import by.epam.hospital.web.command.ICommand;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class PatientFormCommand implements ICommand {

    private final static String MAIN_PAGE_PATIENT_FORM = "patientForm";

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
	IWorkerService workerService = ServiceHolder.getWorkerService();
	List<WorkerViewModel> doctors = workerService.getActualDoctorsList();
	request.setAttribute(WebParameter.DOCTORS, doctors);
	request.setAttribute(WebParameter.MAIN_PAGE, MAIN_PAGE_PATIENT_FORM);
	return WebPage.CORE_PAGE;
    }
}

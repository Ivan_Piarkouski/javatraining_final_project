package by.epam.hospital.web.filter;


import by.epam.hospital.service.IValidationService;
import by.epam.hospital.service.ServiceHolder;
import by.epam.hospital.service.exception.ServiceException;
import by.epam.hospital.service.model.LogInModel;
import by.epam.hospital.web.WebPage;
import by.epam.hospital.web.WebParameter;
import org.apache.log4j.Logger;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class LogInFilter implements Filter {

    private final static Logger log = Logger.getLogger(LogInFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
	throws IOException, ServletException {

	String page = null;
	HttpSession session = ((HttpServletRequest) servletRequest).getSession(true);
	LogInModel logInModel = (LogInModel) session.getAttribute(WebParameter.LOGGED_USER);
	if (logInModel == null) {
	    String email = servletRequest.getParameter(WebParameter.EMAIL);
	    String pass = servletRequest.getParameter(WebParameter.PASS);
	    IValidationService validator = ServiceHolder.getValidationService();
	    if (!validator.validateEmail(email)) {
		page = WebPage.LOGIN;
		if (email != null) {
		    servletRequest.setAttribute(WebParameter.SHOW_WARNING, true);
		    servletRequest.setAttribute(WebParameter.EMAIL, email);
		}
	    } else {
		try {
		    logInModel = ServiceHolder.getWorkerService().getLoggedUser(email, pass);
		} catch (ServiceException e) {
		    log.error(e);
		    page = WebPage.ERROR;
		}
		if (logInModel == null && page == null) {
		    page = WebPage.LOGIN;
		    servletRequest.setAttribute(WebParameter.SHOW_WARNING, true);
		    servletRequest.setAttribute(WebParameter.EMAIL, email);
		} else {
		    session.setAttribute(WebParameter.LOGGED_USER, logInModel);
		}
	    }
	}
	if (page == null) {
	    filterChain.doFilter(servletRequest, servletResponse);
	} else {
	    RequestDispatcher dispatcher = servletRequest.getRequestDispatcher(page);
	    dispatcher.forward(servletRequest, servletResponse);
	}
    }

    @Override
    public void destroy() {

    }
}

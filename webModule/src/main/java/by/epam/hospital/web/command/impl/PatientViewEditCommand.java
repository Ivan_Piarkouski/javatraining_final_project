package by.epam.hospital.web.command.impl;

import by.epam.hospital.service.ServiceHolder;
import by.epam.hospital.service.exception.ServiceException;
import by.epam.hospital.service.model.PatientViewModel;
import by.epam.hospital.web.WebPage;
import by.epam.hospital.web.WebParameter;
import by.epam.hospital.web.command.ICommand;

import javax.servlet.http.HttpServletRequest;


public class PatientViewEditCommand implements ICommand {

    private final static String MAIN_PAGE_PATIENT_VIEW_EDIT = "viewEditPatient";

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
	String patientId = request.getParameter(WebParameter.PATIENT_ID);
	PatientViewModel patient = ServiceHolder.getPatientService().getPatientById(Integer.parseInt(patientId));
	request.setAttribute(WebParameter.MAIN_PAGE, MAIN_PAGE_PATIENT_VIEW_EDIT);
	request.setAttribute(WebParameter.PATIENT, patient);
	return WebPage.CORE_PAGE;
    }
}

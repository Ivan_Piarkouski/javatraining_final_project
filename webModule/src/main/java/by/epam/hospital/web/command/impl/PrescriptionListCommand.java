package by.epam.hospital.web.command.impl;


import by.epam.hospital.service.ServiceHolder;
import by.epam.hospital.service.exception.ServiceException;
import by.epam.hospital.service.model.LogInModel;
import by.epam.hospital.service.model.PaginatedListModel;
import by.epam.hospital.service.model.PrescriptionModel;
import by.epam.hospital.web.WebPage;
import by.epam.hospital.web.WebParameter;
import by.epam.hospital.web.command.ICommand;

import javax.servlet.http.HttpServletRequest;


public class PrescriptionListCommand implements ICommand {

    private final static String MAIN_PAGE_PRESCRIPTION_LIST = "viewListPrescriptions";

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
	LogInModel loggedUser = (LogInModel) request.getSession(true).getAttribute(WebParameter.LOGGED_USER);
	if (ServiceHolder.getAuthorizationService().authorizeNurse(loggedUser)) {
	    String filter = request.getParameter(WebParameter.FILTER);
	    String page = request.getParameter(WebParameter.PAGE);
	    String itemsPerPage = request.getParameter(WebParameter.ITEMS_PER_PAGE);

	    PaginatedListModel<PrescriptionModel> paginatedPrescriptions = ServiceHolder.getPrescriptionService().
		getPaginatedUndoneNursePrescriptions(filter, page, itemsPerPage);

	    request.setAttribute(WebParameter.PAGINATED_MODEL_LIST, paginatedPrescriptions);
	    request.setAttribute(WebParameter.FILTER, filter);
	    request.setAttribute(WebParameter.MAIN_PAGE, MAIN_PAGE_PRESCRIPTION_LIST);
	} else {
	    request.setAttribute(WebParameter.MAIN_PAGE, WebParameter.MAIN_PAGE_NOT_AUTHORIZED);
	}
	return WebPage.CORE_PAGE;
    }
}

package by.epam.hospital.web.command.impl;

import by.epam.hospital.service.ServiceHolder;
import by.epam.hospital.service.exception.ServiceException;
import by.epam.hospital.service.model.LogInModel;
import by.epam.hospital.web.WebPage;
import by.epam.hospital.web.WebParameter;
import by.epam.hospital.web.command.CommandHolder;
import by.epam.hospital.web.command.ICommand;

import javax.servlet.http.HttpServletRequest;

public class PatientDeleteCommand implements ICommand {

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
	String patientId = request.getParameter(WebParameter.PATIENT_ID);
	LogInModel loggedUser = (LogInModel)request.getSession(true).getAttribute(WebParameter.LOGGED_USER);
	int intPatientId = Integer.parseInt(patientId);

	if(ServiceHolder.getAuthorizationService().authorizePatientDelete(loggedUser, intPatientId)) {
	    ServiceHolder.getPatientService().deletePatient(intPatientId);
	    request.setAttribute(WebParameter.COMMAND, CommandHolder.PATIENT_LIST_COMMAND.name());
	}else {
	    request.setAttribute(WebParameter.COMMAND, CommandHolder.NOT_AUTHORIZED_COMMAND.name());
	}
	return WebPage.MAIN_PAGE;
    }
}

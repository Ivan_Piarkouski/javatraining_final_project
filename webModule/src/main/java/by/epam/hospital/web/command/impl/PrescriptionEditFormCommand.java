package by.epam.hospital.web.command.impl;

import by.epam.hospital.service.IPrescriptionService;
import by.epam.hospital.service.ServiceHolder;
import by.epam.hospital.service.exception.ServiceException;
import by.epam.hospital.service.model.LogInModel;
import by.epam.hospital.service.model.PrescriptionModel;
import by.epam.hospital.service.model.WorkerViewModel;
import by.epam.hospital.web.WebPage;
import by.epam.hospital.web.WebParameter;
import by.epam.hospital.web.command.ICommand;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class PrescriptionEditFormCommand implements ICommand {

    private final static String MAIN_PAGE_PRESCRIPTION_EDIT_FORM = "prescriptionEditForm";

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
	LogInModel loggedUser = (LogInModel) request.getSession(true).getAttribute(WebParameter.LOGGED_USER);
	String prescriptionId = request.getParameter(WebParameter.PRESCRIPTION_ID);
	int parsedPrescriptionId = Integer.parseInt(prescriptionId);

	if (ServiceHolder.getAuthorizationService().authorizePrescriptionEdit(loggedUser, parsedPrescriptionId)) {

	    IPrescriptionService prescriptionService = ServiceHolder.getPrescriptionService();
	    PrescriptionModel prescriptionModel = prescriptionService.getPrescription(parsedPrescriptionId);
	    List<String> prescriptionTypes = prescriptionService.getPrescriptionTypes();
	    List<String> prescriptionStatuses = prescriptionService.getPrescriptionStatuses();
	    List<WorkerViewModel> doctors = ServiceHolder.getWorkerService().getActualDoctorsList();

	    request.setAttribute(WebParameter.DOCTORS, doctors);
	    request.setAttribute(WebParameter.PRESCRIPTION, prescriptionModel);
	    request.setAttribute(WebParameter.PRESCRIPTION_TYPES, prescriptionTypes);
	    request.setAttribute(WebParameter.PRESCRIPTION_STATUSES, prescriptionStatuses);
	    request.setAttribute(WebParameter.MAIN_PAGE, MAIN_PAGE_PRESCRIPTION_EDIT_FORM);
	} else {
	    request.setAttribute(WebParameter.MAIN_PAGE, WebParameter.MAIN_PAGE_NOT_AUTHORIZED);
	}
	return WebPage.CORE_PAGE;
    }
}

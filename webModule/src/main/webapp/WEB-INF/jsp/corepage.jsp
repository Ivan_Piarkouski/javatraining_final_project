<%@ page contentType="text/html;charset=UTF-8" language="java" errorPage="/./error.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<c:url value="/bootstrap/css/bootstrap.css"/>" rel="stylesheet" type="text/css">
    <c:set var="mainpage" scope="request" value="${requestScope.mainPage}"/>
    <c:set var="welcome" scope="request" value="welcome"/>
    <c:set var="listPatients" scope="request" value="viewListPatients"/>
    <c:set var="patientForm" scope="request" value="patientForm"/>
    <c:set var="viewPatient" scope="request" value="viewPatient"/>
    <c:set var="viewEditPatient" scope="request" value="viewEditPatient"/>
    <c:set var="listPrescriptions" scope="request" value="viewListPrescriptions"/>
    <c:set var="patientDeleteConfirm" scope="request" value="patientDeleteConfirm"/>
    <c:set var="diagnosisForm" scope="request" value="diagnosisForm"/>
    <c:set var="prescriptionForm" scope="request" value="prescriptionForm"/>
    <c:set var="prescriptionView" scope="request" value="prescriptionView"/>
    <c:set var="prescriptionDeleteConfirm" scope="request" value="prescriptionDeleteConfirm"/>
    <c:set var="prescriptionEditForm" scope="request" value="prescriptionEditForm"/>
    <c:set var="patientFullEditForm" scope="request" value="patientFullEditForm"/>
    <c:set var="patientSignOutForm" scope="request" value="patientSignOutForm"/>
    <c:set var="notAthorized" scope="request" value="notAuthorized"/>
</head>

<body>
<div class="section">
    <div class="container-fluid">
        <div class="row">
            <c:choose>
                <c:when test="${viewPatient.equals(mainpage) || viewEditPatient.equals(mainpage) ||
                patientDeleteConfirm.equals(mainpage) || diagnosisForm.equals(mainpage) || prescriptionForm.equals(mainpage)
                || patientFullEditForm.equals(mainpage) || patientSignOutForm.equals(mainpage)}">
                    <div class="col-md-12"><c:import url="headers/patientHeader.jsp"/></div>
                </c:when>
                <c:when test="${prescriptionView.equals(mainpage) || prescriptionDeleteConfirm.equals(mainpage) ||
                prescriptionEditForm.equals(mainpage)}">
                    <div class="col-md-12"><c:import url="headers/prescriptionHeader.jsp"/></div>
                </c:when>
                <c:otherwise>
                    <div class="col-md-12"><c:import url="headers/mainHeader.jsp"/></div>
                </c:otherwise>
            </c:choose>
        </div>
        <div class="row"></div>
        <div class="row">
            <div class="col-md-2"><c:import url="navigation.jsp"/></div>
            <div class="col-md-10">
                <div class="row">
                    <c:choose>
                        <c:when test="${welcome.equals(mainpage)}">
                            <c:import url="mainpages/welcome.jsp"/>
                        </c:when>
                        <c:when test="${listPatients.equals(mainpage)}">
                            <c:import url="mainpages/patientList.jsp"/>
                        </c:when>
                        <c:when test="${patientForm.equals(mainpage)}">
                            <c:import url="mainpages/patientForm.jsp"/>
                        </c:when>
                        <c:when test="${viewPatient.equals(mainpage)}">
                            <c:import url="mainpages/patientView.jsp"/>
                        </c:when>
                        <c:when test="${listPrescriptions.equals(mainpage)}">
                            <c:import url="mainpages/prescritionList.jsp"/>
                        </c:when>
                        <c:when test="${viewEditPatient.equals(mainpage)}">
                            <c:import url="mainpages/patientViewEdit.jsp"/>
                        </c:when>
                        <c:when test="${patientDeleteConfirm.equals(mainpage)}">
                            <c:import url="mainpages/patientConfirmDelete.jsp"/>
                        </c:when>
                        <c:when test="${diagnosisForm.equals(mainpage)}">
                            <c:import url="mainpages/patientDiagnosisForm.jsp"/>
                        </c:when>
                        <c:when test="${prescriptionForm.equals(mainpage)}">
                            <c:import url="mainpages/prescriptionForm.jsp"/>
                        </c:when>
                        <c:when test="${prescriptionView.equals(mainpage)}">
                            <c:import url="mainpages/prescriptionView.jsp"/>
                        </c:when>
                        <c:when test="${prescriptionDeleteConfirm.equals(mainpage)}">
                            <c:import url="mainpages/prescriptionDeleteConfirm.jsp"/>
                        </c:when>
                        <c:when test="${prescriptionEditForm.equals(mainpage)}">
                            <c:import url="mainpages/prescriptionEditForm.jsp"/>
                        </c:when>
                        <c:when test="${patientFullEditForm.equals(mainpage)}">
                            <c:import url="mainpages/patientFullEditForm.jsp"/>
                        </c:when>
                        <c:when test="${patientSignOutForm.equals(mainpage)}">
                            <c:import url="mainpages/patientSignOutForm.jsp"/>
                        </c:when>
                        <c:when test="${notAthorized.equals(mainpage)}">
                            <c:import url="mainpages/notAuthorized.jsp"/>
                        </c:when>
                    </c:choose>
                </div>
                <c:choose>
                    <c:when test="${listPatients.equals(mainpage) || listPrescriptions.equals(mainpage)}">
                        <div class="row">
                            <c:import url="footer.jsp"/>
                        </div>
                    </c:when>
                </c:choose>
            </div>
        </div>
    </div>
</div>
</body>

</html>

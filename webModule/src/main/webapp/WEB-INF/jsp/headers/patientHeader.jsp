<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <fmt:setLocale value="${sessionScope.local}"/>
    <fmt:setBundle basename="localization.local" var="bundl"/>
    <fmt:message bundle="${bundl}" key="local.button.name.en" var="en_button"/>
    <fmt:message bundle="${bundl}" key="local.button.name.ru" var="ru_button"/>
    <fmt:message bundle="${bundl}" key="local.button.name.mainPage" var="mainPage"/>
    <fmt:message bundle="${bundl}" key="local.hospital.name" var="hospitalName"/>
</head>
<body>
<div class="section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-1">
                <form role="form" action="<c:url value="/mainpage"/>" method="post">
                    <button type="submit" class="btn btn-link btn-xs">${mainPage}</button>
                </form>
            </div>
            <div class="col-md-10"><h2 class="text-center text-primary">${hospitalName}</h2><br></div>
            <div class="col-md-1" align="right">
                <table>
                    <tr>
                        <td>
                            <form role="form" action="<c:url value="/mainpage"/>" method="post">
                                <input type="hidden" name="local" value="en"/>
                                <input type="hidden" name="command" value="${sessionScope.command}">
                                <c:choose>
                                    <c:when test="${requestScope.patient!=null}">
                                        <input type="hidden" name="patientId" value="${requestScope.patient.patientId}">
                                    </c:when>
                                    <c:otherwise>
                                        <input type="hidden" name="patientId" value="${requestScope.patientId}">
                                    </c:otherwise>
                                </c:choose>
                                <button type="submit" class="btn btn-link btn-xs">${en_button}</button>
                            </form>
                        </td>
                        <td>
                            <form role="form" action="<c:url value="/mainpage"/>" method="post">
                                <input type="hidden" name="local" value="ru"/>
                                <input type="hidden" name="command" value="${sessionScope.command}">
                                <c:choose>
                                    <c:when test="${requestScope.patient!=null}">
                                        <input type="hidden" name="patientId" value="${requestScope.patient.patientId}">
                                    </c:when>
                                    <c:otherwise>
                                        <input type="hidden" name="patientId" value="${requestScope.patientId}">
                                    </c:otherwise>
                                </c:choose>
                                <button type="submit" class="btn btn-link btn-xs">${ru_button}</button>
                            </form>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
</body>
</html>

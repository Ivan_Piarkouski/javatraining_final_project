<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <fmt:setLocale value="${sessionScope.local}"/>
    <fmt:setBundle basename="localization.local" var="bundl"/>
    <fmt:message bundle="${bundl}" key="local.navigation.button.allPatients" var="all_patients"/>
    <fmt:message bundle="${bundl}" key="local.navigation.button.myPatients" var="my_patients"/>
    <fmt:message bundle="${bundl}" key="local.navigation.button.addPatient" var="add_patient"/>
    <fmt:message bundle="${bundl}" key="local.navigation.button.currentPrescriptions" var="currentPrescriptions"/>
    <fmt:message bundle="${bundl}" key="local.navigation.button.logout" var="logout"/>
    <fmt:message bundle="${bundl}" key="local.navigation.button.myOperations" var="my_operations"/>
    <c:set value="${sessionScope.loggedUser.position}" scope="request" var="position"/>
    <c:set value="NURSE" scope="request" var="positionNurse"/>
    <c:set value="DOCTOR" scope="request" var="positionDoctor"/>
    <c:set value="CHIEF_DOCTOR" scope="request" var="positionChiefDoctor"/>
</head>
<body>
<table cellpadding="0px" cellspacing="0px">
    <tr>
        <form action="<c:url value="/mainpage"/>" method="post">
            <input type="hidden" name="command" value="patient_list_command">
            <button type="submit" class="btn btn-block btn-success">${all_patients}</button>
        </form>
    </tr>
    <c:if test="${positionChiefDoctor.equals(position) || positionDoctor.equals(position)}">
        <tr>
            <form action="<c:url value="/mainpage"/>" method="post">
                <input type="hidden" name="command" value="my_patient_list_command">

                <button type="submit" class="btn btn-block btn-success">${my_patients}</button>
            </form>
        </tr>
        <tr>
            <form action="<c:url value="/mainpage"/>" method="post">
                <input type="hidden" name="command" value="operation_list_command">
                <button type="submit" class="btn btn-block btn-success">${my_operations}</button>
            </form>
        </tr>
    </c:if>
    <tr>
        <form action="<c:url value="/mainpage"/>" method="post">
            <input type="hidden" name="command" value="patient_form_command">
            <button type="submit" class="btn btn-block btn-success">${add_patient}</button>
        </form>
    </tr>
    <c:if test="${positionNurse.equals(position)}">
        <tr>
            <form action="<c:url value="/mainpage"/>" method="post">
                <input type="hidden" name="command" value="prescription_list_command">
                <button type="submit" class="btn btn-block btn-success">${currentPrescriptions}</button>
            </form>
        </tr>
    </c:if>
    <tr>
        <form action="<c:url value="/mainpage"/>" method="post">
            <input type="hidden" name="command" value="logout_command">
            <button type="submit" class="btn btn-block btn-success">${logout}</button>
        </form>
    </tr>
</table>
</body>
</html>

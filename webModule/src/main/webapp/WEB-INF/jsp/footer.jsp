<%@ taglib prefix="mt" uri="/WEB-INF/tld/customTag.tld" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <fmt:setLocale value="${sessionScope.local}"/>
    <fmt:setBundle basename="localization.local" var="bundl"/>
    <fmt:message bundle="${bundl}" key="local.paging.itemsPerPage" var="itemsPerPage"/>
    <fmt:message bundle="${bundl}" key="local.paging.page" var="page"/>
</head>
<body>
<div class="row">
    <div class="col-md-2">
        <p align="left">${itemsPerPage}</p>
        <table align="left" cellspacing="2px"><tr>
            <td>
        <form action="<c:url value="/mainpage"/>" method="post">
            <input type="hidden" name="command" value="${sessionScope.command}">
            <c:if test="${requestScope.filter != null}">
                <input type="hidden" name="filter" value="${requestScope.filter}">
            </c:if>
            <input type="hidden" name="itemsPerPage" value="5">
            <button type="submit" class="btn btn-default btn-xs">5</button>
        </form>
            </td>
            <td>
        <form action="<c:url value="/mainpage"/>" method="post">
            <input type="hidden" name="command" value="${sessionScope.command}">
            <c:if test="${requestScope.filter != null}">
                <input type="hidden" name="filter" value="${requestScope.filter}">
            </c:if>
            <input type="hidden" name="itemsPerPage" value="10">
            <button type="submit" class="btn btn-default btn-xs">10</button>
        </form>
            </td>
            <td>
        <form action="<c:url value="/mainpage"/>" method="post">
            <input type="hidden" name="command" value="${sessionScope.command}">
            <c:if test="${requestScope.filter != null}">
                <input type="hidden" name="filter" value="${requestScope.filter}">
            </c:if>
            <input type="hidden" name="itemsPerPage" value="25">
            <button type="submit" class="btn btn-default btn-xs">25</button>
        </form>
            </td>
            <td>
                <form action="<c:url value="/mainpage"/>" method="post">
                    <input type="hidden" name="command" value="${sessionScope.command}">
                    <c:if test="${requestScope.filter != null}">
                        <input type="hidden" name="filter" value="${requestScope.filter}">
                    </c:if>
                    <input type="hidden" name="itemsPerPage" value="100">
                    <button type="submit" class="btn btn-default btn-xs">100</button>
                </form>
            </td>
            </tr>
            </table>
        </div>
    <div class="col-md-8" align="center">
        <p align="center">${page}</p>
        <mt:paging/>
    </div>
    <div class="col-md-2">
    </div>
</div>
</body>
</html>

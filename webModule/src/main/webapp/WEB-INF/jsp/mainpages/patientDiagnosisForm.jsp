<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <fmt:setLocale value="${sessionScope.local}"/>
    <fmt:setBundle basename="localization.local" var="bundl"/>
    <fmt:message bundle="${bundl}" key="local.button.name.save" var="save"/>
    <fmt:message bundle="${bundl}" key="local.patient.patientDiagnosis" var="patientDiagnosis"/>
    <fmt:message bundle="${bundl}" key="local.message.warning" var="warning"/>
</head>
<body>
<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <form role="form" action="<c:url value="/mainpage"/>" method="post">
                    <input class="hidden" name="command" value="set_init_diagnosis_command">
                    <input class="hidden" name="patientId" value="${requestScope.patientId}">
                    <div class="form-group">
                        <label>${patientDiagnosis} ${requestScope.patientFullName}:</label><br>
                        <textarea type="text" id="initDiagnosis" name="initDiagnosis" rows="2" cols="110" maxlength="16777215"
                               required pattern="[A-Za-zА-Яа-я\-_\.,?!\\/\s0-9]{3,}">${requestScope.initDiagnosis}</textarea>
                    </div>
                    <c:if test="${requestScope.showWarning != null && requestScope.showWarning}">
                        <p style="color: red">${warning}</p>
                    </c:if>
                    <button type="submit" class="btn btn-default">${save}</button>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <link href="<c:url value="/bootstrap/css/bootstrap.css"/>" rel="stylesheet" type="text/css">
    <fmt:setLocale value="${sessionScope.local}"/>
    <fmt:setBundle basename="localization.local" var="bundl"/>
    <fmt:message bundle="${bundl}" key="local.patient.fullName" var="fullname"/>
    <fmt:message bundle="${bundl}" key="local.patient.address" var="address"/>
    <fmt:message bundle="${bundl}" key="local.patient.inDate" var="inDate"/>
    <fmt:message bundle="${bundl}" key="local.patient.outDate" var="outDate"/>
    <fmt:message bundle="${bundl}" key="local.patient.treatingDoctor" var="treatingDoctor"/>
    <fmt:message bundle="${bundl}" key="local.patient.status" var="patientStatus"/>
    <fmt:message bundle="${bundl}" key="local.patient.initDiagnosis" var="initDiagnosis"/>
    <fmt:message bundle="${bundl}" key="local.patient.finalDiagnosis" var="finalDiagnosis"/>
    <fmt:message bundle="${bundl}" key="local.patient.editPatient" var="editPatient"/>
    <fmt:message bundle="${bundl}" key="local.button.name.save" var="save"/>
    <fmt:message bundle="${bundl}" key="local.message.warning" var="warning"/>
</head>
<body>
<%--@elvariable id="patient" type="by.epam.hospital.service.model.PatientViewModel"--%>
<div class="section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <form role="form" action="<c:url value="/mainpage"/>" method="post">
                    <input class="hidden" name="command" value="patient_edit_command"/>
                    <input class="hidden" name="patientId" value="${patient.patientId}"/>
                    <div class="form-group">
                        <label class="control-label" for="inputFullName">${fullname}</label>
                        <input class="form-control" id="inputFullName" name="inputFullName" value="${patient.fullName}" style="width: 400px"
                               pattern="^[A-Za-zА-Яа-я\-]{2,}\s[A-Za-zА-Яа-я\-]{2,}(\s[A-Za-zА-Яа-я\-]{2,})?$" required maxlength="255"
                               value="${requestScope.inputFullName}">
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="inputAddress">${address}</label>
                        <input class="form-control" id="inputAddress" name="inputAddress" value="${patient.address}" style="width: 400px"
                               required maxlength="255" pattern="[A-Za-zА-Яа-я\-_\.,\\/\s0-9]{3,}" value="${requestScope.inputFullName}">
                    </div>
                    <c:if test="${requestScope.showWarning != null && requestScope.showWarning}">
                        <p style="color: red">${warning}</p>
                    </c:if>
                    <button type="submit" class="btn btn-default">${save}</button>
                </form>
                <br>
            </div>
        </div>
    </div>
</div>
</body>
</html>
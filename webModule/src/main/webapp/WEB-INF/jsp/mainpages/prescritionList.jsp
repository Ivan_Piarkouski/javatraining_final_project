<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head><%--TODO выпилить next command--%>
    <fmt:setLocale value="${sessionScope.local}"/>
    <fmt:setBundle basename="localization.local" var="bundl"/>
    <fmt:message bundle="${bundl}" key="local.patient.fullName" var="fullname"/>
    <fmt:message bundle="${bundl}" key="local.patient.treatingDoctor" var="treatingDoctor"/>
    <fmt:message bundle="${bundl}" key="local.prescription.content" var="prescriptionContent"/>
    <fmt:message bundle="${bundl}" key="local.prescription.type" var="prescriptionType"/>
    <fmt:message bundle="${bundl}" key="local.patient.prescriptionsStatus" var="prescriptionStatus"/>
    <fmt:message bundle="${bundl}" key="local.prescription.doPrescription" var="make"/>
    <fmt:message bundle="${bundl}" key="local.patient.searchHelper" var="searchHelper"/>
    <fmt:message bundle="${bundl}" key="local.button.name.search" var="search"/>
    <c:set var="localPrescription" value="local.prescription."/>
</head>
<body>
<%--@elvariable id="prescription" type="by.epam.hospital.service.model.PrescriptionModel"--%>
<%--@elvariable id="paginatedModelList" type="by.epam.hospital.service.model.PaginatedListModel"--%>
<div class="section">
    <div class="container-fluid">
        <div class="row" align="right">
            <form role="form" action="<c:url value="/mainpage"/>" method="post" style="width: 30%">
                <input type="hidden" name="command" value="${sessionScope.command}">
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="${searchHelper}" name="filter"
                               <c:if test="${requestScope.filter != null}">value="${requestScope.filter}"</c:if>
                               maxlength="255">
                        <c:if test="${paginatedModelList != null}">
                            <input type="hidden" name="itemsPerPage" value="${paginatedModelList.itemsPerPage}">
                        </c:if>
                  <span class="input-group-btn">
                    <button class="btn btn-success" type="submit">${search}</button>
                  </span>
                    </div>
                </div>
            </form>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table class="table" align="center">
                    <thead>
                    <tr>
                        <th>${fullname}</th>
                        <th>${prescriptionContent}</th>
                        <th>${prescriptionType}</th>
                        <th>${prescriptionStatus}</th>
                        <th>${treatingDoctor}</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>

                    <c:forEach var="prescription" items="${paginatedModelList.modelList}">
                        <tr>
                            <td>${prescription.patientName}</td>
                            <td>${prescription.prescriptionContent}</td>
                            <td>
                                <fmt:message bundle="${bundl}" key="${localPrescription.concat(prescription.prescriptionType)}" var="type"/>
                               ${type}
                            </td>
                            <td>
                                <fmt:message bundle="${bundl}" key="${localPrescription.concat(prescription.prescriptionStatus)}" var="status"/>
                               ${status}
                            </td>
                            <td>${prescription.appointor}</td>
                            <td>
                                <form action="<c:url value="/mainpage"/>" method="post">
                                    <input type="hidden" name="command" value="prescription_done_command">
                                    <input type="hidden" name="prescriptionId" value="${prescription.prescriptionId}">
                                    <input type="hidden" name="nextCommand" value="${sessionScope.command}">
                                    <button type="submit" class="btn btn-default">${make}</button>
                                </form>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</body>
</html>

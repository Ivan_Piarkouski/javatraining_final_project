<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <fmt:setLocale value="${sessionScope.local}"/>
    <fmt:setBundle basename="localization.local" var="bundl"/>
    <fmt:message bundle="${bundl}" key="local.prescription.forPatient" var="prescriptionForPatient"/>
    <fmt:message bundle="${bundl}" key="local.prescription.type" var="prescriptionType"/>
    <fmt:message bundle="${bundl}" key="local.button.name.save" var="save"/>
    <fmt:message bundle="${bundl}" key="local.message.warning" var="warning"/>
    <c:set var="localPrescription" value="local.prescription."/>
</head>
<body>
<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <form role="form" action="<c:url value="/mainpage"/>" method="post">
                    <input class="hidden" name="command" value="prescription_add_command">
                    <input class="hidden" name="patientId" value="${requestScope.patientId}">

                    <div class="form-group">
                        <label class="control-label" for="prescriptionContent">${prescriptionForPatient}
                            ${requestScope.patientFullName}</label>
                        <input class="form-control" id="prescriptionContent" name="prescriptionContent"
                               style="width: 90%" pattern="^[\S]{2,}.+" required maxlength="16777215"
                               value="${requestScope.prescriptionContent}">
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="prescriptionType">${prescriptionType}</label>
                        <select class="form-control" name="prescriptionType" id="prescriptionType" size="1"
                                style="width: 20%" required>
                            <c:forEach var="type" items="${requestScope.prescriptionTypes}">
                                <option value="${type}"
                                        <c:if test="${type.equals(requestScope.prescriptionType)}">
                                            selected
                                        </c:if>>
                                    <fmt:message bundle="${bundl}" key="${localPrescription.concat(type)}" var="vType"/>
                                    <c:out value="${vType}"/>
                                </option>
                            </c:forEach>
                        </select>
                    </div>
                    <c:if test="${requestScope.showWarning != null && requestScope.showWarning}">
                        <p style="color: red">${warning}</p>
                    </c:if>
                    <button type="submit" class="btn btn-default">${save}</button>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <fmt:setLocale value="${sessionScope.local}"/>
    <fmt:setBundle basename="localization.local" var="bundl"/>
    <fmt:message bundle="${bundl}" key="local.patient.fullName" var="fullname"/>
    <fmt:message bundle="${bundl}" key="local.patient.treatingDoctor" var="treatingDoctor"/>
    <fmt:message bundle="${bundl}" key="local.patient.address" var="address"/>
    <fmt:message bundle="${bundl}" key="local.button.name.save" var="save"/>
    <fmt:message bundle="${bundl}" key="local.message.warning" var="warning"/>
</head>
<body>
<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <form role="form" action="<c:url value="/mainpage"/>" method="post">
                    <input class="hidden" name="command" value="patient_add_command">

                    <div class="form-group">
                        <label class="control-label" for="inputFullName">${fullname}</label>
                        <input class="form-control" id="inputFullName" name="inputFullName" style="width: 400px"
                        pattern="^[A-Za-zА-Яа-я\-]{2,}\s[A-Za-zА-Яа-я\-]{2,}(\s[A-Za-zА-Яа-я\-]{2,})?$"
                               required maxlength="255" value="${requestScope.inputFullName}">
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="inputAddress">${address}</label>
                        <input class="form-control" id="inputAddress" name="inputAddress" style="width: 400px"
                               required maxlength="255"
                               value="${requestScope.inputAddress}"pattern="[A-Za-zА-Яа-я\-_\.,\\/\s0-9]{3,}">
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="inputDoctorId">${treatingDoctor}</label>
                        <select class="form-control" name="inputDoctorId" id="inputDoctorId" size="1"
                                style="width: 400px" required>
                            <%--@elvariable id="doctor" type="by.epam.hospital.service.model.WorkerViewModel"--%>
                            <c:forEach var="doctor" items="${requestScope.doctors}">
                                <option value="${doctor.id}"
                                        <c:if test="${doctor.id ==requestScope.inputDoctorId}">
                                            selected
                                        </c:if>>${doctor.fullName}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <c:if test="${requestScope.showWarning != null && requestScope.showWarning}">
                        <p style="color: red">${warning}</p>
                    </c:if>
                    <button type="submit" class="btn btn-default">${save}</button>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>

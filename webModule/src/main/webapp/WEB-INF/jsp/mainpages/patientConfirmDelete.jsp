<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <fmt:setLocale value="${sessionScope.local}"/>
    <fmt:setBundle basename="localization.local" var="bundl"/>
    <fmt:message bundle="${bundl}" key="local.button.name.no" var="no"/>
    <fmt:message bundle="${bundl}" key="local.button.name.yes" var="yes"/>
    <fmt:message bundle="${bundl}" key="local.patient.deleteConfirmation" var="confirmationMessage"/>
</head>
<body>
<div class="section">
    <div class="container-fluid">
        <div class="col-md-12">
            <div class="row" align="center">
               <h3> ${confirmationMessage} ${requestScope.patientFullName}</h3><br>
            </div>
            <br>
            <br>
            <div class="row">
                <div class="col-md-5"></div>
                <div class="col-md-1">
                    <form action="<c:url value="/mainpage"/>" method="post">
                        <input type="hidden" name="command" value="patient_delete_command">
                        <input type="hidden" name="patientId" value="${requestScope.patientId}">
                        <button type="submit" class="btn btn-default">${yes}</button>
                    </form>
                </div>
                <div class="col-md-1">
                    <form action="<c:url value="/mainpage"/>" method="post">
                        <input type="hidden" name="command" value="patient_view_command">
                        <input type="hidden" name="patientId" value="${requestScope.patientId}">
                        <button type="submit" class="btn btn-default">${no}</button>
                    </form>
                </div>
                <div class="col-md-5"></div>
            </div>
        </div>
    </div>
</div>
</body>
</html>

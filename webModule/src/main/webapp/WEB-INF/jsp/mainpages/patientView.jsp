<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <fmt:setLocale value="${sessionScope.local}"/>
    <fmt:setBundle basename="localization.local" var="bundl"/>
    <fmt:message bundle="${bundl}" key="local.patient.fullName" var="fullname"/>
    <fmt:message bundle="${bundl}" key="local.patient.address" var="address"/>
    <fmt:message bundle="${bundl}" key="local.patient.inDate" var="inDate"/>
    <fmt:message bundle="${bundl}" key="local.patient.outDate" var="outDate"/>
    <fmt:message bundle="${bundl}" key="local.patient.treatingDoctor" var="treatingDoctor"/>
    <fmt:message bundle="${bundl}" key="local.patient.status" var="patientStatus"/>
    <fmt:message bundle="${bundl}" key="local.patient.initDiagnosis" var="initDiagnosis"/>
    <fmt:message bundle="${bundl}" key="local.patient.finalDiagnosis" var="finalDiagnosis"/>
    <fmt:message bundle="${bundl}" key="local.patient.editPatient" var="editPatient"/>
    <fmt:message bundle="${bundl}" key="local.patient.deletePatient" var="deletePatient"/>
    <fmt:message bundle="${bundl}" key="local.patient.notSet" var="notSet"/>
    <fmt:message bundle="${bundl}" key="local.patient.setInitDiagnosis" var="setDiagnosis"/>
    <fmt:message bundle="${bundl}" key="local.patient.setPrescription" var="setPrescription"/>
    <fmt:message bundle="${bundl}" key="local.prescription.content" var="prescriptionContent"/>
    <fmt:message bundle="${bundl}" key="local.prescription.type" var="prescriptionType"/>
    <fmt:message bundle="${bundl}" key="local.patient.prescriptionsStatus" var="prescriptionStatus"/>
    <fmt:message bundle="${bundl}" key="local.button.view" var="view"/>
    <fmt:message bundle="${bundl}" key="local.patient.signOut" var="signOut"/>
    <c:set value="CHIEF_DOCTOR" scope="request" var="positionChiefDoctor"/>
    <c:set value="DOCTOR" scope="request" var="positionDoctor"/>
    <c:set value="${sessionScope.loggedUser.position}" scope="request" var="position"/>
    <c:set value="${sessionScope.loggedUser.id}" scope="request" var="workerId"/>
    <c:set var="localPrescription" value="local.prescription."/>
</head>
<body>
<%--@elvariable id="patient" type="by.epam.hospital.service.model.PatientViewModel"--%>
<div class="section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <label>${fullname}</label><br>
                <c:out value="${patient.fullName}"/><br><br>
                <label>${address}</label><br>
                <c:out value="${patient.address}"/><br><br>
                <c:if test="${!positionChiefDoctor.equals(position)}">
                    <form action="<c:url value="/mainpage"/>" method="post">
                        <input type="hidden" name="command" value="patient_view_edit_command">
                        <input type="hidden" name="patientId" value="${patient.patientId}">
                        <button type="submit" class="btn btn-default">${editPatient}</button>
                    </form>
                </c:if>
            </div>
            <div class="col-md-6">
                <label>${treatingDoctor}</label><br>
                <c:out value="${patient.treatingDoctor}"/><br><br>
                <label>${inDate}</label><br>
                <c:out value="${patient.inDate}"/><br><br>
                <c:if test="${patient.outDate!=null}">
                    <label>${outDate}</label><br>
                    <c:out value="${patient.outDate}"/><br><br>
                </c:if>
                <label>${patientStatus}</label><br>
                <fmt:message bundle="${bundl}" key="${patient.patientStatus}" var="status"/>
                <c:out value="${status}"/><br><br>
            </div>
        </div>
    </div>
</div>
<div class="section">
    <div class="container-fluid">
        <div class="col-md-12">
            <div class="row">
                <label>${initDiagnosis}</label><br>
                <c:if test="${patient.initDiagnosis==null}">
                    ${notSet}
                </c:if>
                <c:out value="${patient.initDiagnosis}"/><br><br>
            </div>
            <c:if test="${patient.finalDiagnosis!=null}">
                <div class="row">
                    <label>${finalDiagnosis}</label><br>
                    <c:out value="${patient.finalDiagnosis}"/><br><br>
                </div>
            </c:if>
            <div class="row">
                <table cellspacing="2px">
                    <tr>
                        <c:if test="${positionChiefDoctor.equals(position) || patient.initDiagnosis == null}">
                            <td>
                                <form action="<c:url value="/mainpage"/>" method="post">
                                    <input type="hidden" name="command" value="patient_del_confirm_command">
                                    <input type="hidden" name="patientId" value="${patient.patientId}">
                                    <button type="submit" class="btn btn-default">${deletePatient}</button>
                                </form>
                            </td>
                        </c:if>
                        <c:if test="${(positionChiefDoctor.equals(position) || positionDoctor.equals(position)) &&
                patient.initDiagnosis == null && patient.doctorId == workerId}">
                            <td>
                                <form action="<c:url value="/mainpage"/>" method="post">
                                    <input type="hidden" name="command" value="init_diagnosis_form_command">
                                    <input type="hidden" name="patientId" value="${patient.patientId}">
                                    <button type="submit" class="btn btn-default">${setDiagnosis}</button>
                                </form>
                            </td>
                        </c:if>
                        <c:if test="${(positionChiefDoctor.equals(position) || positionDoctor.equals(position)) &&
                 patient.doctorId == workerId && patient.initDiagnosis!=null}">
                            <td>
                                <form action="<c:url value="/mainpage"/>" method="post">
                                    <input type="hidden" name="command" value="prescription_form_command">
                                    <input type="hidden" name="patientId" value="${patient.patientId}">
                                    <button type="submit" class="btn btn-default">${setPrescription}</button>
                                </form>
                            </td>
                        </c:if>
                        <c:if test="${positionChiefDoctor.equals(position)}">
                            <td>
                                <form action="<c:url value="/mainpage"/>" method="post">
                                    <input type="hidden" name="command" value="patient_full_edit_form_command">
                                    <input type="hidden" name="patientId" value="${patient.patientId}">
                                    <button type="submit" class="btn btn-default">${editPatient}</button>
                                </form>
                            </td>
                        </c:if>
                        <c:if test="${(positionChiefDoctor.equals(position) || positionDoctor.equals(position)) &&
                        patient.doctorId == workerId && patient.initDiagnosis!=null}">
                            <td>
                                <form action="<c:url value="/mainpage"/>" method="post">
                                    <input type="hidden" name="command" value="patient_sign_out_form_command">
                                    <input type="hidden" name="patientId" value="${patient.patientId}">
                                    <button type="submit" class="btn btn-default">${signOut}</button>
                                </form>
                            </td>
                        </c:if>
                    </tr>
                </table>
            </div>
            <div class="row">
                <table class="table" align="center">
                    <thead>
                    <tr>
                        <th>${prescriptionContent}</th>
                        <th>${prescriptionType}</th>
                        <th>${prescriptionStatus}</th>
                        <th>${treatingDoctor}</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <%--@elvariable id="prescription" type="by.epam.hospital.service.model.PrescriptionModel"--%>
                    <c:forEach var="prescription" items="${requestScope.prescriptions}">
                        <tr>
                            <td>${prescription.prescriptionContent}</td>
                            <td>
                                <fmt:message bundle="${bundl}"
                                             key="${localPrescription.concat(prescription.prescriptionType)}"
                                             var="type"/>
                                    ${type}
                            </td>
                            <td>
                                <fmt:message bundle="${bundl}"
                                             key="${localPrescription.concat(prescription.prescriptionStatus)}"
                                             var="status"/>
                                    ${status}
                            </td>
                            <td>${prescription.appointor}</td>
                            <td>
                                <form action="<c:url value="/mainpage"/>" method="post">
                                    <input type="hidden" name="command" value="prescription_view_command">
                                    <input type="hidden" name="prescriptionId" value="${prescription.prescriptionId}">
                                    <button type="submit" class="btn btn-default">${view}</button>
                                </form>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</body>
</html>

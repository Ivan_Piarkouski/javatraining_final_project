<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <fmt:setLocale value="${sessionScope.local}"/>
    <fmt:setBundle basename="localization.local" var="bundl"/>
    <fmt:message bundle="${bundl}" key="local.prescription.patient" var="patient"/>
    <fmt:message bundle="${bundl}" key="local.prescription.date" var="date"/>
    <fmt:message bundle="${bundl}" key="local.prescription.appointor" var="appointor"/>
    <fmt:message bundle="${bundl}" key="local.prescription.type" var="type"/>
    <fmt:message bundle="${bundl}" key="local.patient.prescriptionsStatus" var="status"/>
    <fmt:message bundle="${bundl}" key="local.prescription.content" var="content"/>
    <fmt:message bundle="${bundl}" key="local.prescription.doPrescription" var="doPrescription"/>
    <fmt:message bundle="${bundl}" key="local.prescription.deletePrescription" var="deletePrescription"/>
    <fmt:message bundle="${bundl}" key="local.prescription.editPrescription" var="editPrescription"/>
    <fmt:message bundle="${bundl}" key="local.prescription.backToPatient" var="backToPatient"/>
    <c:set var="localPrescription" value="local.prescription."/>
    <fmt:message bundle="${bundl}" key="${localPrescription.concat(prescription.prescriptionType)}" var="typeValue"/>
    <fmt:message bundle="${bundl}" key="${localPrescription.concat(prescription.prescriptionStatus)}" var="statusValue"/>
    <c:set value="assigned" scope="request" var="assigned"/>
    <c:set value="${sessionScope.loggedUser.fullName}" scope="request" var="logedUserName"/>
    <c:set value="${sessionScope.loggedUser.position}" scope="request" var="position"/>
    <c:set value="CHIEF_DOCTOR" scope="request" var="positionChiefDoctor"/>
</head>
<body>
<%--@elvariable id="prescription" type="by.epam.hospital.service.model.PrescriptionModel"--%>
<div class="section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <label>${patient}</label><br>
                <c:out value="${prescription.patientName}"/><br><br>
                <label>${date}</label><br>
                <c:out value="${prescription.date}"/><br><br>
                <label>${appointor}</label><br>
                <c:out value="${prescription.appointor}"/><br><br>
            </div>
            <div class="col-md-6">
                <label>${type}</label><br>
                ${typeValue}<br><br>
                <label>${status}</label><br>
                ${statusValue}<br><br>
            </div>
        </div>
    </div>
</div>
<div class="section">
    <div class="container-fluid">
        <div class="col-md-12">
            <div class="row">
                <label>${content}</label><br>
                ${prescription.prescriptionContent}<br><br>
            </div>
            <div class="row">
                <table cellspacing="2px">
                    <tr>
                        <c:if test="${assigned.equals(prescription.prescriptionStatus) && logedUserName.equals(prescription.appointor)}">
                            <td>
                                <form action="<c:url value="/mainpage"/>" method="post">
                                    <input type="hidden" name="command" value="prescription_done_command">
                                    <input type="hidden" name="nextCommand" value="${sessionScope.command}">
                                    <input type="hidden" name="prescriptionId" value="${prescription.prescriptionId}">
                                    <button type="submit" class="btn btn-default">${doPrescription}</button>
                                </form>
                            </td>
                        </c:if>
                        <c:if test="${positionChiefDoctor.equals(position) || (logedUserName.equals(prescription.appointor) &&
                    assigned.equals(prescription.prescriptionStatus))}">
                            <td>
                                <form action="<c:url value="/mainpage"/>" method="post">
                                    <input type="hidden" name="command" value="prescription_del_confirm_command">
                                    <input type="hidden" name="prescriptionId" value="${prescription.prescriptionId}">
                                    <button type="submit" class="btn btn-default">${deletePrescription}</button>
                                </form>
                            </td>
                        </c:if>
                        <c:if test="${positionChiefDoctor.equals(position) || (logedUserName.equals(prescription.appointor) &&
                    assigned.equals(prescription.prescriptionStatus))}">
                            <td>
                                <form action="<c:url value="/mainpage"/>" method="post">
                                    <input type="hidden" name="command" value="prescription_edit_form_command">
                                    <input type="hidden" name="prescriptionId" value="${prescription.prescriptionId}">
                                    <button type="submit" class="btn btn-default">${editPrescription}</button>
                                </form>
                            </td>
                        </c:if>
                        <td>
                            <form action="<c:url value="/mainpage"/>" method="post">
                                <input type="hidden" name="command" value="patient_view_command">
                                <input type="hidden" name="patientId" value="${prescription.patientId}">
                                <button type="submit" class="btn btn-default">${backToPatient}</button>
                            </form>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
</body>
</html>

<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <fmt:setLocale value="${sessionScope.local}"/>
    <fmt:setBundle basename="localization.local" var="bundl"/>
    <fmt:message bundle="${bundl}" key="local.button.name.no" var="no"/>
    <fmt:message bundle="${bundl}" key="local.button.name.yes" var="yes"/>
    <fmt:message bundle="${bundl}" key="local.prescription.deleteConfirm" var="confirmationMessage"/>
</head>
<body>
<%--@elvariable id="prescription" type="by.epam.hospital.service.model.PrescriptionModel"--%>
<div class="section">
    <div class="container-fluid">
        <div class="col-md-12">
            <div class="row" align="center">
                <h3> ${confirmationMessage} ${prescription.patientName}:</h3><br>
                ${prescription.prescriptionContent}
                <br>
                <br>
                <br>
            </div>
            <div class="row">
                <div class="col-md-5"></div>
                <div class="col-md-1">
                    <form action="<c:url value="/mainpage"/>" method="post">
                        <input type="hidden" name="command" value="prescription_delete_command">
                        <input type="hidden" name="prescriptionId" value="${prescription.prescriptionId}">
                        <input type="hidden" name="patientId" value="${requestScope.patientId}">
                        <button type="submit" class="btn btn-default">${yes}</button>
                    </form>
                </div>
                <div class="col-md-1">
                    <form action="<c:url value="/mainpage"/>" method="post">
                        <input type="hidden" name="command" value="prescription_view_command">
                        <input type="hidden" name="prescriptionId" value="${prescription.prescriptionId}">
                        <button type="submit" class="btn btn-default">${no}</button>
                    </form>
                </div>
                <div class="col-md-5"></div>
            </div>
        </div>
    </div>
</div>
</body>
</html>

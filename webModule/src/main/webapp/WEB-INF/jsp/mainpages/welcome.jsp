<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
  <head>
      <fmt:setLocale value="${sessionScope.local}"/>
      <fmt:setBundle basename="localization.local" var="bundl"/>
      <fmt:message bundle="${bundl}" key="local.message" var="message"/>
  </head>
  <body>
<h1>${message} ${sessionScope.loggedUser.fullName}</h1>
  </body>
</html>

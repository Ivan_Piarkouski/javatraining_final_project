<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <fmt:setLocale value="${sessionScope.local}"/>
    <fmt:setBundle basename="localization.local" var="bundl"/>
    <fmt:message bundle="${bundl}" key="local.patient.fullName" var="fullname"/>
    <fmt:message bundle="${bundl}" key="local.patient.inDate" var="inDate"/>
    <fmt:message bundle="${bundl}" key="local.patient.treatingDoctor" var="treatingDoctor"/>
    <fmt:message bundle="${bundl}" key="local.patient.prescriptionsStatus" var="prescription"/>
    <fmt:message bundle="${bundl}" key="local.patient.initDiagnosis" var="initDiagnosis"/>
    <fmt:message bundle="${bundl}" key="local.button.view" var="view"/>
    <fmt:message bundle="${bundl}" key="local.patient.notSet" var="notSet"/>
    <fmt:message bundle="${bundl}" key="local.patient.set" var="set"/>
    <fmt:message bundle="${bundl}" key="local.patient.searchHelper" var="searchHelper"/>
    <fmt:message bundle="${bundl}" key="local.button.name.search" var="search"/>
</head>
<body>
<%--@elvariable id="paginatedModelList" type="by.epam.hospital.service.model.PaginatedListModel"--%>
<%--@elvariable id="patient" type="by.epam.hospital.service.model.PatientListModel"--%>
<div class="section">
    <div class="container-fluid">
        <div class="row" align="right">
            <form role="form" action="<c:url value="/mainpage"/>" method="post" style="width: 30%">
                <input type="hidden" name="command" value="${sessionScope.command}">
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="${searchHelper}" name="filter"
                                <c:if test="${requestScope.filter != null}">value="${requestScope.filter}"</c:if>
                                maxlength="255">
                        <c:if test="${paginatedModelList != null}">
                            <input type="hidden" name="itemsPerPage" value="${paginatedModelList.itemsPerPage}">
                        </c:if>
                  <span class="input-group-btn">
                    <button class="btn btn-success" type="submit">${search}</button>
                  </span>
                    </div>
                </div>
            </form>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table class="table" align="center">
                    <thead>
                    <tr>
                        <th>${fullname}</th>
                        <th>${inDate}</th>
                        <th>${treatingDoctor}</th>
                        <th>${prescription}</th>
                        <th>${initDiagnosis}</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="patient" items="${paginatedModelList.modelList}">
                        <tr>
                            <td>${patient.fullName}</td>
                            <td>${patient.inDate}</td>
                            <td>${patient.treatingDoctor}</td>
                            <td>
                                <fmt:message bundle="${bundl}" key="${patient.prescriptionStatus}" var="status"/>
                                <c:out value="${status}"/>
                            </td>
                            <td>
                                <c:choose>
                                    <c:when test="${patient.initDiagnosis}">${set}</c:when>
                                    <c:otherwise>${notSet}</c:otherwise>
                                </c:choose>
                            </td>
                            <td>
                                <form action="<c:url value="/mainpage"/>" method="post">
                                    <input type="hidden" name="command" value="patient_view_command">
                                    <input type="hidden" name="patientId" value="${patient.patientId}">
                                    <button type="submit" class="btn btn-default">${view}</button>
                                </form>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</body>
</html>

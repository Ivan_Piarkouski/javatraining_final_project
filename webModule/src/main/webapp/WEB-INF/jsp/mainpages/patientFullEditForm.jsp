<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <fmt:setLocale value="${sessionScope.local}"/>
    <fmt:setBundle basename="localization.local" var="bundl"/>
    <fmt:message bundle="${bundl}" key="local.patient.fullName" var="fullname"/>
    <fmt:message bundle="${bundl}" key="local.patient.address" var="address"/>
    <fmt:message bundle="${bundl}" key="local.patient.inDate" var="inDate"/>
    <fmt:message bundle="${bundl}" key="local.patient.outDate" var="outDate"/>
    <fmt:message bundle="${bundl}" key="local.patient.treatingDoctor" var="treatingDoctor"/>
    <fmt:message bundle="${bundl}" key="local.patient.status" var="patientStatus"/>
    <fmt:message bundle="${bundl}" key="local.patient.initDiagnosis" var="initDiagnosis"/>
    <fmt:message bundle="${bundl}" key="local.patient.finalDiagnosis" var="finalDiagnosis"/>
    <fmt:message bundle="${bundl}" key="local.button.name.save" var="save"/>
    <fmt:message bundle="${bundl}" key="local.message.warning" var="warning"/>
    <c:set var="localPatient" value="local.patient."/>
</head>
<body>
<%--@elvariable id="patient" type="by.epam.hospital.service.model.PatientViewModel"--%>
<div class="container">
    <div class="row">
        <div class="col-md-12">

            <form role="form" action="<c:url value="/mainpage"/>" method="post">
                <input class="hidden" name="command" value="patient_full_edit_command">
                <input class="hidden" name="patientId" value="${patient.patientId}">

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label" for="patientFullName">${fullname}</label>
                            <input class="form-control" id="patientFullName" name="patientFullName"
                                   value="${patient.fullName}" style="width: 60%" required maxlength="255"
                                   pattern="^[A-Za-zА-Яа-я\-]{2,}\s[A-Za-zА-Яа-я\-]{2,}(\s[A-Za-zА-Яа-я\-]{2,})?$">
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="patientAddress">${address}</label>
                            <input class="form-control" id="patientAddress" name="patientAddress"
                                   value="${patient.address}" style="width: 60%" required maxlength="255"
                                   pattern="^[A-Za-zА-Яа-я\-_\.,\\/\s0-9]{3,}">
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="inputDoctorId">${treatingDoctor}</label>
                            <select class="form-control" name="inputDoctorId" id="inputDoctorId" size="1"
                                    style="width: 60%" required>
                                <%--@elvariable id="doctor" type="by.epam.hospital.service.model.WorkerViewModel"--%>
                                <c:forEach var="doctor" items="${requestScope.doctors}">
                                    <option value="${doctor.id}"
                                            <c:if test="${patient.doctorId==doctor.id}">selected </c:if>>
                                            ${doctor.fullName}
                                    </option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>


                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label" for="inDate">${inDate}</label>
                            <input type="date" class="form-control" id="inDate" name="inDate" value="${patient.inDate}"
                                   style="width: 60%" required pattern="^201[5-9]-[0-1][0-9]-[0-3][0-9]$">
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="outDate">${outDate}</label>
                            <input type="date" class="form-control" id="outDate" name="outDate"
                                   value="${patient.outDate}"
                                   style="width: 60%" pattern="^201[5-9]-[0-1][0-9]-[0-3][0-9]$">
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="patientStatus">${patientStatus}</label>
                            <select class="form-control" name="patientStatus" id="patientStatus" size="1"
                                    style="width: 60%" required>
                                <c:forEach var="status" items="${requestScope.patientStatuses}">
                                    <option value="${status}"
                                            <c:if test="${status.equals(patient.patientStatus)}">selected </c:if>>
                                        <fmt:message bundle="${bundl}" key="${localPatient.concat(status)}"
                                                     var="vStatus"/>
                                            ${vStatus}
                                    </option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label>${initDiagnosis}</label><br>
                        <textarea id="initDiagnosis" name="initDiagnosis" rows="2" cols="120" maxlength="16777215">${patient.initDiagnosis}</textarea>
                    </div>

                    <div class="form-group">
                        <label>${finalDiagnosis}</label><br>
                        <textarea id="finalDiagnosis" name="finalDiagnosis" rows="2" cols="120" maxlength="16777215">${patient.finalDiagnosis}</textarea>
                    </div>
                    <c:if test="${requestScope.showWarning != null && requestScope.showWarning}">
                        <p style="color: red">${warning}</p>
                    </c:if>
                    <button type="submit" class="btn btn-default">${save}</button>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>

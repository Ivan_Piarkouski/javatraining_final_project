<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <fmt:setLocale value="${sessionScope.local}"/>
    <fmt:setBundle basename="localization.local" var="bundl"/>
    <fmt:message bundle="${bundl}" key="local.prescription.forPatient" var="prescriptionForPatient"/>
    <fmt:message bundle="${bundl}" key="local.prescription.type" var="prescriptionType"/>
    <fmt:message bundle="${bundl}" key="local.patient.prescriptionsStatus" var="prescriptionStatus"/>
    <fmt:message bundle="${bundl}" key="local.button.name.save" var="save"/>
    <fmt:message bundle="${bundl}" key="local.patient.treatingDoctor" var="treatingDoctor"/>
    <fmt:message bundle="${bundl}" key="local.prescription.content" var="prescriptionContent"/>
    <fmt:message bundle="${bundl}" key="local.prescription.editPrescriptionForPatient" var="editMessage"/>
    <fmt:message bundle="${bundl}" key="local.prescription.date" var="date"/>
    <fmt:message bundle="${bundl}" key="local.message.warning" var="warning"/>
    <c:set var="localPrescription" value="local.prescription."/>
</head>
<body>
<%--@elvariable id="prescription" type="by.epam.hospital.service.model.PrescriptionModel"--%>
<%--<div class="section">--%>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                ${editMessage} <label>${prescription.patientName}</label>

                <form role="form" action="<c:url value="/mainpage"/>" method="post">
                    <input class="hidden" name="command" value="prescription_edit_command">
                    <input class="hidden" name="prescriptionId" value="${prescription.prescriptionId}">

                    <div class="form-group">
                        <label class="control-label" for="prescriptionContent">${prescriptionContent}</label>
                        <input class="form-control" id="prescriptionContent" name="prescriptionContent"
                               value="${prescription.prescriptionContent}"
                               style="width: 90%" pattern="^[\S]{2,}.+" required maxlength="16777215">
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="prescriptionType">${prescriptionType}</label>
                        <select class="form-control" name="prescriptionType" id="prescriptionType" size="1"
                                style="width: 20%" required>
                            <c:forEach var="type" items="${requestScope.prescriptionTypes}">
                                <option value="${type}"
                                        <c:if test="${type.equals(prescription.prescriptionType)}">selected </c:if> >
                                    <fmt:message bundle="${bundl}" key="${localPrescription.concat(type)}" var="vType"/>
                                        ${vType}
                                </option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="prescriptionStatus">${prescriptionStatus}</label>
                        <select class="form-control" name="prescriptionStatus" id="prescriptionStatus" size="1"
                                style="width: 20%" required>
                            <c:forEach var="status" items="${requestScope.prescriptionStatuses}">
                                <option value="${status}"
                                        <c:if test="${status.equals(prescription.prescriptionStatus)}">selected </c:if>>
                                    <fmt:message bundle="${bundl}" key="${localPrescription.concat(status)}"
                                                 var="vStatus"/>
                                        ${vStatus}
                                </option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="inputDoctorId">${treatingDoctor}</label>
                        <select class="form-control" name="inputDoctorId" id="inputDoctorId" size="1"
                                style="width: 20%" required>
                            <%--@elvariable id="doctor" type="by.epam.hospital.service.model.WorkerViewModel"--%>
                            <c:forEach var="doctor" items="${requestScope.doctors}">
                                <option value="${doctor.id}"
                                        <c:if test="${prescription.appointor.equals(doctor.fullName)}">selected </c:if>>
                                        ${doctor.fullName}
                                </option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="date">${date}</label>
                        <input type="date" class="form-control" id="date" name="date" value="${prescription.date}"
                               style="width: 20%" required pattern="^201[5-9]-[0-1][0-9]-[0-3][0-9]$">
                    </div>
                    <c:if test="${requestScope.showWarning != null && requestScope.showWarning}">
                        <p style="color: red">${warning}</p>
                    </c:if>
                    <button type="submit" class="btn btn-default">${save}</button>
                </form>
            </div>
        </div>
    </div>
<%--</div>--%>
</body>
</html>

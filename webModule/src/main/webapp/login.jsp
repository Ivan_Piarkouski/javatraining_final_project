<%@ page contentType="text/html;charset=UTF-8" language="java" errorPage="error.jsp" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css"/>
    <fmt:setLocale value="${sessionScope.local}"/>
    <fmt:setBundle basename="localization.local" var="bundl"/>
    <fmt:message bundle="${bundl}" key="local.button.name.en" var="en_button"/>
    <fmt:message bundle="${bundl}" key="local.button.name.ru" var="ru_button"/>
    <fmt:message bundle="${bundl}" key="local.button.login" var="log_in"/>
    <fmt:message bundle="${bundl}" key="local.field.email" var="f_email"/>
    <fmt:message bundle="${bundl}" key="local.field.password" var="f_pass"/>
    <fmt:message bundle="${bundl}" key="local.message.warning" var="warning"/>
    <title></title>
</head>
<body>
<div class="form-group" align="right">
    <div class="btn-group">
        <table>
            <tr>
                <td>
                    <form role="form" action="<c:url value="/login.jsp"/>" method="post">
                        <input type="hidden" name="local" value="en"/>
                        <button type="submit" class="btn btn-link btn-xs">${en_button}</button>
                    </form>
                </td>
                <td>
                    <form role="form" action="<c:url value="/login.jsp"/>" method="post">
                        <input type="hidden" name="local" value="ru"/>
                        <button type="submit" class="btn btn-link btn-xs">${ru_button}</button>
                    </form>
                </td>
            </tr>
        </table>
    </div>
</div>

<div class="form-group" align="center">
    <form role="form" action="<c:url value="/mainpage"/>" method="post">
        <c:if test="${sessionScope.loggedUser == null}">
            <div class="form-group">
                <label for="email">${f_email}</label>
                <input type="email" class="form-control" id="email" name="email" style="width: 200px" required
                       maxlength="255" pattern="^[A-Za-z0-9\.\-_]+@[A-Za-z0-9\.\-_]+\.[A-Za-z]{2,4}$" value="${requestScope.email}">
            </div>
            <div class="form-group">
                <label for="pass">${f_pass}</label>
                <input type="password" class="form-control" id="pass" name="pass" style="width: 200px" required
                       maxlength="255">
            </div>
        </c:if>
        <c:if test="${requestScope.showWarning != null && requestScope.showWarning}">
            <p style="color: red">${warning}</p>
        </c:if>
        <input type="hidden" name="command" value="welcome_command">
        <button type="submit" class="btn btn-success">${log_in}</button>
    </form>
</div>
</body>
</html>

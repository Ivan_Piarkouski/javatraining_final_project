<%@ page contentType="text/html;charset=UTF-8" language="java" errorPage="error.jsp" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <fmt:setLocale value="${sessionScope.local}"/>
    <fmt:setBundle basename="localization.local" var="bundl"/>
    <fmt:message bundle="${bundl}" key="local.button.name.mainPage" var="mainPage"/>
    <fmt:message bundle="${bundl}" key="local.message.noSuchPage" var="noSuchPage"/>
    <title>${noSuchPage}</title>
</head>
<body>
<h1>${noSuchPage}</h1><br>

<form role="form" action="<c:url value="/mainpage"/>" method="post">
<button type="submit" class="btn btn-link btn-xs">${mainPage}</button>
</form>
</body>
</html>

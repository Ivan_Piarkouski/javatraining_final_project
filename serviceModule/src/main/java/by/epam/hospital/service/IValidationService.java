package by.epam.hospital.service;


public interface IValidationService {

    /**validates string lcale
     *
     * @param locale string
     * @return boolean true if locale is valid, false if not or null
     */
    boolean validateLocale(String locale);

    /** validates fullName
     *
     * checks if given fullName matches inner pattern
     * @param fullName - checked fullName, can be null
     * @return boolean true if fullName is valid and not null, false if not
     */
    boolean validateFullName(String fullName);

    /** validates address
     *
     * checks if given address matches inner pattern
     * @param address - checked address, can be null
     * @return boolean true if address is valid and not null, false if not
     */
    boolean validateAddress(String address);

    /** validates email
     *
     * checks if given email matches inner pattern
     * @param email - checked email, can be null
     * @return boolean true if email is valid and not null, false if not
     */
    boolean validateEmail(String email);

    /** validates diagnosis
     *
     * checks if given diagnosis matches inner pattern
     * @param diagnosis - checked diagnosis, can be null
     * @return boolean true if diagnosis is valid and not null, false if not
     */
    boolean validateDiagnosis(String diagnosis);

    /** validates prescriptionContent
     *
     * checks if given prescriptionContent matches inner pattern
     * @param prescriptionContent - checked prescriptionContent, can be null
     * @return boolean true if prescriptionContent is valid and not null, false if not
     */
    boolean validatePrescriptionContent(String prescriptionContent);

    /**validates date
     *
     * checks if date is in the JDBC date escape format (yyyy-[m]m-[d]d)
     * @param date - checked date, can be null
     * @return boolean true if date is valid and not null, false if not
     */
    boolean validateDate(String date);

    /**validates date
     *
     * checks if date is in the JDBC date escape format (yyyy-[m]m-[d]d) or is empty String
     * @param date - checked date, can be null
     * @return boolean true if date is valid and not null or is empty String, false if not
     */
    boolean validateDateAndEmpty(String date);

    /** validates diagnosis
     *
     * checks if given diagnosis matches inner pattern or is empty string
     * @param diagnosis - checked diagnosis, can be null
     * @return boolean true if diagnosis is valid or is empty string and not null, false if not
     */
    boolean validateDiagnosisAndEmpty(String diagnosis);

}

package by.epam.hospital.service;

import by.epam.hospital.service.impl.AuthorizationServiceImpl;
import by.epam.hospital.service.impl.SystemServiceImpl;
import by.epam.hospital.service.impl.util.IPaginationUtil;
import by.epam.hospital.service.impl.util.PaginationUtil;
import by.epam.hospital.service.impl.PatientServiceImpl;
import by.epam.hospital.service.impl.PrescriptionServiceImpl;
import by.epam.hospital.service.impl.ValidationService;
import by.epam.hospital.service.impl.WorkerServiceImpl;

public abstract class ServiceHolder {

    public static ISystemService getCommonService(){
	return SystemServiceImpl.getInstance();
    }
    public static IValidationService getValidationService() { return ValidationService.getInstance();}
    public static IWorkerService getWorkerService() {return WorkerServiceImpl.getInstance();}
    public static IPrescriptionService getPrescriptionService() {return PrescriptionServiceImpl.getInstance();}
    public static IPatientService getPatientService() {return PatientServiceImpl.getInstance();}
    public static IPaginationUtil getPaginationUtil() {return PaginationUtil.getInstance();}
    public static IAuthorizationService getAuthorizationService() {return AuthorizationServiceImpl.getInstance();}

}

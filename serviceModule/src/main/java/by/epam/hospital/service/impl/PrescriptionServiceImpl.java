package by.epam.hospital.service.impl;


import by.epam.hospital.dao.DaoHolder;
import by.epam.hospital.dao.IPatientDao;
import by.epam.hospital.dao.IPrescriptionDao;
import by.epam.hospital.dao.IWorkerDao;
import by.epam.hospital.dao.exception.DaoException;
import by.epam.hospital.dao.entity.Prescription;
import by.epam.hospital.dao.entity.PrescriptionStatus;
import by.epam.hospital.dao.entity.PrescriptionType;
import by.epam.hospital.service.impl.util.IPaginationUtil;
import by.epam.hospital.service.IPrescriptionService;
import by.epam.hospital.service.ServiceHolder;
import by.epam.hospital.service.exception.ServiceException;
import by.epam.hospital.service.exception.ServiceExceptionMessage;
import by.epam.hospital.service.model.PaginatedListModel;
import by.epam.hospital.service.model.PrescriptionModel;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class PrescriptionServiceImpl implements IPrescriptionService {

    private final static PrescriptionServiceImpl INSTANCE = new PrescriptionServiceImpl();
    private final IPatientDao patientDao = DaoHolder.getPatientDao();
    private final IWorkerDao workerDao = DaoHolder.getWorkerDao();
    private final IPrescriptionDao prescriptionDao = DaoHolder.getPrescriptionDao();
    private final IPaginationUtil paginationService = ServiceHolder.getPaginationUtil();
    private final static String EMPTY_STRING = "";
    private final static String PERCENT = "%";

    private PrescriptionServiceImpl() {
    }

    public static PrescriptionServiceImpl getInstance() {
	return INSTANCE;
    }

    @Override
    public PaginatedListModel<PrescriptionModel> getPaginatedUndoneNursePrescriptions(String filter, String stringPage,
									String stringItemsPerPage) throws ServiceException {
	filter = getSaveFilter(filter);
	int patientCount = getNursePrescriptionCount(filter);
	int itemsPerPage = paginationService.getItemsPerPage(stringItemsPerPage);
	int pageCount = paginationService.getPageCount(itemsPerPage, patientCount);
	int page = paginationService.getPage(stringPage, pageCount);
	int limitFirst = paginationService.countFirstLimit(page, itemsPerPage);

	List<PrescriptionModel> prescriptionModels = getUndoneNursePrescriptions(filter, limitFirst, itemsPerPage);

	PaginatedListModel<PrescriptionModel> prescriptionPaginatedListModel = new PaginatedListModel<>();
	prescriptionPaginatedListModel.setModelList(prescriptionModels);
	prescriptionPaginatedListModel.setItemsPerPage(itemsPerPage);
	prescriptionPaginatedListModel.setPage(page);
	prescriptionPaginatedListModel.setPageCount(pageCount);
	return prescriptionPaginatedListModel;
    }

    private List<PrescriptionModel> getUndoneNursePrescriptions(String filter, int limitFirst, int itemsPerPage) throws ServiceException{
	List<PrescriptionModel> prescriptionModels = new ArrayList<>();
	try {
	    List<Prescription> prescriptions = prescriptionDao.getAssignedNursePrescriptionList(filter, limitFirst, itemsPerPage);
	    for (Prescription prescription : prescriptions) {
		PrescriptionModel prescriptionModel = getFilledPrescriptionModel(prescription);
		prescriptionModels.add(prescriptionModel);
	    }
	} catch (DaoException e) {
	    throw new ServiceException(ServiceExceptionMessage.ERROR_GETTING_UNDONE_NURSE_PRESCRIPTIONS, e);
	}
	return prescriptionModels;
    }

    @Override
    public void doPrescription(int prescriptionId) throws ServiceException {
	try {
	    Prescription prescription = prescriptionDao.getEntity(prescriptionId);
	    PrescriptionStatus status = prescription.getStatus();
	    if (status != null && status == PrescriptionStatus.ASSIGNED) {
		prescription.setStatus(PrescriptionStatus.DONE);
		prescriptionDao.updateEntity(prescription);
	    }
	} catch (DaoException e) {
	    throw new ServiceException(ServiceExceptionMessage.ERROR_FULFILLING_PRESCRIPTION, e);
	}
    }

    @Override
    public PaginatedListModel<PrescriptionModel> getPaginatedDoctorsOperations(int doctorId, String filter, String stringPage,
								 String stringItemsPerPage) throws ServiceException {
	filter = getSaveFilter(filter);
	int patientCount = getDoctorOperationCount(doctorId, filter);
	int itemsPerPage = paginationService.getItemsPerPage(stringItemsPerPage);
	int pageCount = paginationService.getPageCount(itemsPerPage, patientCount);
	int page = paginationService.getPage(stringPage, pageCount);
	int limitFirst = paginationService.countFirstLimit(page, itemsPerPage);

	List<PrescriptionModel> prescriptionModels = getDoctorsOperations(doctorId, filter, limitFirst, itemsPerPage);

	PaginatedListModel<PrescriptionModel> prescriptionPaginatedListModel = new PaginatedListModel<>();
	prescriptionPaginatedListModel.setModelList(prescriptionModels);
	prescriptionPaginatedListModel.setItemsPerPage(itemsPerPage);
	prescriptionPaginatedListModel.setPage(page);
	prescriptionPaginatedListModel.setPageCount(pageCount);
	return prescriptionPaginatedListModel;


    }

    private List<PrescriptionModel> getDoctorsOperations(int doctorId, String filter, int limitFirst, int itemsPerPage)
	throws ServiceException{
	List<PrescriptionModel> prescriptionModels = new ArrayList<>();
	try {
	    List<Prescription> prescriptions = prescriptionDao.getDoctorsOperationList(doctorId, filter, limitFirst,
		itemsPerPage);
	    for (Prescription prescription : prescriptions) {
		PrescriptionModel prescriptionModel = getFilledPrescriptionModel(prescription);
		prescriptionModels.add(prescriptionModel);
	    }
	} catch (DaoException e) {
	    throw new ServiceException(ServiceExceptionMessage.ERROR_GETTING_DOCTOR_OPERATION_LIST, e);
	}
	return prescriptionModels;
    }

    private int getDoctorOperationCount(int doctorId, String filter) throws ServiceException{
	int doctorOperationCount;
	try{
	   doctorOperationCount = prescriptionDao.getDoctorsOperationsCount(doctorId, filter);
	}catch (DaoException e){
	    throw new ServiceException(ServiceExceptionMessage.ERROR_GETTING_DOCTOR_OPERATION_COUNT, e);
	}
	return doctorOperationCount;
    }

    private int getNursePrescriptionCount(String filter) throws ServiceException{
	int nursePrescriptionCount;
	try{
	    nursePrescriptionCount = prescriptionDao.getNursePrescriptionCount(filter);
	}catch (DaoException e){
	    throw new ServiceException(ServiceExceptionMessage.ERROR_GETTING_NURSE_PRESCRIPTION_COUNT, e);
	}
	return nursePrescriptionCount;
    }

    private PrescriptionModel getFilledPrescriptionModel(Prescription prescription) throws DaoException {
	PrescriptionModel prescriptionModel = new PrescriptionModel();
	if (prescription!=null) {
	    prescriptionModel.setPrescriptionId(prescription.getPrescriptionId());
	    prescriptionModel.setPrescriptionContent(prescription.getContent());
	    prescriptionModel.setPrescriptionType(prescription.getType().name().toLowerCase());
	    prescriptionModel.setPrescriptionStatus(prescription.getStatus().name().toLowerCase());
	    prescriptionModel.setAppointor(workerDao.getWorkerFullName(prescription.getWorkerId()));
	    prescriptionModel.setPatientName(patientDao.getPatientFullNameById(prescription.getPatientId()));
	    prescriptionModel.setDate(prescription.getDate().toString());
	    prescriptionModel.setPatientId(prescription.getPatientId());
	}
	return prescriptionModel;
    }

    private String getSaveFilter(String filter){
	if (filter == null){
	    filter = EMPTY_STRING;
	}else{
	    filter=filter.replace(PERCENT,EMPTY_STRING);
	}
	filter=PERCENT+filter+PERCENT;
	return filter;
    }

    @Override
    public List<String> getPrescriptionTypes() {
	PrescriptionType[] prescriptionTypes = PrescriptionType.values();
	List<String> stringTypes = new ArrayList<>();
	for (PrescriptionType prescriptionType : prescriptionTypes) {
	    stringTypes.add(prescriptionType.name().toLowerCase());
	}
	return stringTypes;
    }

    @Override
    public List<String> getPrescriptionStatuses() {
	PrescriptionStatus[] prescriptionStatuses = PrescriptionStatus.values();
	List<String> stringStatuses = new ArrayList<>();
	for (PrescriptionStatus prescriptionStatus : prescriptionStatuses) {
	    stringStatuses.add(prescriptionStatus.name().toLowerCase());
	}
	return stringStatuses;
    }

    @Override
    public void addPrescription(int patientId, int doctorId, String prescriptionContent, String prescriptionType)
	throws ServiceException {
	Prescription prescription = new Prescription();
	prescription.setPatientId(patientId);
	prescription.setContent(prescriptionContent);
	prescription.setType(PrescriptionType.valueOf(prescriptionType.toUpperCase()));
	prescription.setStatus(PrescriptionStatus.ASSIGNED);
	prescription.setWorkerId(doctorId);
	prescription.setDate(new Date(System.currentTimeMillis()));
	try {
	    prescriptionDao.addEntity(prescription);
	} catch (DaoException e) {
	    throw new ServiceException(ServiceExceptionMessage.ERROR_ADDING_PRESCRIPTION, e);
	}
    }

    @Override
    public List<PrescriptionModel> getPatientsPrescriptions(int patientId) throws ServiceException {
	List<PrescriptionModel> prescriptionModels = new ArrayList<>();
	try {
	    List<Prescription> prescriptions = prescriptionDao.getPatientsPrescriptions(patientId);
	    for (Prescription prescription : prescriptions) {
		PrescriptionModel prescriptionModel = getFilledPrescriptionModel(prescription);
		prescriptionModels.add(prescriptionModel);
	    }
	} catch (DaoException e) {
	    throw new ServiceException(ServiceExceptionMessage.ERROR_GETTING_PATIENTS_PRESCRIPTIONS);
	}
	return prescriptionModels;
    }

    @Override
    public PrescriptionModel getPrescription(int prescriptionId) throws ServiceException {
	PrescriptionModel prescriptionModel;
	try {
	    Prescription prescription = prescriptionDao.getEntity(prescriptionId);
	    prescriptionModel = getFilledPrescriptionModel(prescription);
	} catch (DaoException e) {
	    throw new ServiceException(ServiceExceptionMessage.ERROR_GETTING_PRESCRIPTION, e);
	}
	return prescriptionModel;
    }

    @Override
    public void deletePrescription(int prescriptionId) throws ServiceException {
	try {
	    prescriptionDao.deleteEntity(prescriptionId);
	} catch (DaoException e) {
	    throw new ServiceException(ServiceExceptionMessage.ERROR_DELETING_PRESCRIPTION, e);
	}
    }

    @Override
    public int getPatientIdByPrescription(int prescriptionId) throws ServiceException {
	int patientId;
	try {
	    patientId = prescriptionDao.getPatientIdByPrescription(prescriptionId);
	} catch (DaoException e) {
	    throw new ServiceException(ServiceExceptionMessage.ERROR_GETTING_PATIENT_ID_BY_PRESCRIPTION, e);
	}
	return patientId;
    }

    @Override
    public void editPrescription(int prescriptionId, int workerId, String prescriptionContent, String prescriptionDate,
				 String prescriptionStatus, String prescriptionType) throws ServiceException {
	try {
	    Prescription prescription = prescriptionDao.getEntity(prescriptionId);
	    prescription.setWorkerId(workerId);
	    prescription.setContent(prescriptionContent);
	    prescription.setDate(Date.valueOf(prescriptionDate));
	    prescription.setStatus(PrescriptionStatus.valueOf(prescriptionStatus.toUpperCase()));
	    prescription.setType(PrescriptionType.valueOf(prescriptionType.toUpperCase()));
	    prescriptionDao.updateEntity(prescription);
	} catch (DaoException e) {
	    throw new ServiceException(ServiceExceptionMessage.ERROR_EDITING_PRESCRIPTION, e);
	}
    }

}

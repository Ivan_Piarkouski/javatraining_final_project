package by.epam.hospital.service.impl;

import by.epam.hospital.dao.util.ConnectionPull;
import by.epam.hospital.service.ISystemService;

public class SystemServiceImpl implements ISystemService {


    private final static SystemServiceImpl INSTANCE = new SystemServiceImpl();

    private SystemServiceImpl() {
    }

    public static SystemServiceImpl getInstance() {
	return INSTANCE;
    }

    @Override
    public void initDao() {
	ConnectionPull.getInstance().initPool();
    }

    @Override
    public void shutDownDao() {
	ConnectionPull.getInstance().closePull();
    }

}

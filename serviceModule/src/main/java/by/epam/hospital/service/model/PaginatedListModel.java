package by.epam.hospital.service.model;


import java.util.List;

public class PaginatedListModel <T>{

    private List<T> modelList;
    private int page;
    private int pageCount;
    private int itemsPerPage;

    public List<T> getModelList() {
	return modelList;
    }

    public void setModelList(List<T> modelList) {
	this.modelList = modelList;
    }

    public int getPage() {
	return page;
    }

    public void setPage(int page) {
	this.page = page;
    }

    public int getPageCount() {
	return pageCount;
    }

    public void setPageCount(int pageCount) {
	this.pageCount = pageCount;
    }

    public int getItemsPerPage() {
	return itemsPerPage;
    }

    public void setItemsPerPage(int itemsPerPage) {
	this.itemsPerPage = itemsPerPage;
    }
}

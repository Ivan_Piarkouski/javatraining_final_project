package by.epam.hospital.service;

import by.epam.hospital.service.exception.ServiceException;
import by.epam.hospital.service.model.PaginatedListModel;
import by.epam.hospital.service.model.PrescriptionModel;

import java.util.List;

public interface IPrescriptionService {

    /**returns part of undone prescriptions list in PaginatedListModel
     *
     * returns PaginatedListModel, containing list of assigned prescriptions, which can be fulfilled by nurse and are assigned
     * to patients with fullNames like given filter; its already paginated, contains info on current page,
     * items per page and total number of pages
     *
     * @param filter - string to filter prescriptions by patient fullName
     * @param page - page number in String, should be valid
     * @param itemsPerPage - items per page in String, should be valid
     * @return PaginatedListModel<PrescriptionModel> contains list of PrescriptionModels satisfying given params,
     * 		current page, items per page and total number of pages, if no items satisfy params list will be empty
     * @throws ServiceException, NumberFormatException
     */
    PaginatedListModel<PrescriptionModel> getPaginatedUndoneNursePrescriptions(String filter, String page,
									       String itemsPerPage) throws ServiceException;

    /**fulfils prescription
     *
     * @param prescriptionId, positive int
     * @throws ServiceException
     */
    void doPrescription(int prescriptionId) throws ServiceException;

    /**returns part of undone operations list in PaginatedListModel for concrete doctor by doctorsId
     *
     * @param doctorId - positive int
     * @param filter - string to filter prescriptions by patient fullName
     * @param page - page number in String, should be valid
     * @param itemsPerPage - items per page in String, should be valid
     * @return PaginatedListModel<PrescriptionModel> contains list of PrescriptionModels satisfying given params if
     * no items satisfy params list will be empty also contains current page, items per page and total number of pages,
     * @throws ServiceException,  NumberFormatException
     */
    PaginatedListModel<PrescriptionModel> getPaginatedDoctorsOperations(int doctorId, String filter, String page, String itemsPerPage)
	throws ServiceException;

    /**returns list of prescription types
     *
     * @return  List<String> containing prescription types
     */
    List<String> getPrescriptionTypes();

    /**returns list of prescription statuses
     *
     * @return List<String> containing prescription statuses
     */
    List<String> getPrescriptionStatuses();

    /**adds new prescription
     *
     * @param patientId - valid patient id
     * @param doctorId - valid worker id
     * @param prescriptionContent - String, not null
     * @param prescriptionType - valid string type
     * @throws ServiceException, IllegalArgumentException
     */
    void addPrescription(int patientId, int doctorId, String prescriptionContent, String prescriptionType)
	throws ServiceException;

    /**returns prescriptions of one concrete patient by patient id
     *
     * @param patientId positive int
     * @return List<PrescriptionModel>, if there no such patient return empty list
     * @throws ServiceException
     */
    List<PrescriptionModel> getPatientsPrescriptions(int patientId) throws ServiceException;

    /**returns prescription by its id
     *
     * @param prescriptionId - positive int
     * @return PrescriptionModel, if there is no such prescription model will be empty
     * @throws ServiceException
     */
    PrescriptionModel getPrescription(int prescriptionId) throws ServiceException;

    /**deletes prescription by its id
     *
     * @param prescriptionId - positive int
     * @throws ServiceException
     */
    void deletePrescription(int prescriptionId) throws ServiceException;

    /**edits prescription
     *
     * @param prescriptionId positive int
     * @param workerId - valid worker id
     * @param prescriptionContent - not null
     * @param prescriptionDate - valid, not null
     * @param prescriptionStatus- valid, not null
     * @param prescriptionType - valid, not null
     * @throws ServiceException
     */
    void editPrescription(int prescriptionId, int workerId, String prescriptionContent, String prescriptionDate,
			  String prescriptionStatus, String prescriptionType) throws ServiceException;

    /**returns patient id by prescription id
     *
     * @param prescriptionId - positive int
     * @return int patientId
     * @throws ServiceException
     */
    int getPatientIdByPrescription(int prescriptionId) throws ServiceException;
}

package by.epam.hospital.service.model;


public class PatientListModel {

    private int patientId;
    private String fullName;
    private String inDate;
    private String treatingDoctor;
    private String prescriptionStatus;
    private boolean initDiagnosis;

    public int getPatientId() {
	return patientId;
    }

    public void setPatientId(int patientId) {
	this.patientId = patientId;
    }

    public String getFullName() {
	return fullName;
    }

    public void setFullName(String fullName) {
	this.fullName = fullName;
    }

    public String getInDate() {
	return inDate;
    }

    public void setInDate(String inDate) {
	this.inDate = inDate;
    }

    public String getTreatingDoctor() {
	return treatingDoctor;
    }

    public void setTreatingDoctor(String treatingDoctor) {
	this.treatingDoctor = treatingDoctor;
    }

    public String getPrescriptionStatus() {
	return prescriptionStatus;
    }

    public void setPrescriptionStatus(String prescriptionStatus) {
	this.prescriptionStatus = prescriptionStatus;
    }

    public boolean isInitDiagnosis() {
	return initDiagnosis;
    }

    public void setInitDiagnosis(boolean initDiagnosis) {
	this.initDiagnosis = initDiagnosis;
    }

    @Override
    public boolean equals(Object o) {
	if (this == o) {return true;}
	if (o == null || getClass() != o.getClass()) {return false;}

	PatientListModel that = (PatientListModel) o;

	if (initDiagnosis != that.initDiagnosis) {return false;}
	if (patientId != that.patientId) {return false;}
	if (fullName != null ? !fullName.equals(that.fullName) : that.fullName != null) {return false;}
	if (inDate != null ? !inDate.equals(that.inDate) : that.inDate != null) {return false;}
	if (prescriptionStatus != null ? !prescriptionStatus.equals(that.prescriptionStatus) : that.prescriptionStatus != null)
	{return false;}
	if (treatingDoctor != null ? !treatingDoctor.equals(that.treatingDoctor) : that.treatingDoctor != null)
	{return false;}

	return true;
    }

    @Override
    public int hashCode() {
	int result = patientId;
	result = 31 * result + (fullName != null ? fullName.hashCode() : 0);
	result = 31 * result + (inDate != null ? inDate.hashCode() : 0);
	result = 31 * result + (treatingDoctor != null ? treatingDoctor.hashCode() : 0);
	result = 31 * result + (prescriptionStatus != null ? prescriptionStatus.hashCode() : 0);
	result = 31 * result + (initDiagnosis ? 1 : 0);
	return result;
    }

    @Override
    public String toString() {
	return "PatientListModel{" +
	    "patientId=" + patientId +
	    ", fullName='" + fullName + '\'' +
	    ", inDate='" + inDate + '\'' +
	    ", treatingDoctor='" + treatingDoctor + '\'' +
	    ", prescriptionStatus='" + prescriptionStatus + '\'' +
	    ", initDiagnosis=" + initDiagnosis +
	    '}';
    }
}

package by.epam.hospital.service.model;

public class LogInModel {
    private int id;
    private String fullName;
    private String position;
    private String status;

    public int getId() {
	return id;
    }

    public void setId(int id) {
	this.id = id;
    }

    public String getFullName() {
	return fullName;
    }

    public void setFullName(String fullName) {
	this.fullName = fullName;
    }

    public String getPosition() {
	return position;
    }

    public void setPosition(String position) {
	this.position = position;
    }

    public String getStatus() {
	return status;
    }

    public void setStatus(String status) {
	this.status = status;
    }

    @Override
    public boolean equals(Object o) {

	if (this == o) {return true;}
	if (o == null || getClass() != o.getClass()) {return false;}

	LogInModel that = (LogInModel) o;

	if (id != that.id) return false;
	if (fullName != null ? !fullName.equals(that.fullName) : that.fullName != null) {return false;}
	if (position != null ? !position.equals(that.position) : that.position != null) {return false;}
	if (status != null ? !status.equals(that.status) : that.status != null) {return false;}

	return true;
    }

    @Override
    public int hashCode() {
	int result = id;
	result = 31 * result + (fullName != null ? fullName.hashCode() : 0);
	result = 31 * result + (position != null ? position.hashCode() : 0);
	result = 31 * result + (status != null ? status.hashCode() : 0);
	return result;
    }

    @Override
    public String toString() {
	return "LogInModel{" +
	    "id=" + id +
	    ", fullName='" + fullName + '\'' +
	    ", position='" + position + '\'' +
	    ", status='" + status + '\'' +
	    '}';
    }
}

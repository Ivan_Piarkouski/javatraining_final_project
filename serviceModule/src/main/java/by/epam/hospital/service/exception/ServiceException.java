package by.epam.hospital.service.exception;


public class ServiceException extends Exception {

    public ServiceException(ServiceExceptionMessage exceptionMessage) {
	super(exceptionMessage.getErrorMessage());
    }

    public ServiceException(ServiceExceptionMessage exceptionMessage, Throwable cause) {
	super(exceptionMessage.getErrorMessage(), cause);
    }
}

package by.epam.hospital.service;

import by.epam.hospital.service.exception.ServiceException;
import by.epam.hospital.service.model.LogInModel;

public interface IAuthorizationService {

    /**checks if given LogInModel has position NURSE or not
     *
     * @param logInModel logInModel to check, not null
     * @return boolean true if NURSE, otherwise false
     */
    boolean authorizeNurse(LogInModel logInModel);

    /**checks if given LogInModel has position DOCTOR or CHIEF_DOCTOR or not
     *
     * @param logInModel logInModel to check, not null
     * @return boolean true if DOCTOR or CHIEF_DOCTOR, otherwise false
     */
    boolean authorizeDoctorOrChief(LogInModel logInModel);

    /**checks if given LogInModel has position CHIEF_DOCTOR or not
     *
     * @param logInModel logInModel to check, not null
     * @return boolean true if CHIEF_DOCTOR, otherwise false
     */
    boolean authorizeChief(LogInModel logInModel);

    /** checks if init diagnosis can be set
     *
     * checks if LogInModel is authorised to set diagnosis to given patient, and that diagnosis is not set yet
     * @param logInModel - logInModel to check, not null
     * @param patientId - id of patient we are going to set diagnosis
     * @return boolean true if diagnosis can be set, false if not
     * @throws ServiceException
     */
    boolean authorizeInitDiagnosisSet(LogInModel logInModel, int patientId) throws ServiceException;

    /** checks if patient can be sign out and final diagnosis can be set
     *
     * checks if LogInModel is authorised to sign out and set diagnosis to given patient, and that init diagnosis is  already
     * set, and patient not sign out yet
     * @param logInModel - logInModel to check, not null
     * @param patientId - id of patient we are going to sign out and set final diagnosis
     * @return boolean true if final diagnosis can be set and patient signed out, false if not
     * @throws ServiceException
     */
    boolean authorizePatientSignOut(LogInModel logInModel, int patientId) throws ServiceException;

    /** checks if patient can be deleted
     *
     * allows to delete patient if he/she don't have init diagnosis yet or position of LogInModel is CHIEF_DOCTOR
     * @param logInModel - logInModel to check, not null
     * @param patientId - id of patient we are going to delete
     * @return boolean true if patient can be deleted, false if not
     * @throws ServiceException
     */
    boolean authorizePatientDelete(LogInModel logInModel, int patientId) throws ServiceException;

    /** checks if prescription can be set to the given patient
     *
     * checks if LogInModel is authorised to set prescription to given patient, and that init diagnosis is  already
     * set, and patient not sign out yet
     * @param logInModel - logInModel to check, not null
     * @param patientId - id of patient we are going to set prescription
     * @return boolean true if prescription can be set, false if not
     * @throws ServiceException
     */
    boolean authorizePrescriptionAdd(LogInModel logInModel, int patientId) throws ServiceException;

    /**checks if prescription can be deleted
     *
     * checks if LogInModel has position CHIEF_DOCTOR or if the prescription has not done yet and LogInModel is DOCTOR
     * who assigned this prescription
     * @param logInModel - logInModel to check, not null
     * @param prescriptionId - id of prescription we are going to delete
     * @return boolean true if prescription can be deleted, false if not
     * @throws ServiceException
     */
    boolean authorizePrescriptionDelete(LogInModel logInModel, int prescriptionId) throws ServiceException;

    /**checks if prescription can be done
     *
     * Operations can be done only by doctors, who assigned them, other prescriptions can be done by nurses and chief
     * doctor or by treating doctor
     * @param logInModel - logInModel to check, not null
     * @param prescriptionId - id of prescription we are going to do
     * @return boolean true if prescription can be done, false if not
     * @throws ServiceException
     */
    boolean authorizePrescriptionDo(LogInModel logInModel, int prescriptionId) throws ServiceException;

    /**checks if prescription can be edited
     *
     * checks if LogInModel has position CHIEF_DOCTOR or if the prescription has not done yet and LogInModel is DOCTOR
     * who assigned this prescription
     * @param logInModel - logInModel to check, not null
     * @param prescriptionId - id of prescription we are going to edit
     * @return boolean true if prescription can be edited, false if not
     * @throws ServiceException
     */
    boolean authorizePrescriptionEdit(LogInModel logInModel, int prescriptionId) throws ServiceException;

}

package by.epam.hospital.service.model;

public class WorkerViewModel {

    private int id;
    private String fullName;

    public int getId() {
	return id;
    }

    public void setId(int id) {
	this.id = id;
    }

    public String getFullName() {
	return fullName;
    }

    public void setFullName(String fullName) {
	this.fullName = fullName;
    }

    @Override
    public boolean equals(Object o) {
	if (this == o) {return true;}
	if (o == null || getClass() != o.getClass()) {return false;}

	WorkerViewModel that = (WorkerViewModel) o;

	if (id != that.id) {return false;}
	if (fullName != null ? !fullName.equals(that.fullName) : that.fullName != null) {return false;}

	return true;
    }

    @Override
    public int hashCode() {
	int result = id;
	result = 31 * result + (fullName != null ? fullName.hashCode() : 0);
	return result;
    }

    @Override
    public String toString() {
	return "WorkerViewModel{" +
	    "id=" + id +
	    ", fullName='" + fullName + '\'' +
	    '}';
    }
}

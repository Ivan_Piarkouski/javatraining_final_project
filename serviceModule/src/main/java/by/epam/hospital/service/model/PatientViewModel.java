package by.epam.hospital.service.model;

public class PatientViewModel {

    private int patientId;
    private String fullName;
    private String address;
    private String patientStatus;
    private String initDiagnosis;
    private String finalDiagnosis;
    private String treatingDoctor;
    private int doctorId;
    private String inDate;
    private String outDate;

    public int getPatientId() {
	return patientId;
    }

    public void setPatientId(int patientId) {
	this.patientId = patientId;
    }

    public String getFullName() {
	return fullName;
    }

    public void setFullName(String fullName) {
	this.fullName = fullName;
    }

    public String getAddress() {
	return address;
    }

    public void setAddress(String address) {
	this.address = address;
    }

    public String getPatientStatus() {
	return patientStatus;
    }

    public void setPatientStatus(String patientStatus) {
	this.patientStatus = patientStatus;
    }

    public String getInitDiagnosis() {
	return initDiagnosis;
    }

    public void setInitDiagnosis(String initDiagnosis) {
	this.initDiagnosis = initDiagnosis;
    }

    public String getFinalDiagnosis() {
	return finalDiagnosis;
    }

    public void setFinalDiagnosis(String finalDiagnosis) {
	this.finalDiagnosis = finalDiagnosis;
    }

    public String getTreatingDoctor() {
	return treatingDoctor;
    }

    public void setTreatingDoctor(String treatingDoctor) {
	this.treatingDoctor = treatingDoctor;
    }

    public String getOutDate() {
	return outDate;
    }

    public void setOutDate(String outDate) {
	this.outDate = outDate;
    }

    public String getInDate() {
	return inDate;
    }

    public void setInDate(String inDate) {
	this.inDate = inDate;
    }

    public int getDoctorId() {
	return doctorId;
    }

    public void setDoctorId(int doctorId) {
	this.doctorId = doctorId;
    }

    @Override
    public boolean equals(Object o) {
	if (this == o) {return true;}
	if (o == null || getClass() != o.getClass()) {return false;}

	PatientViewModel that = (PatientViewModel) o;

	if (doctorId != that.doctorId) {return false;}
	if (patientId != that.patientId) {return false;}
	if (address != null ? !address.equals(that.address) : that.address != null) {return false;}
	if (finalDiagnosis != null ? !finalDiagnosis.equals(that.finalDiagnosis) : that.finalDiagnosis != null)
		{return false;}
	if (fullName != null ? !fullName.equals(that.fullName) : that.fullName != null) {return false;}
	if (inDate != null ? !inDate.equals(that.inDate) : that.inDate != null) {return false;}
	if (initDiagnosis != null ? !initDiagnosis.equals(that.initDiagnosis) : that.initDiagnosis != null)
		{return false;}
	if (outDate != null ? !outDate.equals(that.outDate) : that.outDate != null) {return false;}
	if (patientStatus != null ? !patientStatus.equals(that.patientStatus) : that.patientStatus != null)
		{return false;}
	if (treatingDoctor != null ? !treatingDoctor.equals(that.treatingDoctor) : that.treatingDoctor != null)
		{return false;}

	return true;
    }

    @Override
    public int hashCode() {
	int result = patientId;
	result = 31 * result + (fullName != null ? fullName.hashCode() : 0);
	result = 31 * result + (address != null ? address.hashCode() : 0);
	result = 31 * result + (patientStatus != null ? patientStatus.hashCode() : 0);
	result = 31 * result + (initDiagnosis != null ? initDiagnosis.hashCode() : 0);
	result = 31 * result + (finalDiagnosis != null ? finalDiagnosis.hashCode() : 0);
	result = 31 * result + (treatingDoctor != null ? treatingDoctor.hashCode() : 0);
	result = 31 * result + doctorId;
	result = 31 * result + (inDate != null ? inDate.hashCode() : 0);
	result = 31 * result + (outDate != null ? outDate.hashCode() : 0);
	return result;
    }

    @Override
    public String toString() {
	return "PatientViewModel{" +
	    "patientId=" + patientId +
	    ", fullName='" + fullName + '\'' +
	    ", address='" + address + '\'' +
	    ", patientStatus='" + patientStatus + '\'' +
	    ", initDiagnosis='" + initDiagnosis + '\'' +
	    ", finalDiagnosis='" + finalDiagnosis + '\'' +
	    ", treatingDoctor='" + treatingDoctor + '\'' +
	    ", doctorId=" + doctorId +
	    ", inDate='" + inDate + '\'' +
	    ", outDate='" + outDate + '\'' +
	    '}';
    }
}

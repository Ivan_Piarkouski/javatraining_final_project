package by.epam.hospital.service;


import by.epam.hospital.service.exception.ServiceException;
import by.epam.hospital.service.model.PaginatedListModel;
import by.epam.hospital.service.model.PatientListModel;
import by.epam.hospital.service.model.PatientViewModel;

import java.util.List;

public interface IPatientService{

    /**adds new patient
     *
     * @param fullName - fullName of new patient, not null
     * @param address - address of new patient, not null
     * @param doctorId - should be valid worker's id
     * @throws ServiceException
     */
    void addPatient(String fullName, String address, int doctorId) throws ServiceException;

    /** returns patient by id
     *
     * @param patientId - patient id, positive int
     * @return PatientViewModel or empty PatientModel if there is no patient with given id
     * @throws ServiceException
     */
    PatientViewModel getPatientById(int patientId) throws ServiceException;

    /**edits patient fullName and address
     *
     * @param fullName - new fullName of patient, not null
     * @param address - new address of patient, not null
     * @param patientId - patient id, positive int
     * @throws ServiceException
     */
    void editPatientData(String fullName, String address, int patientId) throws ServiceException;

    /** returns patient's fullName by id
     *
     * @param patientId patient id, positive int
     * @return String patient's fullName, null if there no such patient
     * @throws ServiceException
     */
    String getPatientFullNameById(int patientId) throws ServiceException;

    /**deletes patient by id
     *
     * @param patientId - patient id, positive int
     * @throws ServiceException
     */
    void deletePatient(int patientId) throws ServiceException;

    /**returns part of doctor's patient list in PaginatedListModel
     *
     *  returns PaginatedListModel, containing list of patients treated by doctor with the given id, with fullNames like
     *  given filter, its already paginated, contains info on current page, items per page and total number of pages
     * @param doctorId - positive int
     * @param filter - string to filter results by fullName
     * @param stringPage - page number in String, should be valid
     * @param stringItemsPerPage - items per page in String, should be valid
     * @return PaginatedListModel<PatientListModel> contains list of PatientListModels satisfying given params,
     * 		current page, items per page and total number of pages, if no items satisfy params list will be empty
     * @throws ServiceException, NumberFormatException
     */
    PaginatedListModel<PatientListModel> getDoctorsCurrentPaginatedPatientList(int doctorId, String filter, String stringPage,
									       String stringItemsPerPage)throws ServiceException;

    /**sets patient init diagnosis
     *
     * @param patientsInitDiagnosis -init diagnosis
     * @param patientId - positive int
     * @throws ServiceException
     */
    void setPatientsInitDiagnosis(String patientsInitDiagnosis, int patientId) throws ServiceException;

    /**returns list of patient statuses in string form
     *
     * @return List<String> string patient statuses
     */
    List<String> getPatientStatuses();

    /**updates patient in database
     *
     *
     * @param patientViewModel - receives patientViewModel, not null, model's patient id should be positive int, doctorsId - a valid
     *                         doctors id; fullName, address, patientStatus, inDate not null and valid
     * @throws ServiceException, IllegalArgumentException
     */
    void fullUpdatePatient(PatientViewModel patientViewModel) throws ServiceException;

    /**signs out patient and sets final diagnosis
     *
     * @param patientId positive int
     * @param finalDiagnosis finalDiagnosis, not null
     * @throws ServiceException
     */
    void signOutPatient(int patientId, String finalDiagnosis) throws ServiceException;

    /**returns part of paginated list of patients on treatment in PaginatedListModel
     *
     * @param filter - string to filter results by fullName
     * @param stringPage - page number in String, should be valid
     * @param stringItemsPerPage - items per page in String, should be valid
     * @return PaginatedListModel<PatientListModel> contains list of PatientListModels satisfying given params,
     * 		current page, items per page and total number of pages, if no items satisfy params list will be empty
     * @throws ServiceException, NumberFormatException
     */
    PaginatedListModel<PatientListModel> getCurrentPaginatedPatientList(String filter, String stringPage, String stringItemsPerPage) throws ServiceException;
}

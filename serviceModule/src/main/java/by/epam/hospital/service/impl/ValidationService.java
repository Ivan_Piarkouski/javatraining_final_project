package by.epam.hospital.service.impl;


import by.epam.hospital.service.IValidationService;

import java.sql.Date;
import java.util.Arrays;
import java.util.List;

public class ValidationService implements IValidationService {

    private final static ValidationService INSTANCE = new ValidationService();

    private final static String FULL_NAME_PATTERN = "^[A-Za-zА-Яа-я\\-]{2,}\\s[A-Za-zА-Яа-я\\-]{2,}(\\s[A-Za-zА-Яа-я\\-]{2,})?$";
    private final static String ADDRESS_PATTERN = "[A-Za-zА-Яа-я\\-_\\.,\\\\/\\s0-9]{3,}";
    private final static String EMAIL_PATTERN = "^[A-Za-z0-9\\.\\-_]+@[A-Za-z0-9\\.\\-_]+\\.[A-Za-z]{2,4}$";
    private final static String DIAGNOSIS_PRESCRIPTION_PATTERN = "^[\\S]{1,}.+";
    private final static String EMPTY_STRING = "";
    private final static String[] LOCALES = {"ru", "en"};
    private final static List <String> listLocales = Arrays.asList(LOCALES);

    private ValidationService() {
    }

    public static ValidationService getInstance() {
	return INSTANCE;
    }

    @Override
    public boolean validateLocale(String locale){
	return locale!=null && listLocales.contains(locale);
    }

    @Override
    public boolean validateDate(String date) {
	try {
	    Date.valueOf(date);
	    return true;
	} catch (IllegalArgumentException e) {
	    return false;
	}
    }

    @Override
    public boolean validateDateAndEmpty(String date) {

	if (date != null && date.equals(EMPTY_STRING)) {
	    return true;
	}
	try {
	    Date.valueOf(date);
	    return true;
	} catch (IllegalArgumentException e) {
	    return false;
	}
    }

    @Override
    public boolean validatePrescriptionContent(String prescriptionContent) {
	return match(DIAGNOSIS_PRESCRIPTION_PATTERN, prescriptionContent);
    }

    @Override
    public boolean validateDiagnosis(String diagnosis) {
	return match(DIAGNOSIS_PRESCRIPTION_PATTERN, diagnosis);
    }

    @Override
    public boolean validateDiagnosisAndEmpty(String diagnosis) {
	return matchEmptyInclude(DIAGNOSIS_PRESCRIPTION_PATTERN, diagnosis);
    }

    @Override
    public boolean validateFullName(String fullName) {
	return match(FULL_NAME_PATTERN, fullName);
    }

    @Override
    public boolean validateAddress(String address) {
	return match(ADDRESS_PATTERN, address);
    }

    @Override
    public boolean validateEmail(String email) {
	return match(EMAIL_PATTERN, email);
    }

    private boolean match(String pattern, String matcher) {
	boolean result = false;
	if (matcher != null && matcher.matches(pattern)) {
	    result = true;
	}
	return result;
    }

    private boolean matchEmptyInclude(String pattern, String matcher) {
	boolean result = false;
	if (matcher != null && (matcher.equals(EMPTY_STRING) || matcher.matches(pattern))) {
	    result = true;
	}
	return result;
    }
}

package by.epam.hospital.service.impl;

import by.epam.hospital.dao.DaoHolder;
import by.epam.hospital.dao.IWorkerDao;
import by.epam.hospital.dao.entity.WorkersStatus;
import by.epam.hospital.dao.exception.DaoException;
import by.epam.hospital.dao.entity.Worker;
import by.epam.hospital.service.IWorkerService;
import by.epam.hospital.service.exception.ServiceException;
import by.epam.hospital.service.exception.ServiceExceptionMessage;
import by.epam.hospital.service.model.LogInModel;
import by.epam.hospital.service.model.WorkerViewModel;

import java.util.ArrayList;
import java.util.List;


public class WorkerServiceImpl implements IWorkerService {

    private final static WorkerServiceImpl INSTANCE = new WorkerServiceImpl();

    private final IWorkerDao workerDao = DaoHolder.getWorkerDao();

    private WorkerServiceImpl(){}

    public static WorkerServiceImpl getInstance(){ return INSTANCE;}

    @Override
    public LogInModel getLoggedUser(String email, String pass) throws ServiceException {
	Worker worker;
	LogInModel logInModel = null;
	try {
	    worker = workerDao.getWorkerByEmail(email);
	} catch (DaoException e) {
	    throw new ServiceException(ServiceExceptionMessage.ERROR_GETTING_LOGGED_USER, e);
	}
	if (worker!=null && pass!=null && pass.hashCode()==worker.getHashPass() && WorkersStatus.IS_WORKING ==
	    worker.getStatus()){
	    logInModel = new LogInModel();
	    logInModel.setId(worker.getId());
	    logInModel.setFullName(worker.getFullName());
	    logInModel.setPosition(worker.getPosition().name());
	    logInModel.setStatus(worker.getStatus().name());
	}
	return logInModel;
    }

    @Override
    public List<WorkerViewModel> getActualDoctorsList() throws ServiceException{
	List<WorkerViewModel> doctors = new ArrayList<>();
	try{
	    List <Worker> workers = workerDao.getCurrentDoctorsList();
	    for(Worker worker: workers){
		WorkerViewModel doctor = new WorkerViewModel();
		doctor.setId(worker.getId());
		doctor.setFullName(worker.getFullName());
		doctors.add(doctor);
	    }
	}catch (DaoException e){
	    throw new ServiceException(ServiceExceptionMessage.ERROR_GETTING_CURRENT_DOCTORS_LIST, e);
	}
	return doctors;
    }
}

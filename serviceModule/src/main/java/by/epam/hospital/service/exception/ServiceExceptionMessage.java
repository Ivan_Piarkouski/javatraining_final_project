package by.epam.hospital.service.exception;


public enum ServiceExceptionMessage {

    //WorkerService error messages
    ERROR_GETTING_LOGGED_USER ("Error while getting logged worker"),
    ERROR_GETTING_CURRENT_DOCTORS_LIST ("Error while getting current doctors list"),

    //PatientService error messages
    ERROR_GETTING_CURRENT_PATIENT_LIST ("Error while getting current patient list"),
    ERROR_GETTING_DOCTORS_PATIENT_LIST ("Error while getting current doctors patient list by doctor id"),
    ERROR_ADDING_PATIENT ("Error while adding new patient"),
    ERROR_GETTING_PATIENT_BY_ID ("Error while getting patient by id"),
    ERROR_EDITING_PATIENT("Error while editing patient data"),
    ERROR_DELETING_PATIENT("Error while deleting patient"),
    ERROR_GETTING_PATIENT_FULL_NAME("Error while getting patient full name"),
    ERROR_SETTING_PATIENT_INIT_DIAGNOSIS("Error while setting patient init diagnosis"),
    ERROR_FULL_UPDATING_PATIENT("Error while updating patient"),
    ERROR_GETTING_CURRENT_PATIENT_COUNT("Error while getting count of current patients"),
    ERROR_GETTING_DOCTORS_PATIENT_COUNT("Error while getting count of current patients by doctor id"),
    ERROR_SIGNING_OUT_PATIENT("Error while signing out patient"),

    //PrescriptionService error messages
    ERROR_FULFILLING_PRESCRIPTION("Error while fulfilling prescription"),
    ERROR_GETTING_UNDONE_NURSE_PRESCRIPTIONS ("Error while getting undone nurse prescriptions"),
    ERROR_GETTING_PATIENTS_PRESCRIPTIONS ("Error getting patients prescriptions"),
    ERROR_ADDING_PRESCRIPTION ("Error while adding new prescription"),
    ERROR_GETTING_PRESCRIPTION ("Error while getting prescription by id"),
    ERROR_DELETING_PRESCRIPTION("Error while deleting prescription"),
    ERROR_EDITING_PRESCRIPTION("Error while editing prescription"),
    ERROR_GETTING_PATIENT_ID_BY_PRESCRIPTION("Error while getting patient id by prescription id"),
    ERROR_GETTING_DOCTOR_OPERATION_COUNT("Error while getting count of doctor's operation"),
    ERROR_GETTING_NURSE_PRESCRIPTION_COUNT("Error while getting count of prescriptions for nurse"),
    ERROR_GETTING_DOCTOR_OPERATION_LIST("Error while getting doctor's operations list"),

    //AuthorizationService error messages
    ERROR_AUTHORIZE_GETTING_PATIENT("Error getting patient while authorization"),
    ERROR_AUTHORIZE_GETTING_PRESCRIPTION("Error getting prescription while authorization");


    private final String errorMessage;
    private ServiceExceptionMessage (String errorMessage){
	this.errorMessage=errorMessage;
    }
    public String getErrorMessage(){
	return errorMessage;
    }
}

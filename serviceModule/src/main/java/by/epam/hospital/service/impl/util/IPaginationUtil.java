package by.epam.hospital.service.impl.util;


public interface IPaginationUtil {

    /**returns items per page
     *
     * if stringItemsPerPage returns default items per page
     * @param stringItemsPerPage - string items per page, should be valid
     * @return int items per page
     * throws NumberFormatExeption
     */
    int getItemsPerPage(String stringItemsPerPage);

    /**returns current page
     *
     * if page is null - returns default page
     * @param page - string page, should be valid
     * @param pageCount - int max number of page
     * @return valid current page
     */
    int getPage(String page, int pageCount);

    /**returns number of pages
     *
     * @param itemsPerPage - positive int
     * @param itemsCount - positive int or 0
     * @return number of pages
     */
    int getPageCount(int itemsPerPage, int itemsCount);

    /**counts from what item list will be started, for pagination
     *
     * @param page - positive int
     * @param itemsPerPage - positive int
     * @return limit fom which list should be started for pagination
     */
    int countFirstLimit(int page, int itemsPerPage);
}

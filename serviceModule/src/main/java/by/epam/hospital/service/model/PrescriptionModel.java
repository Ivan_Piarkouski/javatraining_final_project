package by.epam.hospital.service.model;

public class PrescriptionModel {

    private int prescriptionId;
    private int patientId;
    private String patientName;
    private String prescriptionContent;
    private String prescriptionType;
    private String prescriptionStatus;
    private String appointor;
    private String date;

    public int getPatientId() {

	return patientId;
    }

    public void setPatientId(int patientId) {
	this.patientId = patientId;
    }

    public int getPrescriptionId() {
	return prescriptionId;
    }

    public void setPrescriptionId(int prescriptionId) {
	this.prescriptionId = prescriptionId;
    }

    public String getPrescriptionContent() {
	return prescriptionContent;
    }

    public void setPrescriptionContent(String prescriptionContent) {
	this.prescriptionContent = prescriptionContent;
    }

    public String getPrescriptionType() {
	return prescriptionType;
    }

    public void setPrescriptionType(String prescriptionType) {
	this.prescriptionType = prescriptionType;
    }

    public String getAppointor() {
	return appointor;
    }

    public void setAppointor(String appointor) {
	this.appointor = appointor;
    }

    public String getPatientName() {
	return patientName;
    }

    public void setPatientName(String patientName) {
	this.patientName = patientName;
    }

    public String getPrescriptionStatus() {
	return prescriptionStatus;
    }

    public void setPrescriptionStatus(String prescriptionStatus) {
	this.prescriptionStatus = prescriptionStatus;
    }

    public String getDate() {
	return date;
    }

    public void setDate(String date) {
	this.date = date;
    }

    @Override
    public String toString() {
	return "PrescriptionModel{" +
	    "prescriptionId=" + prescriptionId +
	    ", patientId=" + patientId +
	    ", patientName='" + patientName + '\'' +
	    ", prescriptionContent='" + prescriptionContent + '\'' +
	    ", prescriptionType='" + prescriptionType + '\'' +
	    ", prescriptionStatus='" + prescriptionStatus + '\'' +
	    ", appointor='" + appointor + '\'' +
	    ", date='" + date + '\'' +
	    '}';
    }

    @Override
    public boolean equals(Object o) {
	if (this == o) return true;
	if (o == null || getClass() != o.getClass()) return false;

	PrescriptionModel that = (PrescriptionModel) o;

	if (patientId != that.patientId) return false;
	if (prescriptionId != that.prescriptionId) return false;
	if (appointor != null ? !appointor.equals(that.appointor) : that.appointor != null) return false;
	if (date != null ? !date.equals(that.date) : that.date != null) return false;
	if (patientName != null ? !patientName.equals(that.patientName) : that.patientName != null) return false;
	if (prescriptionContent != null ? !prescriptionContent.equals(that.prescriptionContent) : that.prescriptionContent != null)
	    return false;
	if (prescriptionStatus != null ? !prescriptionStatus.equals(that.prescriptionStatus) : that.prescriptionStatus != null)
	    return false;
	if (prescriptionType != null ? !prescriptionType.equals(that.prescriptionType) : that.prescriptionType != null)
	    return false;

	return true;
    }

    @Override
    public int hashCode() {
	int result = prescriptionId;
	result = 31 * result + patientId;
	result = 31 * result + (patientName != null ? patientName.hashCode() : 0);
	result = 31 * result + (prescriptionContent != null ? prescriptionContent.hashCode() : 0);
	result = 31 * result + (prescriptionType != null ? prescriptionType.hashCode() : 0);
	result = 31 * result + (prescriptionStatus != null ? prescriptionStatus.hashCode() : 0);
	result = 31 * result + (appointor != null ? appointor.hashCode() : 0);
	result = 31 * result + (date != null ? date.hashCode() : 0);
	return result;
    }

}

package by.epam.hospital.service.impl;

import by.epam.hospital.dao.DaoHolder;
import by.epam.hospital.dao.IPatientDao;
import by.epam.hospital.dao.IPrescriptionDao;
import by.epam.hospital.dao.IWorkerDao;
import by.epam.hospital.dao.entity.Patient;
import by.epam.hospital.dao.entity.PatientStatus;
import by.epam.hospital.dao.entity.PrescriptionStatus;
import by.epam.hospital.dao.exception.DaoException;
import by.epam.hospital.service.IPatientService;
import by.epam.hospital.service.ServiceHolder;
import by.epam.hospital.service.exception.ServiceException;
import by.epam.hospital.service.exception.ServiceExceptionMessage;
import by.epam.hospital.service.impl.util.IPaginationUtil;
import by.epam.hospital.service.model.PaginatedListModel;
import by.epam.hospital.service.model.PatientListModel;
import by.epam.hospital.service.model.PatientViewModel;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;


public class PatientServiceImpl implements IPatientService {

    private final static PatientServiceImpl INSTANCE = new PatientServiceImpl();
    private final static String LOCAL_PRESCRIPTION_PREFIX = "local.prescription.";
    private final static String LOCAL_PATIENT_PREFIX = "local.patient.";
    private final static String NO_PRESCRIPTIONS = "noPrescriptions";
    private final static String EMPTY_STRING = "";
    private final static String PERCENT = "%";

    private final IPatientDao patientDao = DaoHolder.getPatientDao();
    private final IWorkerDao workerDao = DaoHolder.getWorkerDao();
    private final IPrescriptionDao prescriptionDao = DaoHolder.getPrescriptionDao();
    private final IPaginationUtil paginationService = ServiceHolder.getPaginationUtil();

    private PatientServiceImpl() {
    }

    public static PatientServiceImpl getInstance() {
	return INSTANCE;
    }


    private int getCurrentPatientCount(String filter) throws ServiceException {
	int patientCount;
	try {
	    patientCount = patientDao.getCurrentPatientCount(filter);
	} catch (DaoException e) {
	    throw new ServiceException(ServiceExceptionMessage.ERROR_GETTING_CURRENT_PATIENT_COUNT, e);
	}
	return patientCount;
    }

    private int getCurrentPatientCount(int doctorId, String filter) throws ServiceException {
	int patientCount;
	try {
	    patientCount = patientDao.getDoctorsCurrentPatientsCount(doctorId, filter);
	} catch (DaoException e) {
	    throw new ServiceException(ServiceExceptionMessage.ERROR_GETTING_DOCTORS_PATIENT_COUNT, e);
	}
	return patientCount;
    }

    @Override
    public PaginatedListModel<PatientListModel> getCurrentPaginatedPatientList(String filter, String stringPage, String stringItemsPerPage) throws ServiceException {
	filter = getSaveFilter(filter);
	int patientCount = getCurrentPatientCount(filter);
	int itemsPerPage = paginationService.getItemsPerPage(stringItemsPerPage);
	int pageCount = paginationService.getPageCount(itemsPerPage, patientCount);
	int page = paginationService.getPage(stringPage, pageCount);
	int limitFirst = paginationService.countFirstLimit(page, itemsPerPage);
	List<PatientListModel> modelList = getCurrentPatientList(filter, limitFirst, itemsPerPage);

	PaginatedListModel<PatientListModel> patientsPaginatedListModel = new PaginatedListModel<>();
	patientsPaginatedListModel.setModelList(modelList);
	patientsPaginatedListModel.setItemsPerPage(itemsPerPage);
	patientsPaginatedListModel.setPage(page);
	patientsPaginatedListModel.setPageCount(pageCount);
	return patientsPaginatedListModel;
    }


    private List<PatientListModel> getCurrentPatientList(String filter, int limitFirst, int itemsPerPage) throws ServiceException {
	List<PatientListModel> modelList = new ArrayList<>();
	try {
	    List<Patient> patientList = patientDao.getCurrentPatientList(filter, limitFirst, itemsPerPage);
	    for (Patient patient : patientList) {
		PatientListModel model = getPatientListModel(patient);
		modelList.add(model);
	    }
	} catch (DaoException e) {
	    throw new ServiceException(ServiceExceptionMessage.ERROR_GETTING_CURRENT_PATIENT_LIST, e);
	}
	return modelList;
    }

    private String getSaveFilter(String filter) {
	if (filter == null) {
	    filter = EMPTY_STRING;
	} else {
	    filter = filter.replace(PERCENT, EMPTY_STRING);
	}
	filter = PERCENT + filter + PERCENT;
	return filter;
    }


    private String getPrescriptionStatusByPatientId(int patientId) throws DaoException {
	String prescriptionStatus = NO_PRESCRIPTIONS;
	List<PrescriptionStatus> statuses = prescriptionDao.getPrescriptionStatusesByPatientId(patientId);
	for (PrescriptionStatus status : statuses) {
	    if (status == PrescriptionStatus.ASSIGNED) {
		prescriptionStatus = status.name();
		break;
	    }
	    prescriptionStatus = PrescriptionStatus.DONE.name();
	}
	return LOCAL_PRESCRIPTION_PREFIX.concat(prescriptionStatus.toLowerCase());
    }


    @Override
    public void addPatient(String fullName, String address, int doctorId) throws ServiceException {
	Patient patient = new Patient();
	patient.setFullName(fullName);
	patient.setAddress(address);
	patient.setDoctorId(doctorId);
	patient.setInDate(new Date(System.currentTimeMillis()));
	patient.setStatus(PatientStatus.ON_TREATMENT);
	try {
	    patientDao.addEntity(patient);
	} catch (DaoException e) {
	    throw new ServiceException(ServiceExceptionMessage.ERROR_ADDING_PATIENT, e);
	}
    }

    @Override
    public PatientViewModel getPatientById(int patientId) throws ServiceException {
	PatientViewModel patientVM = new PatientViewModel();
	try {
	    Patient patient = patientDao.getEntity(patientId);
	    patientVM.setPatientId(patient.getPatientId());
	    patientVM.setFullName(patient.getFullName());
	    patientVM.setDoctorId(patient.getDoctorId());
	    patientVM.setTreatingDoctor(workerDao.getWorkerFullName(patient.getDoctorId()));
	    patientVM.setAddress(patient.getAddress());
	    patientVM.setFinalDiagnosis(patient.getFinalDiagnosis());
	    patientVM.setInitDiagnosis(patient.getInitDiagnosis());
	    patientVM.setInDate(patient.getInDate().toString());
	    patientVM.setPatientStatus(LOCAL_PATIENT_PREFIX.concat(patient.getStatus().name().toLowerCase()));
	    if (patient.getOutDate() != null) {
		patientVM.setOutDate(patient.getOutDate().toString());
	    }
	} catch (DaoException e) {
	    throw new ServiceException(ServiceExceptionMessage.ERROR_GETTING_PATIENT_BY_ID, e);
	}
	return patientVM;
    }

    @Override
    public void editPatientData(String fullName, String address, int patientId) throws ServiceException {
	Patient patient;
	try {
	    patient = patientDao.getEntity(patientId);
	    if (patient != null) {
		patient.setFullName(fullName);
		patient.setAddress(address);
		patientDao.updateEntity(patient);
	    }
	} catch (DaoException e) {
	    throw new ServiceException(ServiceExceptionMessage.ERROR_EDITING_PATIENT, e);
	}
    }

    @Override
    public String getPatientFullNameById(int patientId) throws ServiceException {
	String fullName;
	try {
	    fullName = patientDao.getPatientFullNameById(patientId);
	} catch (DaoException e) {
	    throw new ServiceException(ServiceExceptionMessage.ERROR_GETTING_PATIENT_FULL_NAME, e);
	}
	return fullName;
    }

    @Override
    public void deletePatient(int patientId) throws ServiceException {
	try {
	    prescriptionDao.deletePrescriptionsByPatient(patientId);
	    patientDao.deleteEntity(patientId);
	} catch (DaoException e) {
	    throw new ServiceException(ServiceExceptionMessage.ERROR_DELETING_PATIENT, e);
	}
    }

    @Override
    public PaginatedListModel<PatientListModel> getDoctorsCurrentPaginatedPatientList(int doctorId, String filter, String stringPage, String stringItemsPerPage) throws ServiceException {
	filter = getSaveFilter(filter);
	int patientCount = getCurrentPatientCount(doctorId, filter);
	int itemsPerPage = paginationService.getItemsPerPage(stringItemsPerPage);
	int pageCount = paginationService.getPageCount(itemsPerPage, patientCount);
	int page = paginationService.getPage(stringPage, pageCount);
	int limitFirst = paginationService.countFirstLimit(page, itemsPerPage);
	List<PatientListModel> modelList = getDoctorsCurrentPatientList(doctorId, filter, limitFirst, itemsPerPage);

	PaginatedListModel<PatientListModel> patientsPaginatedListModel = new PaginatedListModel<>();
	patientsPaginatedListModel.setModelList(modelList);
	patientsPaginatedListModel.setItemsPerPage(itemsPerPage);
	patientsPaginatedListModel.setPage(page);
	patientsPaginatedListModel.setPageCount(pageCount);
	return patientsPaginatedListModel;
    }

    private List<PatientListModel> getDoctorsCurrentPatientList(int doctorId, String filter, int limitFirst, int itemsPerPage) throws ServiceException {
	List<PatientListModel> doctorsPatients = new ArrayList<>();
	try {
	    List<Patient> patientList = patientDao.getDoctorsCurrentPatients(doctorId, filter, limitFirst, itemsPerPage);
	    for (Patient patient : patientList) {
		PatientListModel model = getPatientListModel(patient);
		doctorsPatients.add(model);
	    }
	} catch (DaoException e) {
	    throw new ServiceException(ServiceExceptionMessage.ERROR_GETTING_DOCTORS_PATIENT_LIST, e);
	}
	return doctorsPatients;
    }

    private PatientListModel getPatientListModel(Patient patient) throws DaoException {
	PatientListModel model = new PatientListModel();
	if (patient != null) {
	    int patientId = patient.getPatientId();
	    model.setPatientId(patientId);
	    model.setFullName(patient.getFullName());
	    model.setInDate(patient.getInDate().toString());
	    String doctorFullName = workerDao.getWorkerFullName(patient.getDoctorId());
	    model.setTreatingDoctor(doctorFullName);
	    model.setPrescriptionStatus(getPrescriptionStatusByPatientId(patientId));
	    if (patient.getInitDiagnosis() != null) {
		model.setInitDiagnosis(true);
	    } else {
		model.setInitDiagnosis(false);
	    }
	}

	return model;
    }

    @Override
    public void setPatientsInitDiagnosis(String patientsInitDiagnosis, int patientId) throws ServiceException {
	try {
	    Patient patient = patientDao.getEntity(patientId);
	    if(patient!=null) {
		patient.setInitDiagnosis(patientsInitDiagnosis);
		patientDao.updateEntity(patient);
	    }
	} catch (DaoException e) {
	    throw new ServiceException(ServiceExceptionMessage.ERROR_SETTING_PATIENT_INIT_DIAGNOSIS, e);
	}
    }

    @Override
    public List<String> getPatientStatuses() {
	PatientStatus[] patientStatuses = PatientStatus.values();
	List<String> stringStatuses = new ArrayList<>();
	for (PatientStatus patientStatus : patientStatuses) {
	    stringStatuses.add(patientStatus.name().toLowerCase());
	}
	return stringStatuses;
    }

    @Override
    public void fullUpdatePatient(PatientViewModel patientModel) throws ServiceException {
	Patient patient = new Patient();
	patient.setStatus(PatientStatus.valueOf(patientModel.getPatientStatus().toUpperCase()));
	patient.setPatientId(patientModel.getPatientId());
	patient.setAddress(patientModel.getAddress());
	patient.setFullName(patientModel.getFullName());
	patient.setDoctorId(patientModel.getDoctorId());
	patient.setInDate(Date.valueOf(patientModel.getInDate()));
	if (!patientModel.getOutDate().equals(EMPTY_STRING)) {
	    patient.setOutDate(Date.valueOf(patientModel.getOutDate()));
	}
	if (!patientModel.getInitDiagnosis().equals(EMPTY_STRING)) {
	    patient.setInitDiagnosis(patientModel.getInitDiagnosis());
	}
	if (!patientModel.getFinalDiagnosis().equals(EMPTY_STRING)) {
	    patient.setFinalDiagnosis(patientModel.getFinalDiagnosis());
	}
	try {
	    patientDao.updateEntity(patient);
	} catch (DaoException e) {
	    throw new ServiceException(ServiceExceptionMessage.ERROR_FULL_UPDATING_PATIENT, e);
	}
    }

    @Override
    public void signOutPatient(int patientId, String finalDiagnosis) throws ServiceException {
	try {
	    Patient patient = patientDao.getEntity(patientId);
	    if (patient != null && finalDiagnosis != null) {
		patient.setFinalDiagnosis(finalDiagnosis);
		patient.setStatus(PatientStatus.RECOVERED);
		patient.setOutDate(new Date(System.currentTimeMillis()));
		patientDao.updateEntity(patient);
	    } else {
		throw new ServiceException(ServiceExceptionMessage.ERROR_SIGNING_OUT_PATIENT);
	    }
	} catch (DaoException e) {
	    throw new ServiceException(ServiceExceptionMessage.ERROR_SIGNING_OUT_PATIENT);
	}
    }
}

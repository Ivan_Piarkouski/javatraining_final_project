package by.epam.hospital.service.impl;


import by.epam.hospital.dao.DaoHolder;
import by.epam.hospital.dao.IPatientDao;
import by.epam.hospital.dao.IPrescriptionDao;
import by.epam.hospital.dao.entity.PatientStatus;
import by.epam.hospital.dao.exception.DaoException;
import by.epam.hospital.dao.entity.Patient;
import by.epam.hospital.dao.entity.Prescription;
import by.epam.hospital.dao.entity.PrescriptionStatus;
import by.epam.hospital.dao.entity.PrescriptionType;
import by.epam.hospital.dao.entity.WorkersPosition;
import by.epam.hospital.service.IAuthorizationService;
import by.epam.hospital.service.exception.ServiceException;
import by.epam.hospital.service.exception.ServiceExceptionMessage;
import by.epam.hospital.service.model.LogInModel;

public class AuthorizationServiceImpl implements IAuthorizationService{

    private final static AuthorizationServiceImpl INSTANCE = new AuthorizationServiceImpl();
    private final IPatientDao patientDao = DaoHolder.getPatientDao();
    private final IPrescriptionDao prescriptionDao = DaoHolder.getPrescriptionDao();

    private AuthorizationServiceImpl(){}

    public static AuthorizationServiceImpl getInstance(){return INSTANCE;}

    @Override
    public boolean authorizeNurse(LogInModel logInModel) {
	return WorkersPosition.NURSE == WorkersPosition.valueOf(logInModel.getPosition());
    }

    @Override
    public boolean authorizeDoctorOrChief(LogInModel logInModel) {
	WorkersPosition position = WorkersPosition.valueOf(logInModel.getPosition());
	return WorkersPosition.DOCTOR == position || WorkersPosition.CHIEF_DOCTOR == position;
    }

    @Override
    public boolean authorizeChief(LogInModel logInModel) {
	return WorkersPosition.CHIEF_DOCTOR == WorkersPosition.valueOf(logInModel.getPosition());
    }

    private boolean authorizeDoctor(LogInModel logInModel) {
	WorkersPosition position = WorkersPosition.valueOf(logInModel.getPosition());
	return WorkersPosition.DOCTOR == position;
    }

    private Patient getPatient(int patientId) throws ServiceException{
	Patient patient;
	try {
	    patient = patientDao.getEntity(patientId);
	}catch (DaoException e){
	    throw new ServiceException(ServiceExceptionMessage.ERROR_AUTHORIZE_GETTING_PATIENT, e);
	}
	return patient;
    }

    private Prescription getPrescription(int prescriptionId) throws ServiceException{
	Prescription prescription;
	try {
	    prescription = prescriptionDao.getEntity(prescriptionId);
	}catch (DaoException e){
	    throw new ServiceException(ServiceExceptionMessage.ERROR_AUTHORIZE_GETTING_PATIENT, e);
	}
	return prescription;
    }

    @Override
    public boolean authorizeInitDiagnosisSet(LogInModel logInModel, int patientId) throws ServiceException{
	Patient patient = getPatient(patientId);
	return authorizeDoctorOrChief(logInModel) && patient != null && patient.getInitDiagnosis() == null
	    && patient.getDoctorId() == logInModel.getId();
    }

    @Override
    public boolean authorizePatientSignOut(LogInModel logInModel, int patientId) throws ServiceException{
	Patient patient = getPatient(patientId);
	return authorizeDoctorOrChief(logInModel) && patient != null && patient.getInitDiagnosis() != null
	    && patient.getDoctorId() == logInModel.getId() && patient.getStatus()== PatientStatus.ON_TREATMENT;
    }

    @Override
    public boolean authorizePatientDelete(LogInModel logInModel, int patientId) throws ServiceException{
	Patient patient = getPatient(patientId);
	return authorizeChief(logInModel) || patient.getInitDiagnosis() == null;
    }

    @Override
    public boolean authorizePrescriptionAdd(LogInModel logInModel, int patientId) throws ServiceException{
	Patient patient = getPatient(patientId);
	return authorizeDoctorOrChief(logInModel) && patient != null && patient.getInitDiagnosis() != null
	    && patient.getDoctorId() == logInModel.getId() && patient.getStatus()== PatientStatus.ON_TREATMENT;
    }

    @Override
    public boolean authorizePrescriptionDelete(LogInModel logInModel, int prescriptionId) throws ServiceException{
	Prescription prescription = getPrescription(prescriptionId);
	return authorizeChief(logInModel) || (authorizeDoctor(logInModel) && prescription.getStatus() ==
	    PrescriptionStatus.ASSIGNED && logInModel.getId()==prescription.getWorkerId());
    }

    @Override
    public boolean authorizePrescriptionDo(LogInModel logInModel, int prescriptionId) throws ServiceException {
	Prescription prescription = getPrescription(prescriptionId);
	if(prescription.getType() == PrescriptionType.OPERATION){
	    return authorizeDoctorOrChief(logInModel) && logInModel.getId()==prescription.getWorkerId();
	}else {
	    return !authorizeDoctorOrChief(logInModel) || logInModel.getId() == prescription.getWorkerId();
	}
    }

    @Override
    public boolean authorizePrescriptionEdit(LogInModel logInModel, int prescriptionId) throws ServiceException{
	Prescription prescription = getPrescription(prescriptionId);
	if(prescription.getStatus() == PrescriptionStatus.ASSIGNED && logInModel.getId()==prescription.getWorkerId()){
	    return authorizeDoctorOrChief(logInModel);
	}else {
	    return authorizeChief(logInModel);
	}
    }
}

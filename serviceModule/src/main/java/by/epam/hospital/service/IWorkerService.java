package by.epam.hospital.service;

import by.epam.hospital.service.exception.ServiceException;
import by.epam.hospital.service.model.LogInModel;
import by.epam.hospital.service.model.WorkerViewModel;

import java.util.List;


public interface IWorkerService {

    /**returns Logged user by email and pass
     *
     * checks if there is a worker with such email and if hashcode of pass = pass hashcode in database
     * @param email - email used to get worker from database, can be null
     * @param pass - pass used to get hashcode and check with database
     * @return LogInModel if there is a worker with such email and pass or null
     * @throws ServiceException
     */
    LogInModel getLoggedUser(String email, String pass) throws ServiceException;

    /**returns currently working list of doctors
     *
     * @return List<WorkerViewModel>
     * @throws ServiceException
     */
    List<WorkerViewModel> getActualDoctorsList() throws ServiceException;
}

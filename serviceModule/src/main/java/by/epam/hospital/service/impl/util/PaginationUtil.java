package by.epam.hospital.service.impl.util;

public class PaginationUtil implements IPaginationUtil {

    private final static int DEFAULT_ITEMS_PER_PAGE = 5;
    private final static int DEFAULT_PAGE = 1;

    private final static PaginationUtil INSTANCE = new PaginationUtil();

    private PaginationUtil(){}

    public static PaginationUtil getInstance(){ return INSTANCE;}

    @Override
    public int getItemsPerPage(String stringItemsPerPage) {
	int itemsPerPage;
	if(stringItemsPerPage == null){
	    itemsPerPage = DEFAULT_ITEMS_PER_PAGE;
	}else {
	    itemsPerPage = Integer.parseInt(stringItemsPerPage);
	}
	return itemsPerPage;
    }

    @Override
    public int getPageCount(int itemsPerPage, int itemsCount){
	int pageCount = itemsCount/itemsPerPage;
	if(itemsCount%itemsPerPage >0){
	    pageCount++;
	}
	return pageCount;
    }

    @Override
    public int getPage(String stringPage, int pageCount) {
	int page;
	if(stringPage == null){
	    page = DEFAULT_PAGE;
	}else{
	    page = Integer.parseInt(stringPage);
	    while (page>pageCount){
		page--;
	    }
	}
	return page;
    }

    @Override
   public int countFirstLimit(int page, int itemsPerPage){
	return (page-1)*itemsPerPage;
    }

}

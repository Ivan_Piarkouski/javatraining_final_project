package by.epam.hospital.service;

public interface ISystemService {

    /**
     *method prepares dao for work, inits pull of connections
     */
    void initDao();

    /**
     * method shuts dao down, closing pull of connections
     */
    void shutDownDao();
}
